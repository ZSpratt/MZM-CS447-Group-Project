package com.gdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by zachm on 10/19/2017.
 */
public class Operations {
	public static boolean CompareVector(Vector2 a, Vector2 b, float delta) {
		float dx = a.x - b.x;
		float dy = a.y - b.y;
		return (dx * dx + dy * dy < delta * delta);
	}

	public static boolean CompareVector(Vector2 a, Vector2 b, float deltaX, float deltaY) {
		float dx = a.x - b.x;
		float dy = a.y - b.y;
		return (Math.abs(dx) <= deltaX && Math.abs(dy) <= deltaY);
	}

	public static boolean CompareFloat(float a, float b, float delta) {
		return (a + delta > b && a - delta < b);
	}

	public static Color interpolateColor(Color x, Color y, float delta) {
		return interpolateColor(x, y, false, delta, 1);
	}

	public static Color interpolateColor(Color x, Color y, float delta, int power) {
		return interpolateColor(x, y, false, delta, power);
	}

	public static Color interpolateColor(Color x, Color y, boolean alpha, float delta, int power) {
		delta = (float) Math.pow(delta, power);
		float r = (float) Math.sqrt((1 - delta) * (x.r * x.r) + (delta) * (y.r * y.r));
		float g = (float) Math.sqrt((1 - delta) * (x.g * x.g) + (delta) * (y.g * y.g));
		float b = (float) Math.sqrt((1 - delta) * (x.b * x.b) + (delta) * (y.b * y.b));
		float a = 1;
		if (alpha) {
			a = (float) Math.sqrt(delta * (x.a * x.a) + (1 - delta) * (y.a * y.a));
		}
		return new Color(r, g, b, a);
	}

	public static Color multiplyColor(Color x, Color y, float alpha) {
		float r = x.r * y.r;
		float g = x.g * y.g;
		float b = x.b * y.b;
		float a = 1;
		return Operations.interpolateColor(x, new Color(r, g, b, a), alpha);
	}

	public static Vector2 worldToCamera(Vector2 a) {
		Vector2 result = new Vector2(a);
		result.sub(Game.mainCamera.position.x, Game.mainCamera.position.y);
		result.scl(3/Game.mainCamera.zoom);

		return result;
	}
}
