package com.gdx.game;


import com.badlogic.gdx.graphics.Color;
import com.gdx.game.Objects.MechParts.PartsList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class PlayerProfile {
	public static int[] equips = new int[5];
	public static boolean[] inverts = new boolean[5];
	public static Color primaryColor = new Color(1, 1, 1, 1);
	public static Color secondaryColor = new Color(1, 0, 0, 1);
	private static float wallet = 0;
	public static HashMap<String, Integer> inventory = new HashMap<String, Integer>();

	public static void addCurrency(float val) {
		wallet += val;
		//System.out.println(wallet);
	}

	public static void remCurrency(float val) {
		wallet -= val;
		//System.out.println(wallet);
	}

	public static int getWallet() {
		return (int)wallet;
	}

	public static boolean hasItem(String type, int i) {
		if (type.equals("Head")) {
			return hasItem(PartsList.Head[i]);
		}
		if (type.equals("Arm")) {
			return hasItem(PartsList.Arm[i]);
		}
		if (type.equals("Leg")) {
			return hasItem(PartsList.Leg[i]);
		}
		if (type.equals("Torso")) {
			return hasItem(PartsList.Torso[i]);
		}
		return  false;
	}

	public static boolean hasItem(String resource) {
		if (resource.equals(PartsList.Head[0]) || resource.equals(PartsList.Arm[0]) || resource.equals(PartsList.Arm[1]) || resource.equals(PartsList.Leg[0]) || resource.equals(PartsList.Torso[0])) {
			return  true;
		}
		return inventory.keySet().contains(resource);
	}

	public static float partCount(String type, int i) {
		if (type.equals("Head")) {
			return partCount(PartsList.Head[i]);
		}
		if (type.equals("Arm")) {
			return partCount(PartsList.Arm[i]);
		}
		if (type.equals("Leg")) {
			return partCount(PartsList.Leg[i]);
		}
		if (type.equals("Torso")) {
			return partCount(PartsList.Torso[i]);
		}
		return Float.NaN;
	}

	public static float partCount(String resource) {
		if (resource.equals(PartsList.Head[0]) || resource.equals(PartsList.Leg[0]) || resource.equals(PartsList.Torso[0]) || resource.equals(PartsList.Arm[0]) || resource.equals(PartsList.Arm[1])) {
			return Float.POSITIVE_INFINITY;
		}
		if (inventory.keySet().contains(resource)) {
			return inventory.get(resource);
		}
		else return 0;
	}

	public static void removeItem(String type, int i) {
		if (type.equals("Head")) {
			removeItem(PartsList.Head[i]);
		}
		if (type.equals("Arm")) {
			removeItem(PartsList.Arm[i]);
		}
		if (type.equals("Leg")) {
			removeItem(PartsList.Leg[i]);
		}
		if (type.equals("Torso")) {
			removeItem(PartsList.Torso[i]);
		}
	}

	public static void removeItem(String resource) {
		if (inventory.keySet().contains(resource)) {
			inventory.put(resource,  inventory.get(resource) - 1);
			if (inventory.get(resource) <= 0) {
				inventory.remove(resource);
			}
		}
		//System.out.println(resource + " : " + inventory.get(resource));
	}

	public static void updateEquips (){
		if (!hasItem(PartsList.Leg[equips[0]])) {
			equips[0] = 0;
		}
		if (!hasItem(PartsList.Torso[equips[1]])) {
			equips[1] = 0;
		}
		if (!hasItem(PartsList.Arm[equips[2]])) {
			equips[2] = 0;
		}
		if (!hasItem(PartsList.Arm[equips[3]])) {
			equips[3] = 1;
		}
		if (!hasItem(PartsList.Head[equips[4]])) {
			equips[4] = 0;
		}
	}

	public static void saveProfile() {
		//System.out.println("SAVE?");
		String lines = "";

		lines += "legs:" + equips[0] + ", " + inverts[0] + "\n";
		lines += "torso:" + equips[1] + ", " + inverts[1] + "\n";
		lines += "arml:" + equips[2] + ", " + inverts[2] + "\n";
		lines += "armr:" + equips[3] + ", " + inverts[3] + "\n";
		lines += "head:" + equips[4] + ", " + inverts[4] + "\n";
		lines += "wallet:" + (int) wallet + "\n";

		lines += "p:" + primaryColor.r + ", " + primaryColor.g + ", " + primaryColor.b +  "\n";

		lines += "s:" + secondaryColor.r + ", " + secondaryColor.g + ", " + secondaryColor.b +  "\n";

		lines += "up:" + Settings.up + "\n";
		lines += "down:" + Settings.down + "\n";
		lines += "left:" + Settings.left + "\n";
		lines += "right:" + Settings.right + "\n";
		lines += "pause:" + Settings.pause + "\n";
		lines += "repair:" + Settings.repair + "\n";
		lines += "respawn:" + Settings.respawn + "\n";
		lines += "interact:" + Settings.interact + "\n";
		lines += "profile:" + Settings.profile + "\n";
		lines += "reload:" + Settings.reload + "\n";

		lines += "gibScale:" + Settings.gibScale + "\n";
		lines += "volume:" + Settings.volume + "\n";

		Iterator<String> keys = inventory.keySet().iterator();
		while (keys.hasNext()) {
			String str = keys.next();
			lines += "Inv:" + str + "," + inventory.get(str) + "\n";
		}

		Game.saveGame(lines.split("\n"));
	}

	public static void loadProfile() {
		//System.out.println("LOAD?");
		String[] lines = Game.loadGame();
		if (lines != null) {
			for (int i = 0; i < lines.length; i++) {
				String[] data = lines[i].split(":");
				if (data[0].equals("legs")) {
					String[] args = data[1].split(", ");
					equips[0] = Integer.parseInt(args[0]);
					if (args.length > 1) {
						inverts[0] = Boolean.parseBoolean(args[1]);
					}
				}

				if (data[0].equals("torso")) {
					String[] args = data[1].split(", ");
					equips[1] = Integer.parseInt(args[0]);
					if (args.length > 1) {
						inverts[1] = Boolean.parseBoolean(args[1]);
					}
				}

				if (data[0].equals("arml")) {
					String[] args = data[1].split(", ");
					equips[2] = Integer.parseInt(args[0]);
					if (args.length > 1) {
						inverts[2] = Boolean.parseBoolean(args[1]);
					}
				}

				if (data[0].equals("armr")) {
					String[] args = data[1].split(", ");
					equips[3] = Integer.parseInt(args[0]);
					if (args.length > 1) {
						inverts[3] = Boolean.parseBoolean(args[1]);
					}
				}

				if (data[0].equals("head")) {
					String[] args = data[1].split(", ");
					equips[4] = Integer.parseInt(args[0]);
					if (args.length > 1) {
						inverts[4] = Boolean.parseBoolean(args[1]);
					}
				}
				if (data[0].equals("wallet")) {
					String[] args = data[1].split(", ");
					wallet = Integer.parseInt(args[0]);
				}

				if (data[0].equals("p")) {
					String[] args = data[1].split(", ");
					//System.out.println(Float.parseFloat(args[0]) + ", " + Float.parseFloat(args[1]) + ", " + Float.parseFloat(args[2]) + ", " + 1);
					primaryColor.set(Float.parseFloat(args[0]), Float.parseFloat(args[1]), Float.parseFloat(args[2]), 1);
				}

				if (data[0].equals("s")) {
					String[] args = data[1].split(",");
					//System.out.println(Float.parseFloat(args[0]) + ", " + Float.parseFloat(args[1]) + ", " + Float.parseFloat(args[2]) + ", " + 1);
					secondaryColor.set(Float.parseFloat(args[0]), Float.parseFloat(args[1]), Float.parseFloat(args[2]), 1);
				}

				if (data[0].equals("up")) {
					Settings.up = Integer.parseInt(data[1]);
				}

				if (data[0].equals("down")) {
					Settings.down = Integer.parseInt(data[1]);
				}

				if (data[0].equals("left")) {
					Settings.left = Integer.parseInt(data[1]);
				}

				if (data[0].equals("right")) {
					Settings.right = Integer.parseInt(data[1]);
				}

				if (data[0].equals("pause")) {
					Settings.pause = Integer.parseInt(data[1]);
				}

				if (data[0].equals("repair")) {
					Settings.repair = Integer.parseInt(data[1]);
				}

				if (data[0].equals("respawn")) {
					Settings.respawn = Integer.parseInt(data[1]);
				}

				if (data[0].equals("interact")) {
					Settings.interact = Integer.parseInt(data[1]);
				}

				if (data[0].equals("profile")) {
					Settings.profile = Integer.parseInt(data[1]);
				}

				if (data[0].equals("reload")) {
					Settings.reload = Integer.parseInt(data[1]);
				}

				if (data[0].equals("gibScale")) {
					Settings.gibScale = Float.parseFloat(data[1]);
				}

				if (data[0].equals("volume")) {
					Settings.volume = Float.parseFloat(data[1]);
				}

				if (data[0].equals("Inv")) {
					String[] args = data[1].split(",");
					//System.out.println(Float.parseFloat(args[0]) + ", " + Float.parseFloat(args[1]) + ", " + Float.parseFloat(args[2]) + ", " + 1);
					inventory.put(args[0], Integer.parseInt(args[1]));
				}
			}
		}
	}
}
