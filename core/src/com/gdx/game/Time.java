package com.gdx.game;

public class Time {
    public static void updateTime(float delta){
        realDeltaTime = delta;
        if (delta > 30/1000f) {
            delta = 30/1000f;
        }
        rawDeltaTime = delta;
        deltaTime = rawDeltaTime * timeScale;
    }

    public static float timeScale = 1f;
    public static float realDeltaTime = 1.0f;
    public static float rawDeltaTime = 1.0f;
    public static float deltaTime = 1.0f;

}
