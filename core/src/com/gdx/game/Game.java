package com.gdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Objects.Mech;
import com.gdx.game.Stages.LoadStage;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import java.io.*;
import java.util.concurrent.ConcurrentHashMap;

public class Game extends ApplicationAdapter implements Runnable{
	public static Game thisGame;
	public static int GAME_WIDTH = 300;
	public static int GAME_HEIGHT = 150;
	public static boolean paused = false;

	public static boolean connected = false;
	public static boolean isServer = false;
	Thread netThread;

	ServerSocket serverSocket = null;
	public static String netID = "";
	public static int clientCount = 0;
	ArrayList<Socket> clients = new ArrayList<Socket>();
	ArrayList<String> clientIDs = new ArrayList<String>();
	Socket client = null;

	public static boolean debug = false;
	public static boolean netDebug = false;
	public static boolean profile = false;

	//thread protection?
	public static boolean stopUpdate = false;
	public static boolean updateStopped = false;

	private Hashtable<String, classProfile> profiles = new Hashtable<String, classProfile>();
	private float profileInterval = 1;
	private float currentProfile = 0;

	public static SpriteBatch spriteBatch;
	public static SpriteBatch renderBatch;
	public static ShapeRenderer shapeBatch;
	FrameBuffer fb;

	public static OrthographicCamera mainCamera;
	public static OrthographicCamera renderCam;
	public static Viewport viewport;

	public static Stage currentStage;
	public static Stage nextStage;
	public static boolean swapStage = false;

	private static ArrayList<GameObject> objects = new ArrayList<GameObject>();
	private static ArrayList<GameObject> deleteObjects = new ArrayList<GameObject>();
	public static ArrayList<Drawable> drawObjects = new ArrayList<Drawable>();
	private static ArrayList<DrawLight> drawLights = new ArrayList<DrawLight>();
	private static ArrayList<DrawableGUI> drawObjectsGUI = new ArrayList<DrawableGUI>();
	public static ConcurrentHashMap<String, NetworkEntity> networkObjects = new ConcurrentHashMap<String, NetworkEntity>();
	public static ArrayList<Entity> physicsObjects = new ArrayList<Entity>();

	private Comparator<Drawable> drawComp = new Comparator<Drawable>() {
		@Override
		public int compare(Drawable o1, Drawable o2) {
			return  o1.z() - o2.z();
		}
	};
	private Comparator<DrawableGUI> drawGUIComp = new Comparator<DrawableGUI>() {
		@Override
		public int compare(DrawableGUI o1, DrawableGUI o2) {
			return o1.guiz() - o2.guiz();
		}
	};
	private Comparator<DrawLight> drawLightComp = new Comparator<DrawLight>() {
		@Override
		public int compare(DrawLight o1, DrawLight o2) {
			return o1.lightz() - o2.lightz();
		}
	};

	public static float mouseX;
	public static float mouseY;

	public static float GUImouseX;
	public static float GUImouseY;

	public static Random random;

	public static BitmapFont[] fonts = new BitmapFont[4];

	public static Texture background;
	public static float[] backGroundPosSize = {0, 0, 0, 0};
	private static ArrayList<Float[]> debugDrawLines = new ArrayList<Float[]>();

	public static Color ambientLightColor = Color.WHITE;
	public static Color backgroundColor = Color.DARK_GRAY;

	public static Vector2 gravity = new Vector2(0, -70);

	public static void addDebugLine(float x1, float y1, float x2, float y2 ) {
		Float[] line = new Float[4];
		line[0] = x1;
		line[1] = y1;
		line[2] = x2;
		line[3] = y2;
		debugDrawLines.add(line);
	}

	public static void clearDebugLide() {
		debugDrawLines.clear();
	}

	public static GameObject addGameObject(GameObject go) {
		objects.add(go);

		if (go instanceof Drawable) {
			drawObjects.add((Drawable) go);
		}

		if (go instanceof DrawableGUI) {
			drawObjectsGUI.add((DrawableGUI) go);
		}

		if (go instanceof DrawLight) {
			drawLights.add((DrawLight) go);
		}

		if (go instanceof NetworkEntity) {
			networkObjects.put(((NetworkEntity) go).netId, (NetworkEntity) go);
		}

		if (go instanceof Entity) {
			if (((Entity) go).hasPhysics()) {
				physicsObjects.add((Entity) go);
			}
		}
		return  go;
	}

	public static void setStage(Stage newStage) {
		swapStage = true;
		if (currentStage != null) {
			currentStage.endStage();
		}
		nextStage = newStage;
	}

	public static void clearAllObjects() {
		SoundManager.disableSounds = true;
		for (int i = 0; i < objects.size(); i++) {
			objects.get(i).Delete();
		}
		objects.clear();
		deleteObjects.clear();
		drawObjects.clear();
		drawObjectsGUI.clear();
		drawLights.clear();
		physicsObjects.clear();
		SoundManager.disableSounds = false;
	}

	public static void saveGame(String[] lines) {
		try {
			File f = new File("Save.dat");
			f.createNewFile();

			PrintWriter save = new PrintWriter(f);

			for (int i = 0; i < lines.length; i++) {
				save.write(lines[i] + "\n");
			}
			save.flush();
			save.close();
		} catch (IOException e) {
			System.err.println("Could not save");
			e.printStackTrace();
		}
	}

	public synchronized static void setServer() {
		if (thisGame.client == null && thisGame.serverSocket == null && connected == false) {
			try {
				netID = "Server";
				System.out.println("Server start");
				connected = true;
				isServer = true;
				thisGame.serverSocket = new ServerSocket(5000);
				thisGame.serverSocket.setSoTimeout(50);
				thisGame.netThread = new Thread(thisGame);
				thisGame.netThread.start();

				Game.paused = false;
			} catch (SocketException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String serverAddress = "";
	public synchronized static void connectToServer(String address) {
		Iterator<String> keys = networkObjects.keySet().iterator();
		String key = "";
		try {
			Game.paused = false;
			Game.setStage((Stage) currentStage.getClass().getConstructors()[0].newInstance());
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		if (connected == false) {
			connected = true;
			isServer = false;
			Game.serverAddress = address;
			thisGame.netThread = new Thread(thisGame);
			thisGame.netThread.start();
		}

		if (currentStage != null) {
			currentStage.startStage();
		}
	}

	public synchronized static void disconnect() {
		try {
		Game.thisGame.netThread.interrupt();
		if (isServer) {
			Game.thisGame.NCS.interrupt();
			Game.thisGame.serverSocket.close();
			Game.thisGame.serverSocket = null;
		} else {
			if (Game.thisGame.client != null) {
				Game.thisGame.client.close();
				Game.thisGame.client = null;
			}
		}
			Game.thisGame.clientIDs.clear();
			for (Socket c : Game.thisGame.clients) {
				c.close();
			}
			Game.thisGame.clients.clear();
		} catch (IOException e) {
			e.printStackTrace();
		}
		connected = false;
		isServer = false;

		Iterator<String> keys = networkObjects.keySet().iterator();
		String key = "";
		while (keys.hasNext()) {
			key = keys.next();
			//System.out.println(key);
			if (!networkObjects.get(key).isLocal) {
				//System.out.print(key);
				networkObjects.get(key).Delete();
			}
		}
	}

	public static String[] loadGame() {
		try {
			File f = new File("Save.dat");
			if (f.exists()) {
				FileReader file = new FileReader("Save.dat");
				BufferedReader load = new BufferedReader(file);
				String lines = "";
				String in;
				while ((in = load.readLine()) != null) {
					lines += in + "\n";
				}
				load.close();
				return lines.split("\n");
			}
		} catch (FileNotFoundException e) {
			System.err.println("Could not load");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Could not read");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void create () {
		thisGame = this;
		random = new Random();
		spriteBatch = new SpriteBatch();
		renderBatch = new SpriteBatch();
		shapeBatch = new ShapeRenderer();
		fb = new FrameBuffer(Pixmap.Format.RGBA8888, GAME_WIDTH, GAME_HEIGHT, true);

		mainCamera = new OrthographicCamera(600, 400);
		renderCam = new OrthographicCamera(600, 400);
		viewport  = new FitViewport(GAME_WIDTH, GAME_HEIGHT, mainCamera);

		for (int i = 0; i < 4; i++) {
			fonts[i] = new BitmapFont(Gdx.files.internal("Fonts/Font[" + i + "].fnt"), Gdx.files.internal("Fonts/Font[" + i + "].png"), false);
		}

		InProcess in = new InProcess();
		Gdx.input.setInputProcessor(in);
		mainCamera.zoom = 2;

		currentStage = new LoadStage();
		currentStage.startStage();
	}

	@Override
	public void resize(int width, int height) {
		fb = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, true);
		viewport.update(width, height);
	}

	public void update() {
		clearDebugLide();

		Time.updateTime(Gdx.graphics.getDeltaTime());

		if (Gdx.input.isKeyJustPressed(Settings.profile))  {
			profile = !profile;
		}
		if (!profile) {
			currentProfile = 0;
		}

		if (currentStage != null && currentStage.stageOver) {
			clearAllObjects();
			currentStage = nextStage;
			currentStage.startStage();
			swapStage = false;
			profiles.clear();
		}

		for (int i = 0; i < objects.size(); i++) {
			if (objects.get(i).enabled) {
				long currentTime = System.nanoTime();
				objects.get(i).earlyUpdate();
				objects.get(i).update();

				if (i <= objects.size()) {
					if (objects.get(i).delete) {
						deleteObjects.add(objects.get(i));
					}

					objects.get(i).lateUpdate();
					if (profile && currentProfile == 0) {
						long deltaTime = System.nanoTime() - currentTime;
						String clas = objects.get(i).getClass().getName();
						if (profiles.containsKey(clas)) {
							profiles.get(clas).totalTime += deltaTime;
							profiles.get(clas).count += 1;
						} else {
							classProfile cp = new classProfile();
							cp.count = 1;
							cp.name = clas;
							cp.totalTime = deltaTime;
							profiles.put(clas, cp);
						}
					}
				}
			}

		}

		for (int i = 0; i < physicsObjects.size(); i++) {
			if (physicsObjects.get(i) != null && physicsObjects.get(i).enabled) {
				physicsObjects.get(i).preparePhysics();
			}
		}

		for (int i = 0; i < physicsObjects.size(); i++) {
			Entity a = physicsObjects.get(i);
			if (a.enabled) {
				for (int n = i + 1; n < physicsObjects.size(); n++) {
					Entity b = physicsObjects.get(n);
					if (a.isLocal | b.isLocal) {
						a.checkCollision(b);
					}
				}
			}
		}

		for (GameObject o : deleteObjects) {
			objects.remove(o);
			if (o instanceof  Drawable) {
				drawObjects.remove(o);
			}
			if (o instanceof DrawLight) {
				drawLights.remove(o);
			}
			if (o instanceof DrawableGUI) {
				drawObjectsGUI.remove(o);
			}

			/*
			if (o instanceof NetworkEntity) {
				networkObjects.remove(((NetworkEntity) o).netId);
			}
			*/

			if (o instanceof Entity) {
				physicsObjects.remove(o);
			}
		}
		deleteObjects.clear();

		InProcess.scroll = 0;
		SoundManager.updateSounds();

		//System.out.println(player.z);
	}

	@Override
	public synchronized void render () {
		//System.out.println(Gdx.input.getX() + ", " + Gdx.input.getY());
		Vector3 v = viewport.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
		mouseX =  v.x;
		mouseY =  v.y;

		//System.out.println(mouseX + ", " + mouseY);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		update();

		mainCamera.update();
		spriteBatch.setProjectionMatrix(viewport.getCamera().combined);
		shapeBatch.setProjectionMatrix(viewport.getCamera().combined);
		spriteBatch.setBlendFunction(Gdx.gl.GL_SRC_ALPHA, Gdx.gl.GL_ONE_MINUS_SRC_ALPHA);

		viewport.apply();

		fb.begin();
		shapeBatch.begin(ShapeRenderer.ShapeType.Filled);
		shapeBatch.setColor(backgroundColor);
		shapeBatch.rect(-viewport.getWorldWidth()/2 * mainCamera.zoom + mainCamera.position.x, -viewport.getWorldHeight()/2 * mainCamera.zoom + mainCamera.position.y, viewport.getWorldWidth() * mainCamera.zoom, viewport.getWorldHeight() * mainCamera.zoom);
		shapeBatch.end();

		spriteBatch.setColor(Color.WHITE);
		spriteBatch.begin();
		if (background != null) {
			if (backGroundPosSize != null) {
				spriteBatch.draw(background, backGroundPosSize[0], backGroundPosSize[1], 0, 0, (int) backGroundPosSize[2], (int) backGroundPosSize[3]);
			}
		}

		Collections.sort(drawObjects, drawComp);
		boolean active = true;
		for (int i = 0; i < drawObjects.size(); i++) {
			try {
				active = ((GameObject) drawObjects.get(i)).enabled;
			} catch (Exception e) {
				active = true;
			}
			if (active) {
				drawObjects.get(i).render(spriteBatch);
			}
		}

		spriteBatch.end();

		if (Game.debug) {
			shapeBatch.begin(ShapeRenderer.ShapeType.Line);
			shapeBatch.setColor(Color.WHITE);
			for (int i = 0; i < physicsObjects.size(); i++) {
				if (physicsObjects.get(i).enabled) {
					if (physicsObjects.get(i).team < 0) {
						shapeBatch.setColor(Color.RED);
					}
					if (physicsObjects.get(i).team == 0) {
						shapeBatch.setColor(Color.WHITE);
					}
					if (physicsObjects.get(i).team >= 1) {
						shapeBatch.setColor(Color.BLUE);
					}
					Polygon p = physicsObjects.get(i).getCollider();
					if (p != null) {
						//System.out.println(p.getX() + ", " + p.getY());
						shapeBatch.polygon(p.getTransformedVertices());
					}
				}
			}
			shapeBatch.end();
		}

		fb.end(viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

		spriteBatch.begin();
		spriteBatch.setColor(ambientLightColor);
		spriteBatch.draw(ResourceManager.getImage("fullBright.png"), mainCamera.position.x -GAME_WIDTH/2 * mainCamera.zoom, mainCamera.position.y -GAME_HEIGHT/2 * mainCamera.zoom, GAME_WIDTH * mainCamera.zoom, GAME_HEIGHT * mainCamera.zoom);

		Collections.sort(drawLights, drawLightComp);
		for (int i = 0; i < drawLights.size(); i++) {
			try {
				active = ((GameObject) drawLights.get(i)).enabled;
			} catch (Exception e) {
				active = true;
			}
			if (active) {
				spriteBatch.setColor(new Color(1.0f, 1f, 1f, 1));
				drawLights.get(i).renderLight(spriteBatch);
			}
		}

		spriteBatch.setBlendFunction(Gdx.gl.GL_ZERO, Gdx.gl.GL_SRC_COLOR);
		spriteBatch.draw(fb.getColorBufferTexture(), mainCamera.position.x - GAME_WIDTH/2 * mainCamera.zoom, mainCamera.position.y - GAME_HEIGHT/2 * mainCamera.zoom, GAME_WIDTH * mainCamera.zoom, GAME_HEIGHT * mainCamera.zoom, 0, 0, fb.getWidth(), fb.getHeight(), false, true);

		spriteBatch.end();

		shapeBatch.setColor(Color.RED);
		shapeBatch.begin(ShapeRenderer.ShapeType.Line);
		for (Float[] line : debugDrawLines) {
			shapeBatch.point(line[0], line[1], 1);
			shapeBatch.point(line[2], line[3], 1);
			shapeBatch.line(line[0], line[1], line[2], line[3]);
		}
		shapeBatch.end();

		spriteBatch.setBlendFunction(Gdx.gl.GL_SRC_ALPHA, Gdx.gl.GL_ONE_MINUS_SRC_ALPHA);
		Vector3 tmpPos = new Vector3(mainCamera.position);
		float tmpZoom = mainCamera.zoom;

		mainCamera.position.set(0, 0, 0);
		mainCamera.zoom = 3;
		mainCamera.update();
		spriteBatch.setProjectionMatrix(mainCamera.combined);

		v = viewport.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
		GUImouseX =  v.x;
		GUImouseY =  v.y;

		spriteBatch.begin();
		Collections.sort(drawObjectsGUI, drawGUIComp);
		for (int i = 0; i< drawObjectsGUI.size(); i++) {
			try {
				active = ((GameObject) drawObjectsGUI.get(i)).enabled;
			} catch (Exception e) {
				active = true;
			}
			if (active) {
				drawObjectsGUI.get(i).renderGUI(spriteBatch);
			}
		}
		if (profile) {
			Iterator<String> keys = profiles.keySet().iterator();
			int i = 0;
			while (keys.hasNext()) {
				String key = keys.next();
				//System.out.println(profiles.get(key).toString());
				spriteBatch.setColor(Color.WHITE);
				StringDraw sd = new StringDraw(profiles.get(key).toString(), -GAME_WIDTH / 2 * 3, +GAME_HEIGHT / 2 * 3 - 32 * i, 0, 0);
				sd.font = fonts[0];
				sd.update();
				sd.renderGUI(spriteBatch);
				i += 1;
			}
			currentProfile += Time.deltaTime;
			if (currentProfile >= profileInterval) {
				profiles.clear();
				currentProfile = 0;
			}
		}

		spriteBatch.end();

		mainCamera.position.set(tmpPos);
		mainCamera.zoom = tmpZoom;
		mainCamera.update();
	}

	@Override
	public void dispose () {
		spriteBatch.dispose();
		renderBatch.dispose();
		shapeBatch.dispose();
		if (currentStage != null) {
			currentStage.onEndStage();
		}
		if (netThread != null) {
			netThread.interrupt();
		}
		System.exit(1);
	}

	private class networkConnectServer implements Runnable {

		@Override
		public void run() {
			while (! Thread.currentThread().isInterrupted()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {

				}

				try {
					Socket client = serverSocket.accept();
					client.setSoTimeout(400);
					client.setTcpNoDelay(true);
					BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
					String id = "Client" + (clientCount + 1);
					writer.write(id + "\n\r");
					writer.flush();
					clients.add(client);
					clientIDs.add(id);
					clientCount += 1;
				} catch (GdxRuntimeException e) {
				} catch (IOException e) {
				}
			}
		}
	}

	public static Thread NCS = null;

	private int messagesIn = 0;
	private int messagesOut = 0;
	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {

			}
			ArrayList<String> clientsDisconnected = new ArrayList<String>();

			if (isServer) {
				if (NCS == null) {
					networkConnectServer thread = new networkConnectServer();
					NCS = new Thread(thread);
					NCS.start();
				}
				//System.out.println("Server");
				messagesIn = 0;
				messagesOut = 0;

				for (int i = 0; i < clients.size(); i++) {
					String fail = processNetworkOutput(clients.get(i));
					fail = processNetowrkInput(clients.get(i), clientIDs.get(i));
					if (fail != null) {
						System.out.println(fail);
						clientsDisconnected.add(fail);
					}
				}

				for (int i = 0; i < clientsDisconnected.size(); i++) {
					int d = clientIDs.indexOf(clientsDisconnected.get(i));
					clients.remove(d);
					String ID = clientIDs.get(d);
					clientIDs.remove(d);
					Iterator<String> keys = networkObjects.keySet().iterator();
					String key = "";
					while (keys.hasNext()) {
						key = keys.next();
						//System.out.println(key);
						if (key.contains(ID)) {
							//System.out.print(key);
							networkObjects.get(key).Delete();
						}
					}
				}
				clientsDisconnected.clear();

				if (Game.debug) {
					System.out.println(String.format("Messages In, Out : %d, %d", messagesIn, messagesOut));
				}
			} else {
				//System.out.println("Client");
				if (client == null) {
					try {
						client = new Socket(serverAddress, 5000);
						client.setSoTimeout(400);
						client.setTcpNoDelay(true);
						BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
						netID = reader.readLine();
					} catch (IOException e) {
						//e.printStackTrace();
					}
				} else {
					if (!client.isConnected()) {
						client = null;
						connected = false;
						return;
					}
					messagesOut = 0;
					messagesIn = 0;

					processNetworkOutput(client);
					String fail = processNetowrkInput(client, "");

					if (Game.debug) {
						System.out.println(String.format("Messages In, Out : %d, %d", messagesIn, messagesOut));
					}

					if (fail != null) {
						Iterator<String> keys = networkObjects.keySet().iterator();
						String key = "";
						while (keys.hasNext()) {
							key = keys.next();
							//System.out.println(key);
							if (!networkObjects.get(key).isLocal) {
								//System.out.print(key);
								networkObjects.get(key).Delete();
							}
						}
						connected = false;
						client = null;
					}
				}
			}

			Iterator<String> keys = networkObjects.keySet().iterator();
			String key;
			while (keys.hasNext()) {
				key = keys.next();
				NetworkEntity entity = networkObjects.get(key);
				if (entity.delete) {
					networkObjects.remove(key);
				}
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private synchronized String processNetowrkInput(Socket client, String  clientID) {
		try {
			Iterator<String> keys = networkObjects.keySet().iterator();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
			BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
			if (Game.netDebug) {
				System.out.println("Start Network write");
			}
			String key;
			writer.write(currentStage.serialize() + "\n");
			while (keys.hasNext()) {
				messagesOut += 1;
				key = keys.next();
				NetworkEntity entity = networkObjects.get(key);
				if (entity.isLocal | (isServer && !entity.netId.endsWith(clientID))) {
					if (entity.updateCount == 0 || entity.delete) {
						String message = entity.serialize();
						if (Game.netDebug) {
							System.out.println(message);
						}
						writer.write(message + "\n");
					}
				}
				//System.out.println(message);
			}

			writer.write("\n\r");
			writer.flush();

			if (Game.netDebug) {
				System.out.println("End Network write");
			}
		} catch (SocketException e) {
			//e.printStackTrace();
			return clientID;
		} catch (IOException e) {
			e.printStackTrace();
			return clientID;
		} catch (ConcurrentModificationException e) {
			e.printStackTrace();
			return clientID;
		}
		return null;
	}

	private synchronized String processNetworkOutput(Socket client) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
			if (reader.ready()) {
				if (Game.netDebug) {
					System.out.println("Start Network read");
				}
				int i = 0;
				while (reader.ready()) {
					String line = reader.readLine();
					messagesIn += 1;
					i += 1;
					if (Game.netDebug) {
						System.out.println(line);
					}
					String[] data = line.split("  ");
					if (data[0].equals("DEL")) {
						if (networkObjects.containsKey(data[1])) {
							networkObjects.get(data[1]).Delete();
						}
						networkObjects.remove(data[1]);
					} else if (data[0].equals("STG")) {
						currentStage.deSerialize(data);
					} else if (data.length > 1) {
						if (networkObjects.containsKey(data[0])) {
							networkObjects.get(data[0]).deSerialize(data);
						} else {
							String type = data[0].split("@")[0];
							if (Game.netDebug) {
								System.out.println("Object not found\n" + line);
							}
							try {
								Class<?> objClass = Class.forName(type);
								Constructor<?> objConstructor = objClass.getConstructor((String.class));
								//System.out.println(objConstructor);
								NetworkEntity netEntity = (NetworkEntity) (objConstructor.newInstance(line));

								Game.addGameObject(netEntity);

							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InstantiationException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
						}
					}
				}
				if (Game.netDebug) {
					System.out.println("End Network read\n" + i + " Messages read");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private class classProfile {
		int count;
		String name;
		long totalTime;

		@Override
		public String toString() {
			return name + " (" + count + ") : Total Time : " + totalTime / 1000000f + "ms , Average Time : " + totalTime/count / 1000000f + " ms";
		}
	}
}
