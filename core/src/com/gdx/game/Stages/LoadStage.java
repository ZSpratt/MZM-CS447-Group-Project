package com.gdx.game.Stages;

import com.badlogic.gdx.graphics.Color;
import com.gdx.game.BaseObjects.GUIFillBar;
import com.gdx.game.BaseObjects.Stage;
import com.gdx.game.BaseObjects.StringDraw;
import com.gdx.game.Game;
import com.gdx.game.ResourceManager;

/**
 * Created by zachm on 11/17/2017.
 */
public class LoadStage extends Stage {
	//StringDraw progress;
	GUIFillBar fill;

	@Override
	public void startStage() {
		super.startStage();

		//progress = (StringDraw) Game.addGameObject(new StringDraw("", 0, 0, 0, 4));
		fill = (GUIFillBar) Game.addGameObject(new GUIFillBar(0, -128, 800, 16, 0));
		fill.emptyColor = new Color(Color.BLACK);
		fill.midColor = new Color(Color.GRAY);
		fill.fullColor = new Color(Color.WHITE);
	}

	@Override
	public void update() {
		if (ResourceManager.loadProgress <= 1) {
			ResourceManager.loadAll();
		}

		fill.fill = ResourceManager.loadProgress;

		String s = ResourceManager.lastLoaded;
		StringDraw newLine = (StringDraw) Game.addGameObject(new StringDraw(s, 0, -100, 0, 4));
		newLine.lifetime = 1;
		newLine.velocity.set(0, 600);

		if (ResourceManager.loadProgress >= 1) {
			Game.setStage(new LevelStage("Maps/MapProjects/HubWorld.tmx"));
		}
	}
}
