package com.gdx.game.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.*;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Objects.GUI_PARTS.*;
import com.gdx.game.Objects.Mech;
import com.gdx.game.Objects.MechParts.PartsList;
import com.gdx.game.Objects.Objective;
import com.gdx.game.Objects.Zombie;

import java.util.ArrayList;

/**
 * Created by zachm on 11/17/2017.
 */
public class LevelStage extends Stage{

	public Mech playerMech;
	public PauseMenu pauseMenu;
	public ShopMenu shopMenu;
	public StageUI stageUI;

	public boolean hideSelector = true;
	public boolean showLevelMenu = false;
	public boolean loaded = false;

	public String levelResource = null;
	//Arena, Capture, Null
	public String gametype = "Arena";
	LevelSelect levelSelectUI;
	long musicID = 0;

	public LevelStage() {
		levelResource = "";
	}

	public LevelStage(String resource) {
		levelResource = resource;
	}

	@Override
	public void onStartStage() {
		ResourceManager.getSound("Sounds/music.wav").stop();
		musicID = ResourceManager.getSound("Sounds/music.wav").loop();
	}

	public void load() {
		System.out.println(levelResource);
		loaded = true;
		PlayerProfile.loadProfile();

		hideSelector = true;
		LevelLoader.loadLevel(levelResource);
		pauseMenu = (PauseMenu) Game.addGameObject(new PauseMenu());
		pauseMenu.enabled = !hideSelector;

		playerMech = new Mech(LevelLoader.playerSpawns.get(Game.random.nextInt(LevelLoader.playerSpawns.size())), 0, 0, null);
		Game.addGameObject(playerMech);
		//shopMenu = (ShopMenu) Game.addGameObject(new ShopMenu());

		stageUI = new StageUI();
		Game.addGameObject(stageUI);
		Game.addGameObject(new ChatWindow(new Vector2(-200, -100), 0, 0, null));

		if (LevelLoader.objectives.size() > 0) {
			gametype = "Objective";
			int i = Game.random.nextInt(LevelLoader.objectives.size());
			objective = new Objective(i, LevelLoader.objectives.get(i), 0, 1100, null);
			Game.addGameObject(objective);
			System.out.println(objective.pos);
		} else if (levelResource.contains("Hub")) {
			gametype = "Hub";
		}

		levelSelectUI = (LevelSelect) Game.addGameObject(new LevelSelect(levelResource));
		levelSelectUI.enabled = false;
	}

	public void spawnZombies(int n) {
		if (LevelLoader.zombieSpawns.size() > 0) {
			for (int i = 0; i <= n; i++) {
				zombies.add((Zombie) Game.addGameObject(new Zombie(LevelLoader.zombieSpawns.get(Game.random.nextInt(LevelLoader.zombieSpawns.size())))));
			}
		}
	}

	public void spawnEnemies(int cycles) {
		if (LevelLoader.enemySpawns.size() > 0) {
			for (int x = 0; x < cycles; x++) {
				for (int i = 0; i < LevelLoader.enemySpawns.size(); i++) {
					Mech enemy = (Mech) Game.addGameObject(new Mech(new Vector2(LevelLoader.enemySpawns.get(i)), 0, 0, null));
					enemy.isPlayer = false;
					enemies.add(enemy);
					if (Game.random.nextFloat() > 0.5f) {
						enemy.right = false;
					}
					enemy.randomParts();
				}
			}
		}
	}

	ArrayList<Zombie> zombies = new ArrayList<Zombie>();
	int zombieCount = 0;
	ArrayList<Mech> enemies = new ArrayList<Mech>();
	int enemyCount = 0;

	boolean rest = false;
	boolean respawn = false;
	boolean gameOver = false;
	boolean noChance = false;
	public boolean win = false;
	Objective objective = null;

	float stageTime = 0;
	int round = 0;
	int lives = 3;
	float respawnTime = 0;

	public void arena() {
		if (!Game.connected || Game.isServer) {
			zombieCount = zombies.size();
			enemyCount = enemies.size();
			for (int i = zombies.size() - 1; i >= 0; i--) {
				if (zombies.get(i).delete) {
					zombies.remove(i);
				}
			}
			for (int i = enemies.size() - 1; i >= 0; i--) {
				if (enemies.get(i).delete) {
					enemies.remove(i);
				}
			}
		}

		if (!rest) {
			stageUI.message = gametype.toUpperCase() + " : " + round ;
			if (!Game.connected || Game.isServer) {
				stageTime += Time.deltaTime;
			}
			if (zombieCount == 0 && enemyCount == 0) {
				rest = true;
				if (!Game.connected || Game.isServer) {
					stageTime = 10;
					round += 1;
				}
			}
		} else {
			if (stageTime <= 0) {
				if (!Game.connected || Game.isServer) {
					spawnZombies(100 + round * 25);
					spawnEnemies(round);
				}
				rest = false;
			} else {
				if (!Game.connected || Game.isServer) {
					stageTime -= Time.deltaTime;
				}
				stageUI.message = "NEXT WAVE IN";
			}
		}

		if (round > 5) {
			win = true;
		}

		if (!rest) {
			stageUI.message = gametype.toUpperCase() + " : " + round;
		} else {
			stageUI.message = "NEXT WAVE IN";
		}
	}

	public void objective() {
		if (!Game.connected || Game.isServer) {
			stageTime += Time.deltaTime;
			zombieCount = zombies.size();
			enemyCount = enemies.size();
			for (int i = zombies.size() - 1; i >= 0; i--) {
				if (zombies.get(i).delete) {
					zombies.remove(i);
				}
			}
			for (int i = enemies.size() - 1; i >= 0; i--) {
				if (enemies.get(i).delete) {
					enemies.remove(i);
				}
			}
		}

		if (zombieCount == 0 && enemyCount == 0) {
			spawnZombies(200);
			spawnEnemies(1);
		}

		if (!Game.connected || Game.isServer) {
			if (objective.delete) {
				win = true;
			}
		}

		stageUI.message = gametype;
		stageTime += Time.deltaTime;
	}

	public void hub() {
		if (!Game.connected || Game.isServer) {
			stageTime += Time.deltaTime;
			stageUI.message = "Hub " + Input.Keys.toString(Settings.mapSelect) + " to open map";
			for (int i = zombies.size() - 1; i >= 0; i--) {
				if (zombies.get(i).delete) {
					zombies.remove(i);
				}
			}
			zombieCount = zombies.size();

			if (zombieCount <= 0) {
				spawnZombies(100);
			}
		} else {
			stageUI.message = "Hub";
		}

	}

	@Override
	public void update() {
		ResourceManager.getSound("Sounds/music.wav").setVolume(musicID, Settings.volume);
		delete = false;
		super.update();

		if (Gdx.input.isKeyJustPressed(Settings.mapSelect)) {
			showLevelMenu = !showLevelMenu;
		}

		if (noChance && !Game.connected) {
			gameOver = true;
		}
		if (levelSelectUI != null && (!Game.connected || Game.isServer)) {
			if (gameOver || showLevelMenu || win) {
				levelSelectUI.enabled = true;
			} else {
				levelSelectUI.enabled = false;
			}
		}

		if (!loaded) {
			if (levelResource != null && levelResource != "") {
				load();
			}

		} else {
			if (playerMech != null && playerMech.delete) {
				respawnTime += Time.deltaTime;
			}
			if (playerMech == null || (respawnTime >= 10 || (!Game.paused && Gdx.input.isKeyJustPressed(Settings.respawn)))) {
				if (respawnTime >= 10 || Gdx.input.isKeyJustPressed(Settings.respawn)) {
					respawnTime = 0;
					if (LevelLoader.playerSpawns.size() > 0) {
						Mech tmp = new Mech(LevelLoader.playerSpawns.get(Game.random.nextInt(LevelLoader.playerSpawns.size())), 0, 0, null);
						if (playerMech != null) {
							playerMech.Delete();
							tmp.health = playerMech.health * 0.75f;
							if (tmp.health > 0) {
								Game.addGameObject(tmp);
								playerMech = tmp;
							} else {
								if (lives > 0) {
									respawn = true;
									tmp.health = 100;
									PlayerProfile.removeItem(playerMech.head.currentResource);
									PlayerProfile.removeItem(playerMech.arms.get(0).currentResource);
									PlayerProfile.removeItem(playerMech.arms.get(1).currentResource);
									PlayerProfile.removeItem(playerMech.legs.currentResource);
									PlayerProfile.removeItem(playerMech.body.currentResource);
									PlayerProfile.updateEquips();
									Game.addGameObject(tmp);
									playerMech = tmp;
								} else {
									noChance = true;
								}
							}
						} else {
							Game.addGameObject(tmp);
							playerMech = tmp;
						}
					}
				}
			}

			if (playerMech != null || playerMech.delete) {
				Game.mainCamera.zoom += InProcess.scroll * 0.125f;
				if (Game.mainCamera.zoom < 1) {
					Game.mainCamera.zoom = 1;
				}

				if (Game.mainCamera.zoom > 10) {
					Game.mainCamera.zoom = 10;
				}


				if (Gdx.input.isKeyPressed(Settings.up)) {
					Game.mainCamera.position.y += 50 * Time.deltaTime * Game.mainCamera.zoom;
				}
				if (Gdx.input.isKeyPressed(Settings.down)) {
					Game.mainCamera.position.y -= 50 * Time.deltaTime * Game.mainCamera.zoom;
				}
				if (Gdx.input.isKeyPressed(Settings.right)) {
					Game.mainCamera.position.x += 50 * Time.deltaTime * Game.mainCamera.zoom;
				}
				if (Gdx.input.isKeyPressed(Settings.left)) {
					Game.mainCamera.position.x -= 50 * Time.deltaTime * Game.mainCamera.zoom;
				}
			}

			if (lives <= 0 && playerMech.delete) {
				noChance = true;
			}

			if (!Game.connected || Game.isServer) {
				if (respawn) {
					lives -= 1;
					respawn = false;
				}
			}

			if (!win) {
				if (gametype.equals("Arena")) {
					arena();
				} else if (gametype.equals("Objective")) {
					objective();
				} else if (gametype.equals("Hub")) {
					hub();
				}else {
					stageTime += Time.deltaTime;
					stageUI.message = "No Game";
				}
			} else {
				stageTime += Time.deltaTime;
				stageUI.message = "Victory";
			}

			if (stageUI != null) {
				stageUI.enabled = (gametype != null);
				stageUI.zombieCount = zombieCount;
				stageUI.enemyCount = enemyCount;
				stageUI.roundTime = stageTime;
				stageUI.lives = lives;
				if (playerMech.delete && lives > 0) {
					stageUI.message = "Respawn in";
					stageUI.roundTime = 10 - respawnTime;
				} else if (playerMech.delete && lives <= 0 && gameOver && !win){
					stageUI.message = "Game Over";
					stageUI.roundTime = 0;
				}
			}

			if (Gdx.input.isKeyJustPressed(Settings.pause)) {
				if (!Game.paused && hideSelector) {
					hideSelector = false;
					Game.paused = true;
				} else {
					hideSelector = true;
					Game.paused = false;
				}
			}

			if (playerMech != null) {
				playerMech.head.updatePart(PartsList.Head[PlayerProfile.equips[4]]);
				playerMech.body.updatePart(PartsList.Torso[PlayerProfile.equips[1]]);
				playerMech.arms.get(0).updatePart(PartsList.Arm[PlayerProfile.equips[2]]);
				playerMech.arms.get(1).updatePart(PartsList.Arm[PlayerProfile.equips[3]]);
				playerMech.legs.updatePart(PartsList.Leg[PlayerProfile.equips[0]]);
			}

			// menu flags
			pauseMenu.enabled = !hideSelector;

			/*
			if (!Game.connected || Game.isServer) {
				if (hideSelector && Gdx.input.isKeyJustPressed(Settings.reload)) {
					restart = true;
					Game.setStage(new LevelStage(levelResource));
				}
			}
			*/

			if (playerMech != null) {
				playerMech.primaryColor.set(PlayerProfile.primaryColor);
				playerMech.secondaryColor.set(PlayerProfile.secondaryColor);
			}
		}
	}

	@Override
	public void onEndStage() {
		super.onEndStage();
		PlayerProfile.saveProfile();
	}

	ArrayList<Boolean> deadClients = new ArrayList<Boolean>();

	@Override
	public synchronized String serialize() {
		gameOver = noChance;
		for (Boolean dead : deadClients) {
			gameOver = gameOver && dead;
		}
		deadClients.clear();
		String s = super.serialize() + "  " + levelResource + "  " + gametype + "  " + zombies.size() + "  " + enemies.size() + "  " + stageTime + "  " + round + "  " + lives + "  " + respawn + "  " + noChance + "  " + gameOver + "  " + win;
		respawn = false;
		return s;
	}

	@Override
	public synchronized void deSerialize(String[] s) {
		super.deSerialize(s);
		if (!Game.isServer && s.length > 2) {
			levelResource = s[2];
		}
		if (!Game.isServer && s.length > 3) {
			gametype = s[3];
		}
		try {
			if (!Game.isServer && s.length > 4) {
				zombieCount = Integer.parseInt(s[4]);
			}

			if (!Game.isServer && s.length > 5) {
				enemyCount = Integer.parseInt(s[5]);
			}

			if (!Game.isServer && s.length > 6) {
				stageTime = Float.parseFloat(s[6]);
			}

			if (!Game.isServer && s.length > 7) {
				round = Integer.parseInt(s[7]);
			}

			if (!Game.isServer && s.length > 8) {
				lives = Integer.parseInt(s[8]);
			}

			if (Game.isServer && s.length > 10) {
				deadClients.add(Boolean.parseBoolean(s[10]));
			}

			if (!Game.isServer && s.length > 11) {
				gameOver = Boolean.parseBoolean(s[11]);
			}

			if (!Game.isServer && s.length > 12) {
				win = Boolean.parseBoolean(s[12]);
			}
		} catch (Exception e) {

		}
	}
}
