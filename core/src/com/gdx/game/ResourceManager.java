package com.gdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

public class ResourceManager {
    private static ConcurrentHashMap<String, Texture> images = new ConcurrentHashMap<String, Texture>();
    private static ConcurrentHashMap<String, Sound> sounds = new ConcurrentHashMap<String, Sound>();

    public static String lastLoaded = "";
    public static float loadProgress = 0;

	public static void loadImage(String dir) {
		try {
			Texture i = new Texture(dir);
			i.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
			images.put(dir, i);
		} catch (Exception e) {
			System.err.println("Texture \"" + dir + "\" Not Loaded");
		}
	}

	public static void loadSound(String dir) {
		try {
			Sound i = Gdx.audio.newSound(Gdx.files.internal(dir));
			sounds.put(dir, i);
		} catch (Exception e) {
			System.err.println("Sound \"" + dir + "\" Not Loaded");
		}
	}

    public static Texture getImage(String dir) {
        if (!images.containsKey(dir)) {
            loadImage(dir);
        }
        return images.get(dir);
    }

    public static Sound getSound(String dir) {
        if (!sounds.containsKey(dir)) {
            loadSound(dir);
        }
        return sounds.get(dir);
    }

	public static Animation<TextureRegion> getAnim(String dir, int frameCount, float frameDuration) {
		int splitW = (int) Math.sqrt(frameCount);
		int splitH = splitW;
		while (splitW * splitH < frameCount){
			splitH += 1;
		}

		Texture idle = ResourceManager.getImage(dir);
		TextureRegion[][] tmp = TextureRegion.split(idle, idle.getWidth()/splitW, idle.getHeight()/splitH);
		TextureRegion[] frames = new TextureRegion[frameCount];

		int diff = splitH * splitW - frameCount;
		//System.out.println(splitH + ", " + splitW + " : " + frameCount + " -> " + diff);
		//System.out.println(tmp.length + ", " + tmp[0].length);

		int index = -1;
		for (int i = 0; i < splitH; i++) {
			for (int j = 0; j < splitW; j++) {
				index++;
				//System.out.println(index + " : " + tmp[i][j]);
				if (index < frameCount) {
					frames[index] = tmp[i][j];
				} else {
					break;
				}
			}
		}
		Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration, frames);
		anim.setPlayMode(Animation.PlayMode.LOOP);
		return anim;
	}

	static String[] manifest = null;
	static int loaded = 0;
	public static void loadAll () {
		if (manifest == null) {
			FileHandle resouceData = Gdx.files.internal("manifest");
			if (resouceData.exists()) {
				manifest = resouceData.readString().split("[\\r\\n]+");
			} else {
				System.err.println("Manifest missing");
				System.exit(0);
			}
		}
		if (manifest != null) {
			String line = manifest[loaded];
			//System.out.println(line);

			if (line.contains(".png")) {
				ResourceManager.loadImage(line);
				lastLoaded = line;
			} else if (line.contains(".wav") || line.contains(".mp3")) {
				ResourceManager.loadSound(line);
				lastLoaded = line;
			} else {
				System.err.println("Unknown filetype in manifest\n" + line);
			}
			loaded += 1;
			loadProgress = (float)(loaded) / manifest.length;
		}

		if (manifest != null && loaded == manifest.length) {
			loadProgress = 1.01f;
		}
	}

	public static String[] getPartData(String dir, String field) {
		FileHandle resourceMeta = Gdx.files.internal(dir);
		String[] lines = resourceMeta.readString().split("[\\r\\n]");
		String[] ret = null;
		for (int i = 0; i < lines.length; i++) {
			if (lines[i].contains(field)) {
				ret = lines[i].split(": ")[1].split(", ");
			}
		}
		return ret;
	}
}
