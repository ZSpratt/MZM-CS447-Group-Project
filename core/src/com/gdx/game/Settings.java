package com.gdx.game;

import com.badlogic.gdx.Input;

public class Settings {
	public static float gibScale = 1.0f;
    public static float volume = 0.5f;

    public static int up = Input.Keys.W;
    public static int down = Input.Keys.S;
    public static int left = Input.Keys.A;
    public static int right = Input.Keys.D;

    public static int sprint = Input.Keys.SHIFT_LEFT;
    public static int mapSelect = Input.Keys.M;
    public static int pause = Input.Keys.ESCAPE;
    public static int reload = Input.Keys.U;

    public static int repair = Input.Keys.R;
    public static int respawn = Input.Keys.N;
    public static int interact = Input.Keys.E;

    public static int profile = Input.Keys.P;
}
