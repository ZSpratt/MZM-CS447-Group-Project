package com.gdx.game;

import com.badlogic.gdx.math.Polygon;

public class CreatePolygon {
	public static Polygon rect(float width, float height) {
		return rect(width, height, 0, 0);
	}

	public static Polygon rect(float width, float height, float x, float y) {
		float wh = width/2f;
		float hh = height/2f;
		float[] verts = new float[8];
		verts[0] = x - wh;
		verts[1] = y - hh;
		verts[2] = x + wh;
		verts[3] = y - hh;
		verts[4] = x + wh;
		verts[5] = y + hh;
		verts[6] = x - wh;
		verts[7] = y + hh;
		return new Polygon(verts);
	}

	public static Polygon trap(float widthA, float widthB, float height) {
		return trap(widthA, widthB, height, 0, 0);
	}

	public static Polygon trap(float widthA, float widthB, float height, float x, float y) {
		float wha = widthA/2f;
		float whb = widthB/2f;
		float hh = height/2f;
		float[] verts = new float[8];
		verts[0] = x - wha;
		verts[1] = y - hh;
		verts[2] = x + wha;
		verts[3] = y - hh;
		verts[4] = x + whb;
		verts[5] = y + hh;
		verts[6] = x - whb;
		verts[7] = y + hh;
		return new Polygon(verts);
	}

	public static Polygon circle(float radius, int steps) {
		return circle(radius, steps, 0, 0);
	}

	public static Polygon circle(float radius, int steps, float x, float y) {
		float[] verts = new float [steps * 2];
		float deltaTheta = 360f/steps;

		for (int i = 0; i < steps * 2; i+=2) {
			verts[i] = radius * (float) Math.cos(Math.toRadians(deltaTheta * i/2));
			verts[i + 1] = radius * (float) Math.sin(Math.toRadians(deltaTheta * i/2));
		}
		return new Polygon(verts);
	}
}
