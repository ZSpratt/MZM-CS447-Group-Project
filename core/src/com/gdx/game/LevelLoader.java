package com.gdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.*;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Objects.GUI_PARTS.MechCustomMenu;
import com.gdx.game.Objects.GUI_PARTS.ShopMenu;
import com.gdx.game.Objects.Mech;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

public class LevelLoader {
	public static ArrayList<Vector2> playerSpawns = new ArrayList<Vector2>();
	public static ArrayList<Vector2> enemySpawns = new ArrayList<Vector2>();
	public static ArrayList<Vector2> zombieSpawns = new ArrayList<Vector2>();
	public static ArrayList<Vector2> objectives = new ArrayList<Vector2>();
	public static ArrayList<Texture> objectivesImages = new ArrayList<Texture>();
	public static ConcurrentHashMap<String, Teleporter> teleporters = new ConcurrentHashMap<String, Teleporter>();
	public static float tileWidth = 32;
	public static float tileHeight = 32;

	public static void loadLevel(String path) {
		playerSpawns.clear();
		enemySpawns.clear();
		zombieSpawns.clear();
		objectives.clear();
		objectivesImages.clear();
		Game.mainCamera.zoom = 5;
		TiledMap map = new TmxMapLoader().load(path);

		TiledMapTileLayer mapLayer;
		try {
			mapLayer = (TiledMapTileLayer) map.getLayers().get(map.getLayers().getIndex("GroundTiles"));
			for (int y = 0; y < mapLayer.getHeight(); y++) {
				for (int x = 0; x < mapLayer.getWidth(); x++) {
					Cell c = mapLayer.getCell(x, y);
					if (c != null) {
						//System.out.println(c.getRotation() + " : " + c.getFlipHorizontally() + " : " + c.getFlipVertically());
						float width = c.getTile().getTextureRegion().getRegionWidth();
						float height = c.getTile().getTextureRegion().getRegionHeight();
						tileWidth = width;
						tileHeight = height;
						Tile t = new Tile(c.getTile().getTextureRegion(), x * width + width/2, y * height + height/2, -10);
						if (c.getRotation() == 1) {
							t.rot = 90;
						} else if (c.getRotation() == 3) {
							t.rot = -90;
						}
						t.sprite.flip(c.getFlipHorizontally(), c.getFlipVertically());
						t.sprite.setRotation(c.getRotation());
						Game.addGameObject(t);
					}
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		try{
			mapLayer = (TiledMapTileLayer) map.getLayers().get(map.getLayers().getIndex("BackgroundTiles"));
			for (int y = 0; y < mapLayer.getHeight(); y++) {
				for (int x = 0; x < mapLayer.getWidth(); x++) {
					Cell c = mapLayer.getCell(x, y);
					if (c != null) {
						//System.out.println(c.getRotation() + " : " + c.getFlipHorizontally() + " : " + c.getFlipVertically());
						float width = c.getTile().getTextureRegion().getRegionWidth();
						float height = c.getTile().getTextureRegion().getRegionHeight();
						Tile t = new Tile(c.getTile().getTextureRegion(), x * width + width/2, y * height + height/2, -12);
						if (c.getRotation() == 1) {
							t.rot = 90;
						} else if (c.getRotation() == 3) {
							t.rot = -90;
						}
						t.sprite.flip(c.getFlipHorizontally(), c.getFlipVertically());
						t.sprite.setRotation(c.getRotation());
						t.color = new Color(0.5f, 0.5f, 0.5f, 1);
						Game.addGameObject(t);
					}
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		MapObjects objects;
		try {
			objects = map.getLayers().get("Collision").getObjects();
			for (int i = 0; i < objects.getCount(); i++) {
				//System.out.println(objects.get(i));
				MapObject o = objects.get(i);
				if (o instanceof RectangleMapObject) {
					RectangleMapObject r = (RectangleMapObject) o;
					Rectangle rect = r.getRectangle();
					//System.out.println(rect);
					Entity e = new Entity(new Vector2(0, 0), 0, 0, null);
					e.hasPhysics = true;
					e.canMoveObjects = true;
					e.kinematic = true;
					e.collider = CreatePolygon.rect(rect.width, rect.height);
					e.localPos.set(rect.x + rect.width/2, rect.y + rect.height/2);
					e.pos.set(e.localPos);
					e.team = -1;
					Game.addGameObject(e);
				} else if (o instanceof PolygonMapObject) {
					PolygonMapObject p = (PolygonMapObject) o;
					Entity e = new Entity(new Vector2(0, 0), 0, 0, null);
					e.hasPhysics = true;
					e.canMoveObjects = true;
					e.kinematic = true;
					e.collider = p.getPolygon();
					e.localPos.set(p.getPolygon().getX(), p.getPolygon().getY());
					e.pos.set(e.localPos);
					e.team = -1;
					Game.addGameObject(e);
				} else {
					System.out.println(o);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		try {
			objects = map.getLayers().get("UpOnly").getObjects();
			for (int i = 0; i < objects.getCount(); i++) {
				//System.out.println(objects.get(i));
				MapObject o = objects.get(i);
				if (o instanceof RectangleMapObject) {
					RectangleMapObject r = (RectangleMapObject) o;
					Rectangle rect = r.getRectangle();
					//System.out.println(rect);
					Entity e = new Entity(new Vector2(0, 0), 0, 0, null);
					e.hasPhysics = true;
					e.canMoveObjects = true;
					e.kinematic = true;
					e.collider = CreatePolygon.rect(rect.width, rect.height);
					e.localPos.set(rect.x + rect.width/2, rect.y + rect.height/2);
					e.pos.set(e.localPos);
					e.team = -2;
					Game.addGameObject(e);
				} else if (o instanceof PolygonMapObject) {
					PolygonMapObject p = (PolygonMapObject) o;
					Entity e = new Entity(new Vector2(0, 0), 0, 0, null);
					e.hasPhysics = true;
					e.canMoveObjects = true;
					e.kinematic = true;
					e.collider = p.getPolygon();
					e.localPos.set(p.getPolygon().getX(), p.getPolygon().getY());
					e.pos.set(e.localPos);
					e.team = -2;
					Game.addGameObject(e);
				} else {
					System.out.println(o);
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		try {
			objects = map.getLayers().get("InteractCollider").getObjects();
			for (int i = 0; i < objects.getCount(); i++) {
				//System.out.println(objects.get(i));
				MapObject o = objects.get(i);
				GameObject ui = null;
				String message = "";
				if (o.getName().equals("Loadout")) {
					ui = new MechCustomMenu();
					message = "open loadout";
				}
				if (o.getName().equals("Shop")) {
					ui = new ShopMenu();
					message = "browse shop";
				}
				if (o instanceof RectangleMapObject) {
					RectangleMapObject r = (RectangleMapObject) o;
					Rectangle rect = r.getRectangle();
					//System.out.println(rect);

					Polygon p = CreatePolygon.rect(rect.width, rect.height);
					Vector2 pos = new Vector2(rect.x + rect.width/2, rect.y + rect.height/2);

					Game.addGameObject(new InteractBox(pos, message, p, ui));

				} else if (o instanceof PolygonMapObject) {
					PolygonMapObject p = (PolygonMapObject) o;

					Vector2 pos = new Vector2(p.getPolygon().getX(), p.getPolygon().getY());

					Game.addGameObject(new InteractBox(pos, message, p.getPolygon(), ui));
				} else {
					System.out.println(o);
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		try {
			objects = map.getLayers().get("Teleporter").getObjects();
			for (int i = 0; i < objects.getCount(); i++) {
				//System.out.println(objects.get(i));
				MapObject o = objects.get(i);
				if (o instanceof RectangleMapObject) {
					RectangleMapObject r = (RectangleMapObject) o;
					Rectangle rect = r.getRectangle();
					//System.out.println(rect);

					Polygon p = CreatePolygon.rect(rect.width, rect.height);
					Vector2 pos = new Vector2(rect.x + rect.width/2, rect.y + rect.height/2);

					Teleporter t = new Teleporter(pos, o.getProperties().get("Destination").toString(), p);
					Game.addGameObject(t);
					teleporters.put(o.getName(), t);

				} else if (o instanceof PolygonMapObject) {
					PolygonMapObject p = (PolygonMapObject) o;

					Vector2 pos = new Vector2(p.getPolygon().getX(), p.getPolygon().getY());

					Teleporter t = new Teleporter(pos, o.getProperties().get("Destination").toString(), p.getPolygon());
					Game.addGameObject(t);
					teleporters.put(o.getName(), t);
				} else {
					System.out.println(o);
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		try {
			mapLayer = (TiledMapTileLayer) map.getLayers().get(map.getLayers().getIndex("PlayerSpawner"));
			for (int y = 0; y < mapLayer.getHeight(); y++) {
				for (int x = 0; x < mapLayer.getWidth(); x++) {
					Cell c = mapLayer.getCell(x, y);
					if (c != null) {
						playerSpawns.add(new Vector2((x * tileWidth) + tileWidth,  y * tileHeight));
					}
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		try {
			mapLayer = (TiledMapTileLayer) map.getLayers().get(map.getLayers().getIndex("EnemySpawner"));
			for (int y = 0; y < mapLayer.getHeight(); y++) {
				for (int x = 0; x < mapLayer.getWidth(); x++) {
					Cell c = mapLayer.getCell(x, y);
					if (c != null) {
						enemySpawns.add(new Vector2((x * tileWidth) + tileWidth,  y * tileHeight));
					}
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		try {
			mapLayer = (TiledMapTileLayer) map.getLayers().get(map.getLayers().getIndex("ObjectiveSpawner"));
			for (int y = 0; y < mapLayer.getHeight(); y++) {
				for (int x = 0; x < mapLayer.getWidth(); x++) {
					Cell c = mapLayer.getCell(x, y);
					if (c != null) {
						objectives.add(new Vector2(x * tileWidth + tileWidth - c.getTile().getTextureRegion().getTexture().getWidth()/2,  y * tileHeight));
						objectivesImages.add(c.getTile().getTextureRegion().getTexture());
					}
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		try {
			mapLayer = (TiledMapTileLayer) map.getLayers().get(map.getLayers().getIndex("ZombieSpawner"));
			for (int y = 0; y < mapLayer.getHeight(); y++) {
				for (int x = 0; x < mapLayer.getWidth(); x++) {
					Cell c = mapLayer.getCell(x, y);
					if (c != null) {
						for (int i = 0; i < 32; i += 4) {
							zombieSpawns.add(new Vector2((x * tileWidth) + i,  y * tileHeight + 7));
						}
					}
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}

		try {
			mapLayer = (TiledMapTileLayer) map.getLayers().get(map.getLayers().getIndex("LightMap"));
			Game.ambientLightColor.set(0.25f, 0.25f, 0.25f, 1);
			for (int y = 0; y < mapLayer.getHeight(); y++) {
				for (int x = 0; x < mapLayer.getWidth(); x++) {
					Cell c = mapLayer.getCell(x, y);
					if (c != null) {
						//System.out.println(c.getRotation() + " : " + c.getFlipHorizontally() + " : " + c.getFlipVertically());
						LightTile t = new LightTile(c.getTile().getTextureRegion(), x * tileWidth, y * tileHeight, -12);
						if (c.getRotation() == 1) {
							t.rot = 90;
						} else if (c.getRotation() == 3) {
							t.rot = -90;
						}
						t.sprite.flip(c.getFlipHorizontally(), c.getFlipVertically());
						t.sprite.setRotation(c.getRotation());
						t.color = new Color(Color.WHITE);
						Game.addGameObject(t);
					}
				}
			}
		}catch (Exception e) {
			//e.printStackTrace();
		}
	}

	private static class Teleporter extends Entity {
		String target;
		StringDraw teleText;
		GUIFillBar teleProgress;

		boolean mechOn = false;
		Mech mech = null;
		float teleTime = 0;

		public Teleporter(Vector2 pos, String target, Polygon collider) {
			super(pos, 0, 500, null);
			this.target = target;
			team = -100;
			this.collider = collider;
			this.hasPhysics = true;
			this.canMoveObjects = false;
			this.kinematic = true;

			teleText = new StringDraw("Teleport Interaction", 0, 0, 0, 4);
			teleProgress = new GUIFillBar(0, 0, 200, 8, 0);
			teleProgress.emptyColor = new Color(Color.BLACK);
			teleProgress.midColor = new Color(Color.GRAY);
			teleProgress.fullColor = new Color(Color.WHITE);
		}

		@Override
		public void update() {

			teleText.enabled = mechOn;
			if (teleText.enabled) {
				teleText.str = String.format("Hold [%s] to teleport to [%s]", Input.Keys.toString(Settings.interact), target);
				teleText.update();
				teleText.lateUpdate();
			}

			if (mechOn && Gdx.input.isKeyPressed(Settings.interact)) {
				teleTime += Time.deltaTime;
			} else {
				teleTime = 0;
			}

			if (teleTime >= 0) {
				teleProgress.enabled = true;
				teleProgress.fill = teleTime/1f;

				if (teleTime >= 1) {
					teleProgress.enabled = false;
					teleText.enabled = false;
					teleTime = 0;
					mechOn = false;
					mech.localPos.set(LevelLoader.teleporters.get(target).pos);
					mech = null;
				}
			}

		}

		@Override
		public void renderGUI(Batch batch) {
			if (teleText.enabled) {
				teleText.pos.set(0, -132);
				teleText.renderGUI(batch);
				teleProgress.pos.set(0, -148);
				teleProgress.renderGUI(batch);
			}
		}

		@Override
		public void preparePhysics() {
			super.preparePhysics();
			mechOn = false;
		}

		@Override
		public void onCollision(Entity other) {
			super.onCollision(other);
			if (other instanceof Mech) {
				if (((Mech) other).isPlayer && other.isLocal && !((Mech) other).delete) {
					mech = (Mech) other;
					mechOn = true;
				}
			}
		}
	}
}
