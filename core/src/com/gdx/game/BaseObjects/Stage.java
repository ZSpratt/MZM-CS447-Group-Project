package com.gdx.game.BaseObjects;

import com.gdx.game.Game;
import com.gdx.game.ResourceManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Stage extends GameObject {
    public String[] preloadSprites;
    public String[] preloadSounds;
    public boolean stageOver = false;
    public boolean restart = false;

    public Stage() {
        super(0, 0, 0, 0);
    }

    public void startStage() {
        Game.clearAllObjects();
        Game.addGameObject(this);

        if (preloadSprites != null) {
            for (int i = 0; i < preloadSprites.length; i++) {
                ResourceManager.loadImage(preloadSprites[i]);
            }
        }

        if (preloadSounds != null) {
            for (int i = 0; i < preloadSounds.length; i++) {
                ResourceManager.loadSound(preloadSounds[i]);
            }
        }
        onStartStage();
    }

    public void onStartStage() {
        ResourceManager.getSound("Sounds/music.wav").stop();
        ResourceManager.getSound("Sounds/music.wav").loop();
    }


    public void endStage() {
        stageOver = true;
        onEndStage();
    }

    public void onEndStage() {

    }

    public synchronized String serialize() {
        if (restart) {
			restart = false;
            return "STG  " + this.toString() + Game.netID + "  Restart";
        } else {
            return "STG  " + this.toString() + Game.netID;
        }
    }

    public synchronized void deSerialize(String[] s) {
        if (!Game.isServer) {
            if (s.length >= 3 && s[2].equals("Restart")) {
                try {
                    Game.setStage(this.getClass().getConstructor().newInstance());
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            } else {
				String type = s[1].split("@")[0];
				//System.out.println("Message Stage Type\n" + type);
				//System.out.println(this.getClass().getName());
				if (!this.getClass().getName().equals(type)) {
					try {
						Class<?> objClass = Class.forName(type);
						Constructor<?> objConstructor = objClass.getConstructor();
						//System.out.println(objConstructor);
						Game.setStage((Stage) objConstructor.newInstance());
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
            //System.out.println(s[1]);
        }
    }
}
