package com.gdx.game.BaseObjects;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;

public interface Physics {
	void onCollision(Entity other);
	void preparePhysics();
	void pushBack(Vector2 a, float d, Entity other);
	void checkCollision(Entity b);
	Polygon getCollider();
	boolean collisionAllowed(Entity other);
	boolean hasPhysics();
}
