package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.*;
import com.gdx.game.ResourceManager;
import com.gdx.game.Game;
import com.gdx.game.Time;

public class Entity extends GameObject implements Drawable, DrawableGUI, Physics {
    public Sprite sprite;
    public Polygon collider;
    public Color color;
	public boolean isLocal = true;

    public Vector2 velScale = new Vector2(0, 0);
    public Vector2 velocity = new Vector2();
    public boolean kinematic = true;
	public boolean canMoveObjects = true;
	public int team = 0;

    public float size;

    public int collisionLayer = -1;
    public boolean hasPhysics = true;

    public boolean mirrorX = false;

    public Entity(Vector2 pos, float angle, int z, GameObject parent) {
        super(pos, angle, z, parent);
        color = Color.WHITE;
    }

    public void setSprite(String res) {
        sprite = new Sprite(ResourceManager.getImage(res));
        float xoff = sprite.getWidth() / 2;
        float yoff = sprite.getHeight() / 2;
        size = sprite.getWidth() * sprite.getWidth() + sprite.getHeight() * sprite.getHeight();
        size = (float) Math.sqrt(size);
        size /= 2f;
    }

    @Override
    public void render(Batch batch) {
		if (sprite != null) {
			if (pos.x + offset.x + sprite.getWidth() / 2 * Game.mainCamera.zoom > Game.mainCamera.position.x - Game.GAME_WIDTH / 2 * Game.mainCamera.zoom) {
				if (pos.x + offset.x - sprite.getWidth() / 2 * Game.mainCamera.zoom < Game.mainCamera.position.x + Game.GAME_WIDTH / 2 * Game.mainCamera.zoom) {
					if (pos.y + offset.y + sprite.getHeight() / 2 * Game.mainCamera.zoom > Game.mainCamera.position.y - Game.GAME_HEIGHT / 2 * Game.mainCamera.zoom) {
						if (pos.y + offset.y - sprite.getHeight() / 2 * Game.mainCamera.zoom < Game.mainCamera.position.y + Game.GAME_HEIGHT / 2 * Game.mainCamera.zoom) {
							//graphics.draw(getCollider());
							sprite.setRotation(rot);
							sprite.setScale(scale);
							sprite.flip(mirrorX, false);
							sprite.setCenter(pos.x + offset.x, pos.y + offset.y);
							sprite.setColor(color);
							sprite.draw(batch);
						}
					}
				}
			}
		}
    }

    @Override
    public int z() {
        return z;
    }

    @Override
    public void renderGUI(Batch batch) {
        return;
    }

    @Override
    public int guiz() {
        return z;
    }

    @Override
    public void lateUpdate() {
		if (velScale != null) {
			if (!kinematic && hasPhysics) {
				velocity.add(new Vector2(Game.gravity).scl(Time.deltaTime));
			}
			velScale.set(velocity);
			velScale.scl(Time.deltaTime);
			localPos.add(velScale);
		}
        super.lateUpdate();
    }

    @Override
    public void onCollision(Entity other) {

    	if (other.team == -1) {
    		if (delta.y != 0) {
				velocity.set(0, 0);
			}
    	}
    }

	@Override
	public void preparePhysics() {
		if (collider != null) {
			collider.setPosition(pos.x, pos.y);
			collider.setRotation(rot);
			collider.setScale(scale, scale);
		}
	}

	public Vector2 delta = new Vector2();
	@Override
	public void pushBack(Vector2 a, float d, Entity other) {
		if (other.team == -1) {
			delta.set(a);
			delta.scl(d);
			localPos.add(delta);
			pos.add(delta);
		}
	}

	Intersector.MinimumTranslationVector mv = new Intersector.MinimumTranslationVector();
	Rectangle rec = new Rectangle();
	Polygon poly = new Polygon();

	@Override
    public void checkCollision(Entity b) {
		if (collisionAllowed(b)) {
			Polygon colliderA = getCollider();
			Polygon colliderB = b.getCollider();

			if (colliderA != null && colliderB != null) {
				if (Intersector.intersectRectangles(colliderA.getBoundingRectangle(), colliderB.getBoundingRectangle(), rec)) {
					if (!Intersector.intersectPolygons(colliderA, colliderB, poly)) {
						Intersector.overlapConvexPolygons(colliderA.getTransformedVertices(), colliderB.getTransformedVertices(), mv);
						delta.set(0, 0);
						b.delta.set(0, 0);
						if (mv.depth > 0) {
							if (canMoveObjects && b.canMoveObjects) {
								if ((team != -2 && b.team != -2) || (mv.normal.y != 0 && (mv.normal.y < 0))) {
									if (kinematic && b.kinematic) {

									} else if (!kinematic && b.kinematic) {
										pushBack(mv.normal, mv.depth, b);
									} else if (kinematic && !b.kinematic) {
										b.pushBack(mv.normal, -mv.depth, this);
									} else if (kinematic && b.kinematic) {
										pushBack(mv.normal, mv.depth / 2, b);
										b.pushBack(mv.normal, -mv.depth / 2, this);
									}
								}
							}
							onCollision(b);
							b.onCollision(this);
						}
					}
				}
			}
        }
    }

	@Override
	public void onDelete() {
		super.onDelete();
	}

	@Override
	public boolean collisionAllowed(Entity other) {
		if (other.team != this.team) {
			return true;
		} else {
			return false;
		}
	}

	@Override
    public Polygon getCollider() {
        return collider;
    }

    @Override
    public boolean hasPhysics() {
        return hasPhysics;
    }
}