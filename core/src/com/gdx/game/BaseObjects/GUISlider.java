package com.gdx.game.BaseObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.gdx.game.Game;

public class GUISlider extends GUIBox {

	public GUIBox slide;
	private GUIBox numberDsiplay;
	private StringDraw number;


	private float delta = 0.5f;
	private boolean active = false;
	private boolean clicked = false;
	private float actHeight = 8;

	public float minValue = 0;
	public float maxValue = 100;
	public boolean showNumber = true;
	public float value;
	public boolean intSlider = false;

	public GUISlider(float x, float y, float width, float height, int z) {
		super(x, y, width, 0, 0);
		slide = new GUIBox(0, 0, height, height, 1);
		number = new StringDraw("bum", 0, 0, 0, 4);
		numberDsiplay = new GUIBox(0, 0, 64, height, 0);
	}

	public void setValue(float value) {
		this.value = value;
		if (intSlider) {
			this.value = Math.round(value);
		}
		delta = (this.value - minValue) / (maxValue - minValue);
	}

	@Override
	public void update() {
		number.update();

		if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			if (!clicked && ! active) {
				if (Math.abs(Game.GUImouseX - localPos.x) <= width / 2 + cornerWidth && Math.abs(Game.GUImouseY - localPos.y) <= slide.height / 2 + slide.cornerHeight) {
					active = true;
				}
			} else if (active) {
				delta = (Game.GUImouseX - pos.x + width/2) / width;
				if (delta < 0) {
					delta = 0;
				}
				if (delta > 1) {
					delta = 1;
				}
			}
			clicked = true;
		} else {
			active = false;
			clicked = false;
		}

		value = minValue + (maxValue - minValue) * delta;
		if (intSlider) {
			value = Math.round(value);
			delta = (value - minValue) / (maxValue - minValue);
		}
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		slide.localPos.set(pos.x - width/2 + width * delta, pos.y);
		slide.pos.set(slide.localPos);
		numberDsiplay.localPos.set(pos.x + width/2 + 64, pos.y);
		numberDsiplay.pos.set(numberDsiplay.localPos);
		number.pos.set(pos.x + width/2 + 64, pos.y);
		slide.renderGUI(batch);
		if (showNumber) {
			if (intSlider) {
				number.str = String.format("%d", (int)value);
			} else {
				number.str = String.format("%.2f", value);
			}
			number.update();
			numberDsiplay.renderGUI(batch);
			number.renderGUI(batch);
		}
	}
}
