package com.gdx.game.BaseObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.gdx.game.Game;
import com.gdx.game.InProcess;

import java.security.Key;

/**
 * Created by zachm on 11/8/2017.
 */
public class GUITextBox extends GUIBox{
	public String value = "";
	public BitmapFont font = Game.fonts[0];
	public StringDraw stringDraw;
	public boolean active = false;
	public boolean clicked = false;
	public GUIBox subBox;
	public int maxLength;

	public Color activeColor;
	public Color inactiveColor;

	public GUITextBox(float x, float y, float width, float height, String value, int maxLength, int z) {
		super(x, y, width, height, z);
		activeColor = color;
		inactiveColor = new Color(color.r - 0.5f, color.g - 0.5f, color.b - 0.5f, 1.0f);

		stringDraw = new StringDraw(value, x, y, z, 4);
		subBox = new GUIBox(x, y, width - 10, height - 10, z + 1);
		subBox.color = new Color(color.r - 0.125f, color.g - 0.125f, color.b - 0.125f, 1.0f);
		this.maxLength = maxLength;

		stringDraw.update();
		subBox.update();
		this.value = value;
	}

	@Override
	public void update() {
		if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			if (!clicked && Math.abs(Game.GUImouseX - localPos.x) < width/2 + cornerWidth && Math.abs(Game.GUImouseY - localPos.y) < height/2 + cornerHeight) {
				active = true;
			} else if (!(Math.abs(Game.GUImouseX - localPos.x) < width/2 + cornerWidth && Math.abs(Game.GUImouseY - localPos.y) < height/2 + cornerHeight)){
				active = false;
			}
			clicked = true;
		} else {
			clicked = false;
		}

		if (active) {
			color = activeColor;
			super.update();
			if (Gdx.input.isKeyJustPressed(Input.Keys.BACKSPACE)) {
				if (value.length() > 0) {
					value = value.substring(0, value.length() - 1);
				}
			} else if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
				if (value.length() < maxLength && Character.isDefined(InProcess.lastChar)) {
					value += InProcess.lastChar;
				}
			}

			subBox.update();
			stringDraw.setFont(font);
			stringDraw.color = color;
			stringDraw.str = value;
			stringDraw.update();
		} else {
			color = inactiveColor;
		}
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		subBox.renderGUI(batch);
		stringDraw.renderGUI(batch);
	}
}
