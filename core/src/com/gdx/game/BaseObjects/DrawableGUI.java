package com.gdx.game.BaseObjects;


import com.badlogic.gdx.graphics.g2d.Batch;

public interface DrawableGUI {
    void renderGUI(Batch batch);

    int guiz();
}
