package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gdx.game.Game;
import com.gdx.game.ResourceManager;

public class Tile extends GameObject implements Drawable{
    public Sprite sprite;
    public Color color;
    public Tile(String res, float x, float y, int z) {
        super(x, y, 0, z);
            sprite = new Sprite(ResourceManager.getImage(res));
        color = Color.WHITE;
    }

    public Tile(TextureRegion t, float x, float y, int z) {
        super(x, y, 0, z);
        sprite = new Sprite(t);
        color = Color.WHITE;
    }

    @Override
    public void render(Batch batch) {
        if (pos.x + offset.x + sprite.getWidth()/2 * Game.mainCamera.zoom > Game.mainCamera.position.x - Game.GAME_WIDTH/2 * Game.mainCamera.zoom) {
            if (pos.x + offset.x - sprite.getWidth()/2 * Game.mainCamera.zoom < Game.mainCamera.position.x + Game.GAME_WIDTH / 2 * Game.mainCamera.zoom) {
                if (pos.y + offset.y + sprite.getHeight()/2 * Game.mainCamera.zoom > Game.mainCamera.position.y - Game.GAME_HEIGHT/2 * Game.mainCamera.zoom) {
                    if (pos.y  + offset.y - sprite.getHeight()/2 * Game.mainCamera.zoom < Game.mainCamera.position.y + Game.GAME_HEIGHT / 2 * Game.mainCamera.zoom) {
                        sprite.setScale(scale);
                        sprite.setRotation(rot);
                        sprite.setCenter(pos.x + offset.x, pos.y + offset.y);
                        sprite.setColor(color);
                        sprite.draw(batch);
                    }
                }
            }
        }
    }

    @Override
    public void lateUpdate() {

    }

    @Override
    public void setParent(GameObject newParent) {

    }

    @Override
    public int z() {
        return z;
    }
}
