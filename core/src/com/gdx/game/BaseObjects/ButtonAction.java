package com.gdx.game.BaseObjects;

/**
 * Created by zachm on 11/8/2017.
 */
public interface ButtonAction {
	public void onClick();
}
