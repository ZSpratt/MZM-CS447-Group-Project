package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gdx.game.ResourceManager;
import com.gdx.game.Time;
/**
 * Created by zachm on 9/13/2017.
 */
public class AnimatedTile extends Tile {
    public Sprite currentFrame;
    public Texture[] frames;
    public Animation anim;
    private boolean looping;

    public float stateTime = 0;

    public AnimatedTile(String res, float x, float y, int z) {
        super(res, x, y, z);
        currentFrame = new Sprite(ResourceManager.getImage(res));
    }

    public void setAnim(Animation anim, float time) {
        this.anim = anim;
        this.stateTime = time;
    }

    @Override
    public void render(Batch batch) {
        if (anim != null) {
            stateTime += Time.deltaTime;
            currentFrame = new Sprite((TextureRegion) anim.getKeyFrame(stateTime, looping));

            currentFrame.setScale(scale);
            currentFrame.setRotation(rot);
            currentFrame.setCenter(pos.x + offset.x, pos.y + offset.y);
            currentFrame.draw(batch);
        }
    }
}
