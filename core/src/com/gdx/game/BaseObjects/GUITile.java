package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.gdx.game.ResourceManager;

public class GUITile extends GameObject implements DrawableGUI{
	public Sprite sprite;
	public Color color;
	public GUITile(String res, float x, float y, int z) {
		super(x, y, 0, z);
		sprite = new Sprite(ResourceManager.getImage(res));
		color = Color.WHITE;
	}

	@Override
	public void lateUpdate() {

	}

	@Override
	public void setParent(GameObject newParent) {

	}

	@Override
	public void renderGUI(Batch batch) {
		sprite.setRotation(rot);
		sprite.setScale(scale);
		sprite.setCenter(pos.x + offset.x, pos.y + offset.y);
		sprite.setColor(color);
		sprite.setScale(scale);
		sprite.draw(batch);
	}

	@Override
	public int guiz() {
		return z;
	}
}
