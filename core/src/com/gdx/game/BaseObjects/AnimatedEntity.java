package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.Time;

public class AnimatedEntity extends Entity {
    public Sprite currentFrame;
    public Texture[] frames;
    public Animation anim;
    public String animState = "";
    public boolean looping = true;

    public float stateTime = 0;
    public float playbackSpeed = 1;

    public AnimatedEntity(Vector2 pos, float angle, int z, GameObject parent) {
        super(pos, angle, z, parent);
    }

    public void setAnim(Animation anim, float time, String animState) {
        this.animState = animState;
        setAnim(anim, time);
    }

    public void setAnim(Animation anim, float time) {
    	this.anim = anim;
    	this.stateTime = time;
	}

    @Override
    public void render(Batch batch) {
        if (anim != null) {
            //Wastes some time, but in order for current frame to update, the frame must be drawn... bad code.
            stateTime += Time.deltaTime * playbackSpeed;

            if (!anim.isAnimationFinished(stateTime) | looping) {
                currentFrame = new Sprite((TextureRegion) anim.getKeyFrame(stateTime, looping));
                currentFrame.setScale(scale);
				currentFrame.flip(mirrorX, false);
                currentFrame.setRotation(rot);
                currentFrame.setCenter(pos.x + offset.x, pos.y + offset.y);
                currentFrame.setColor(color);
                currentFrame.draw(batch);
            }
        } else if (sprite != null) {
            super.render(batch);
        }
    }
}
