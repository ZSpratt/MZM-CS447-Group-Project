package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.g2d.Batch;

public interface Drawable {
    void render(Batch graphics);
    int z();
}
