package com.gdx.game.BaseObjects;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class GameObject {
    public Vector2 localPos = new Vector2(), pos = new Vector2(), offset = new Vector2();
    public float localRot, rot;
    public boolean delete, inheritRotation;
    public float scale = 1.0f;

    public int z;
    public GameObject parent = null;
    public ArrayList<GameObject> children = new ArrayList<GameObject>();

    public boolean enabled = true;

    public GameObject(float x, float y, float angle, int z) {
        init(new Vector2(x, y), angle, z, null);
    }

    public GameObject (Vector2 pos, float angle, int z) {
        init(pos, angle, z, null);
    }

    public GameObject (Vector2 pos, float angle, int z, GameObject parent) {
        init(pos, angle, z, parent);
    }

    private void init(Vector2 pos, float angle, int z, GameObject parent) {
        this.localPos.set(pos);
        this.pos.set(pos);
        this.localRot = angle;
        this.rot = angle;
        this.z = z;

        setParent(parent);
    }

    public void earlyUpdate() {

    }

    public void update() {

    }

    public void lateUpdate() {
        if (parent != null) {
            pos.set(localPos);
            pos.rotate(parent.rot);
            pos.add(parent.pos);
            rot = localRot;
            if (inheritRotation) {
                rot += parent.rot;
            }
        } else {
            pos.set(localPos);
            rot = localRot;
        }
    }

    public void setParent(GameObject newParent) {
        if (parent == newParent) {
            lateUpdate();
            return;
        }

        if (newParent == null) {
            localPos.rotate(parent.rot);
            localPos.add(parent.pos);
            rot += parent.rot;
            parent.children.remove(this);
            this.parent = null;
        } else {
            parent = newParent;
            localPos.rotate(-parent.rot);
            localPos.sub(parent.pos);
            parent.children.add(this);
            rot -= parent.rot;
        }
        lateUpdate();
    }

    public void Delete(){
        if (! delete) {
            onDelete();
        }
        delete = true;
        if(children.size() > 0) {
            for (int i = 0; i < children.size(); i++) {
                children.get(i).Delete();
            }
        }
    }

    public void onDelete() {

    }
}
