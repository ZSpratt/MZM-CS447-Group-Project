package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.Game;
import com.gdx.game.Time;

/**
 * Created by Zachary on 2017-09-02.
 */
public class StringDraw extends GameObject implements DrawableGUI {
    public String str;
    public String[] strs;
    public StringDraw[] lines = null;

    public Color color;
    public Vector2 velocity = new Vector2();
    Vector2 velScal = new Vector2();
    public float lifetime = 0;
    float lifetimed = 0;
    float xOff = 0;
    float yOff = 0;
    public BitmapFont font;

    GlyphLayout layout = new GlyphLayout();

    /// Allignment Guide
    /// 0   1   2
    /// 3   4   5
    /// 6   7   8
    public int allignment = 4;
    public StringDraw(String s, float x, float y, int z, int allignment) {
        super(x, y, 0, z);
        drawSet(s, x, y, Color.WHITE, Game.fonts[0], 0, z, allignment);
    }

    public StringDraw(String s, float x, float y, Color col, int z, int allignment) {
        super(x, y, 0, z);
        drawSet(s, x, y, col, Game.fonts[0], 0, z, allignment);
    }

    public StringDraw(String s, float x, float y, Color col, BitmapFont font, int z, int allignment) {
        super(x, y, 0, z);
        drawSet(s, x, y, col, font, 0, z, allignment);
    }

    public StringDraw(String s, float x, float y, Color col, BitmapFont font, float angle, int z, int allignment) {
        super(x, y, angle, z);
        drawSet(s, x, y, col, font, angle, z, allignment);
    }

    private void drawSet(String s, float x, float y, Color col, BitmapFont font, float angle, int z, int allignment) {
        strs = s.split("\n");
        str = strs[0];
        color = new Color(col);
        this.allignment = allignment;

        if (strs.length > 1) {
            lines = new StringDraw[strs.length - 1];
            for (int i = 1; i < strs.length; i++) {
                lines[i - 1] = ((StringDraw) (Game.addGameObject(new StringDraw(strs[i], x, y, col, font, angle, z, allignment))));
            }
        }
        this.font = font;
    }

    public void setFont(BitmapFont font) {
        this.font = font;
        update();
    }

    @Override
    public void update() {
        layout.setText(font, str);
        velScal.set(velocity);
        localPos.add(velScal.scl(Time.deltaTime));
        if (lifetime != 0) {
            lifetimed += Time.deltaTime;
            if (lifetimed >= lifetime) {
                Delete();
            }
            color.a = (1 - (lifetimed / lifetime));
        }
        if (allignment % 3 == 0) {
            xOff = 0;
        }
        if (allignment % 3 == 1) {
            xOff = layout.width / 2;
        }
        if (allignment % 3 == 2) {
            xOff = layout.width;
        }

        if (allignment / 3 == 0) {
            yOff = 0;
        }
        if (allignment / 3 == 1) {
            yOff = layout.height / 2;
        }
        if (allignment / 3 == 2) {
            yOff = layout.height;
        }
        if (lines != null) {
            int lineCount = lines.length + 1;
            float offset = 1 + ((int) (allignment / 3)) * 0.5f;
            for (int i = 0; i < lines.length; i++) {
                lines[i].color = color;
                //lines[i].font = font;
                lines[i].velocity = velocity;
                lines[i].velScal = velScal;
                lines[i].lifetime = lifetime;
                lines[i].lifetimed = lifetimed;
                lines[i].allignment = allignment;
                lines[i].pos.y = pos.y - layout.height * (i + offset);
                lines[i].pos.x = pos.x;
            }
        }
    }

    @Override
    public void renderGUI(Batch batch) {
        font.setColor(color);
        font.draw(batch, str, pos.x - xOff, pos.y + yOff);

    }

    @Override
    public int guiz() {
        return z;
    }
}
