package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.gdx.game.ResourceManager;
import javafx.scene.control.Button;

/**
 * Created by Zachary on 2017-12-04.
 */
public class GUICheckbox extends GUIButton {

	public boolean toggled = false;
	Texture point;
	public GUICheckbox(float x, float y, float width, float height, int z) {
		super(x, y, width, height, "", null, z);
		final ButtonAction toggle = new ButtonAction() {
			@Override
			public void onClick() {
				toggled = !toggled;
			}
		};
		action = toggle;

		point = ResourceManager.getImage("GUI/Point.png");
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		if (toggled) {
			batch.draw(point, pos.x - point.getWidth()/2, pos.y - point.getHeight()/2);
		}
	}
}
