package com.gdx.game.BaseObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.gdx.game.Game;
import com.gdx.game.InProcess;

import java.security.Key;

public class GUIButton extends GUIBox{
	private BitmapFont font = Game.fonts[0];
	private StringDraw stringDraw;
	public boolean active = false;
	GUIBox subBox;
	public int maxLength;

	public Color activeColor;
	public Color inactiveColor;
	public ButtonAction action;
	public String label = "";
	public boolean disabled = false;

	private boolean clicked = false;

	public GUIBox popup;

	public GUIButton(float x, float y, float width, float height, String value, ButtonAction action, int z) {
		super(x, y, width, height, z);
		activeColor = color;
		inactiveColor = new Color(color.r - 0.5f, color.g - 0.5f, color.b - 0.5f, 1.0f);

		stringDraw = new StringDraw(value, x, y, z, 4);
		subBox = new GUIBox(x, y, width - 10, height - 10, z + 1);
		subBox.color = new Color(color.r - 0.125f, color.g - 0.125f, color.b - 0.125f, 1.0f);
		this.maxLength = maxLength;
		label = value;

		stringDraw.update();
		subBox.update();
		this.action = action;
	}

	@Override
	public void update() {
		//Game.addDebugLine(localPos.x, localPos.y, localPos.x + width/2, localPos.y + height/2);
		//System.out.println("X : " + Game.GUImouseX + ", " + localPos.x + " : " + Math.abs(Game.GUImouseX - localPos.x));
		//System.out.println("Y : " + Game.GUImouseY + ", " + localPos.y + " : " + Math.abs(Game.GUImouseY - localPos.y));
		if (Math.abs(Game.GUImouseX - localPos.x) <= width / 2 + cornerWidth && Math.abs(Game.GUImouseY - localPos.y) <= height / 2 + cornerHeight) {
			active = true;
			//System.out.println(this);
		} else {
			active = false;
		}

		active = active && !disabled;

		if (active) {
			color = activeColor;
			super.update();
		} else {
			color = inactiveColor;
		}

		if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			if (!clicked && active && action != null) {
				action.onClick();
			}
			clicked = true;
			//System.out.println(this.toString() + " Clicked");
		} else {
			clicked = false;
		}
		subBox.update();
		stringDraw.str = label;
		stringDraw.setFont(font);
		stringDraw.color = color;
		stringDraw.update();

		if (popup != null && active) {
			popup.update();
			popup.lateUpdate();
		}
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		batch.setColor(Color.WHITE);
		subBox.renderGUI(batch);
		batch.setColor(Color.WHITE);
		stringDraw.renderGUI(batch);
		batch.setColor(Color.WHITE);

		if (popup != null && active) {
			popup.renderGUI(batch);
		}
	}
}
