package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gdx.game.Operations;
import com.gdx.game.ResourceManager;

/**
 * Created by Zachary on 2017-11-24.
 */
public class GUIFillBar extends GUIBox {
	public float fill = 0.5f;
	public boolean soldiBack = false;
	Texture fillTexture;
	public Color backColor = new Color(Color.BLACK);
	public Color fullColor = new Color(Color.GREEN);
	public Color midColor = new Color(Color.YELLOW);
	public Color emptyColor = new Color(Color.RED);

	public GUIFillBar(float x, float y, float width, float height, int z) {
		super(x, y, width, height, z);
		fillTexture = ResourceManager.getImage("GUI/Fill.png");
	}

	@Override
	public void setResource(String res, int cornerWidth, int cornerHeight) {
		super.setResource(res, cornerWidth, cornerHeight);
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		if (soldiBack) {
			batch.setColor(backColor);
			batch.draw(fillTexture, pos.x - width / 2, pos.y - height / 2, width, height);
		}

		if (fill >= 0.5f) {
			batch.setColor(Operations.interpolateColor(midColor, fullColor, (fill - 0.5f) * 2));
		} else {
			batch.setColor(Operations.interpolateColor(emptyColor, midColor, fill * 2));
		}
		batch.draw(fillTexture, pos.x - width/2, pos.y - height/2, width * fill, height);
		batch.setColor(Color.WHITE);
	}
}
