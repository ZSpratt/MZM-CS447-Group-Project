package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gdx.game.ResourceManager;

/**
 * Created by zachm on 10/17/2017.
 */
public class GUIBox extends GameObject implements DrawableGUI {

	TextureRegion[][] parts;
	public float width, height;

	public int cornerHeight, cornerWidth;
	public Color color = Color.WHITE;
	public GUIBox(float x, float y, float width, float height, int z) {
		super(x, y, 0, z);

		this.width = width;
		this.height = height;

		setResource("GUI/9Square.png", 3, 3);
	}

	public void setResource(String res, int cornerWidth, int cornerHeight) {
		Texture box = ResourceManager.getImage(res);
		this.cornerWidth = cornerWidth;
		this.cornerHeight = cornerHeight;
		int remWidth = box.getWidth() - 2 * cornerWidth;
		int remHeight = box.getHeight() - 2 * cornerHeight;
		parts = new TextureRegion[3][3];

		parts[0][0] = new TextureRegion(box, 0, 0, cornerWidth, cornerHeight);
		parts[1][0] = new TextureRegion(box, cornerWidth, 0, remWidth, cornerHeight);
		parts[2][0] = new TextureRegion(box, cornerWidth + remWidth, 0, cornerWidth, cornerHeight);

		parts[0][1] = new TextureRegion(box, 0, cornerHeight, cornerWidth, remHeight);
		parts[1][1] = new TextureRegion(box, cornerWidth, cornerHeight, remWidth, remHeight);
		parts[2][1] = new TextureRegion(box, cornerWidth + remWidth, cornerHeight, cornerWidth, remHeight);

		parts[0][2] = new TextureRegion(box, 0, cornerHeight + remHeight, cornerWidth, cornerHeight);
		parts[1][2] = new TextureRegion(box, cornerWidth, cornerHeight + remHeight, remWidth, cornerHeight);
		parts[2][2] = new TextureRegion(box, cornerWidth + remWidth, cornerHeight + remHeight, cornerWidth, cornerHeight);
	}

	@Override
	public void update() {
	}

	@Override
	public void renderGUI(Batch batch) {
		float xOff = -width/2;
		float yOff = -height/2;
		batch.setColor(color);

		batch.draw(parts[0][0], pos.x + xOff - cornerWidth, pos.y + yOff + height, cornerWidth, cornerHeight);
		batch.draw(parts[1][0], pos.x + xOff, pos.y + yOff + height, width, cornerHeight);
		batch.draw(parts[2][0], pos.x + xOff + width, pos.y + yOff + height, cornerWidth, cornerHeight);

		batch.draw(parts[0][1], pos.x + xOff - cornerWidth, pos.y + yOff, cornerWidth, height);
		batch.draw(parts[1][1], pos.x + xOff, pos.y + yOff, width, height);
		batch.draw(parts[2][1], pos.x + xOff + width, pos.y + yOff, cornerWidth, height);

		batch.draw(parts[0][2], pos.x + xOff - cornerWidth, pos.y + yOff - cornerHeight, cornerWidth, cornerHeight);
		batch.draw(parts[1][2], pos.x + xOff, pos.y + yOff - cornerHeight, width, cornerHeight);
		batch.draw(parts[2][2], pos.x + xOff + width, pos.y + yOff - cornerHeight, cornerWidth, cornerHeight);

		batch.setColor(Color.WHITE);
	}

	@Override
	public int guiz() {
		return z;
	}
}
