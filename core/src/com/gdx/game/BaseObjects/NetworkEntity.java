package com.gdx.game.BaseObjects;

import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.Game;

/**
 * Created by zachm on 11/8/2017.
 */
public class NetworkEntity extends Entity {
	public String netId;
	public int updateFreq = 1;
	public int updateCount = 0;

	public boolean updateFrame = false;
	public NetworkEntity(Vector2 pos, float angle, int z, GameObject parent) {
		super(pos, angle, z, parent);
		isLocal = true;
		netId = this.toString() + Game.netID;
	}

	public NetworkEntity(String s) {
		super(new Vector2(0, 0), 0, 0, null);
		isLocal = false;
		String[] data = s.split("  ");
		netId = data[0];
		deSerialize(data);
	}

	public synchronized String serialize() {
		if (! updateFrame) {
			updateFrame = true;
			updateCount = (updateCount + 1) % updateFreq;
		}
		if (isLocal) {
			netId = this.toString() + Game.netID;
		}
		String out = "";
		if (delete) {
			out += "DEL  ";
		}
		out += netId + "  " + team + "  " + pos.x + "  " + pos.y + "  " + velocity.x + "  " + velocity.y;
		return out;
	}

	public synchronized void deSerialize(String[] s) {
		updateFrame = false;
		team = Integer.parseInt(s[1]);
		localPos.x = Float.parseFloat(s[2]);
		localPos.y = Float.parseFloat(s[3]);
		velocity.x = Float.parseFloat(s[4]);
		velocity.y = Float.parseFloat(s[5]);
	}
}
