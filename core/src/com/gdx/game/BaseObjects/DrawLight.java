package com.gdx.game.BaseObjects;

import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by zachm on 10/6/2017.
 */
public interface DrawLight {
	void renderLight(Batch graphics);

	int lightz();
}
