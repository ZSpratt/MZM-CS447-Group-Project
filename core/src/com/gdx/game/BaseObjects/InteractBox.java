package com.gdx.game.BaseObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.Game;
import com.gdx.game.Operations;
import com.gdx.game.Settings;

/**
 * Created by Zachary on 2017-12-02.
 */
public class InteractBox extends Entity {
	String message = "";
	StringDraw drawString;

	GameObject ui;
	boolean showing = false;

	public InteractBox(Vector2 pos, String message, Polygon coll, GameObject ui) {
		super(pos, 0, 100, null);
		drawString = new StringDraw("click button", pos.x, pos.y, 0, 4);
		drawString.color.set(0.75f, 0.75f, 1.0f, 1.0f);
		collider = coll;
		this.ui = ui;
		this.message = message;
		team = -100;
		hasPhysics = true;
		kinematic = true;
		canMoveObjects = false;
	}


	@Override
	public void update() {
		if (!Game.paused && !showing && Operations.CompareVector(pos, new Vector2(Game.mainCamera.position.x, Game.mainCamera.position.y), 512)) {
			collider.setPosition(pos.x, pos.y);
			drawString.str = "Press [" + Input.Keys.toString(Settings.interact) + "]";
			if (message != "") {
				drawString.str += " to " + message;
			}
			else{
				drawString.str = "Your mission: destroy SNAI and free the world";
			}
			drawString.enabled = getCollider().contains(Game.mouseX, Game.mouseY);
			drawString.update();
			drawString.lateUpdate();

			if (drawString.enabled && Gdx.input.isKeyJustPressed(Settings.interact)) {
				if (ui != null) {
					showing = true;
					ui.delete = false;
					Game.addGameObject(ui);
					Game.paused = true;
				}
			}
		} else {
			drawString.enabled = false;
			if (showing && this.ui != null) {
				if (Gdx.input.isKeyJustPressed(Settings.pause)) {
					ui.Delete();
					Game.paused = false;
					showing = false;
				}
			}
		}
		drawString.pos.set(Operations.worldToCamera(new Vector2(pos).add(0, -48)));
	}

	@Override
	public void lateUpdate() {
		super.lateUpdate();

	}

	@Override
	public void renderGUI(Batch batch) {
		if (drawString.enabled) {
			drawString.renderGUI(batch);
		}
	}
}
