package com.gdx.game;

import com.badlogic.gdx.InputProcessor;

/**
 * Created by zachm on 10/2/2017.
 */
public class InProcess implements InputProcessor {
	public static int scroll;
	public static char lastChar;
	public static int lastKey = 0;
	@Override
	public boolean keyDown(int keycode) {
		lastKey = keycode;
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {

		lastKey = keycode;return false;
	}

	@Override
	public boolean keyTyped(char character) {
		lastChar = character;
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		InProcess.scroll = amount;
		return false;
	}
}
