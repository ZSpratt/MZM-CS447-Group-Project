package com.gdx.game;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Hashtable;

public class SoundManager {
	public static boolean disableSounds = false;

	private static ArrayList<SoundPacket> soundEffects = new ArrayList<SoundPacket>();
	public static int overlapLimit = 100;
	private static Hashtable<Sound, Integer> soundCount = new Hashtable<Sound, Integer>();

	public static float calculatePan(Vector2 position) {
		float xOffset = position.x - Game.mainCamera.position.x;
		xOffset = xOffset/2048f;
		if (xOffset > 1) {
			xOffset = 1;
		}

		if (xOffset < -1) {
			xOffset = -1;
		}
		return xOffset;
	}

	public static float calculateVolume(Vector2 position) {
		float xOffset = position.x - Game.mainCamera.position.x;
		float yOffset = position.y - Game.mainCamera.position.y;

		float dist = xOffset * xOffset + yOffset * yOffset;
		dist = (float) Math.sqrt(dist);
		float vol =  (2048 - dist)/2048;
		if (vol > 1) {
			vol = 1;
		}

		if (vol < 0) {
			vol = 0;
		}
		return vol;
	}

	public static long playSound(String playerID, Sound s, float volume) {
		if (!disableSounds && (!soundCount.containsKey(s) || soundCount.get(s) < overlapLimit)) {
			SoundPacket packet = new SoundPacket();
			packet.id = s.play(volume, 1 * Settings.volume, 0);
			packet.sound = s;
			packet.volume = volume;
			packet.staticSound = true;

			soundEffects.add(packet);
			if (soundCount.containsKey(s)) {
				soundCount.put(s, soundCount.get(s) + 1);
			} else {
				soundCount.put(s, 1);
			}

			return packet.id;
		}
		return -1;
	}

	public static long playSound(String playerID, Sound s, float volume, Vector2 position) {
		if (!disableSounds && (!soundCount.containsKey(s) || soundCount.get(s) < overlapLimit)) {
			SoundPacket packet = new SoundPacket();
			packet.sound = s;
			packet.volume = volume;
			packet.position = new Vector2(position);
			packet.id = s.play(volume, volume * calculateVolume(position) * Settings.volume, calculatePan(position));

			soundEffects.add(packet);

			if (soundCount.containsKey(s)) {
				soundCount.put(s, soundCount.get(s) + 1);
			} else {
				soundCount.put(s, 1);
			}
			return packet.id;
		}
		return 0;
	}

	public static void updateSounds() {
		for (int i = soundEffects.size() -1 ; i >= 0; i--) {
			SoundPacket sound = soundEffects.get(i);
			if (!sound.staticSound) {
				sound.sound.setPan(sound.id, calculatePan(sound.position), sound.volume * calculateVolume(sound.position) * Settings.volume);
			} else {
				sound.sound.setVolume(sound.id, sound.volume * Settings.volume);
			}
			sound.lifetime += Time.deltaTime;

			if (sound.lifetime > 1  && !sound.dropped) {
				sound.dropped = true;
				if (soundCount.containsKey(sound.sound)) {
					if (soundCount.get(sound.sound) > 0) {
						soundCount.put(sound.sound, soundCount.get(sound.sound) - 1);
					}
				}
			}

			if (sound.lifetime > 10) {
				soundEffects.remove(i);
			}
		}
	}

	private static class SoundPacket {
		public boolean dropped = false;
		public float lifetime = 0;
		public Sound sound;
		public float volume;
		public long id;
		public boolean staticSound = false;
		public Vector2 position;
	}
}
