package com.gdx.game.Objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.*;
import com.gdx.game.BaseObjects.AnimatedEntity;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.BaseObjects.NetworkEntity;
import com.gdx.game.Objects.Projectiles.Explosion;
import com.gdx.game.Objects.Projectiles.Projectile;


public class Zombie extends NetworkEntity {
	AnimatedEntity display = new AnimatedEntity(new Vector2(0, 0), 0, 0, null);

	WalkCollider[] walkCollider = new WalkCollider[4];
	private boolean right = true;

	public Zombie(Vector2 pos) {
		super(pos, 0, 10, null);
		display.setAnim(ResourceManager.getAnim("Zombies/zombie.png", 6, 1/6f), 0);

		hasPhysics = true;
		kinematic = false;
		team = 100;
		collider = CreatePolygon.rect(10, 14);

		if (isLocal) {
			walkCollider[0] = new WalkCollider(new Vector2(pos.x + 14, pos.y - 7), this);
			Game.physicsObjects.add(walkCollider[0]);
			walkCollider[1] = new WalkCollider(new Vector2(pos.x + 14, pos.y + 7), this);
			Game.physicsObjects.add(walkCollider[1]);
		}
		updateFreq = 1;
	}

	public Zombie(String s) {
		super(s);
		display.setAnim(ResourceManager.getAnim("Zombies/zombie.png", 6, 1/6f), 0);
	}

	float walkTurnTime = 0;
	@Override
	public void update() {
		display.localPos.set(localPos);
		display.update();

		if (isLocal) {
			walkTurnTime += Time.deltaTime;
			if (right) {
				if (walkCollider[0].hasGround && !walkCollider[1].hasGround && !(walkTurnTime >= 5 && Game.random.nextFloat() < 0.0125f)) {
					velocity.x = 10;
				} else {
					velocity.x = 0;
					right = false;
					walkCollider[0].localPos.x = - 14;
					walkCollider[1].localPos.x = - 14;
					walkTurnTime = 0;
				}
			} else {
				if (walkCollider[0].hasGround && !walkCollider[1].hasGround && !(walkTurnTime >= 5 && Game.random.nextFloat() < 0.0125f)) {
					velocity.x = -10;
				} else {
					velocity.x = 0;
					right = true;
					walkCollider[0].localPos.x = + 14;
					walkCollider[1].localPos.x = + 14;
					walkTurnTime = 0;
				}
			}

			display.mirrorX = !right;
		}

		if (isLocal) {
			walkCollider[0].update();
			walkCollider[1].update();
		}
	}

	@Override
	public synchronized String serialize() {
		return super.serialize() + "  " + display.mirrorX;
	}

	@Override
	public synchronized void deSerialize(String[] s) {
		super.deSerialize(s);
		if (display != null) {
			display.mirrorX = (s[6].equals("true"));
		}
	}

	@Override
	public void lateUpdate() {
		super.lateUpdate();
		if (display != null) {
			display.lateUpdate();
			if (isLocal) {
				walkCollider[0].lateUpdate();
				walkCollider[1].lateUpdate();
			}
		}
	}

	@Override
	public void render(Batch batch) {
		display.render(batch);
	}

	@Override
	public void onCollision(Entity other) {
		super.onCollision(other);
		if ((other instanceof Mech && ((Mech) other).isPlayer && other.velocity.len2() > 0) || other instanceof Projectile || other instanceof Explosion) {
			Delete();
		}
	}

	@Override
	public void onDelete() {
		super.onDelete();
		if (isLocal) {
			Game.physicsObjects.remove(walkCollider[0]);
			Game.physicsObjects.remove(walkCollider[1]);
			if (!delete) {
				PlayerProfile.addCurrency(1);
			}
		}

		if (!delete) {
			int gibs = (int) ((5 + Game.random.nextFloat() * 10) * Settings.gibScale);
			for (int i = 0; i < gibs; i++) {
				Game.addGameObject(new ZombieBlood(pos, Game.random.nextFloat() * 360));
			}
		}
	}

	private class WalkCollider extends Entity {

		public boolean hasGround;
		public WalkCollider(Vector2 pos, GameObject parent) {
			super(pos, 0, 0, parent);
			hasPhysics = true;
			kinematic = true;
			canMoveObjects = false;
			team = -100;
			collider = CreatePolygon.rect(4, 10);
		}

		@Override
		public void preparePhysics() {
			super.preparePhysics();
			hasGround = false;
		}

		@Override
		public void onCollision(Entity other) {
			if (other.team == -1 || other.team == -2) {
				hasGround = true;
			}
		}
	}
}
