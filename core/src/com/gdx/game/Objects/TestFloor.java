package com.gdx.game.Objects;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.CreatePolygon;

public class TestFloor extends Entity {

	public TestFloor(Vector2 pos) {
		super(pos, 0, 0, null);
		setSprite("badlogic.jpg");
		collider = CreatePolygon.rect(sprite.getWidth(), sprite.getHeight());
		team = -1;
	}
}
