package com.gdx.game.Objects;

import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.CreatePolygon;
import com.gdx.game.Game;
import com.gdx.game.Objects.MechParts.MechPart;
import com.gdx.game.Objects.Projectiles.Explosion;
import com.gdx.game.Time;


public class ZombieBlood extends Entity {

	float rotSpeed = 0;
	float land = 0.25f;
	float minEjectSpeed = 100;
	public ZombieBlood(Vector2 pos, float angle) {
		super(pos, angle, 11, null);
		velocity.set(0, minEjectSpeed + Game.random.nextFloat() * 100);
		velocity.setAngle(angle);
		hasPhysics = true;
		kinematic = false;
		setSprite("Zombies/zombieBlood" + ((int) (Game.random.nextFloat() * 5) + 1) + ".png");
		collider = CreatePolygon.rect(2, 2);
		team = -100;
		rotSpeed = (Game.random.nextFloat() * 1080) - 540;
	}

	@Override
	public void update() {
		team = -100;
		if (hasPhysics) {
			localRot += rotSpeed * Time.deltaTime;
		}
	}

	@Override
	public void onCollision(Entity other) {
		super.onCollision(other);
		if ((other.team == -1) && !(other instanceof Explosion)) {
			if (Game.random.nextFloat() < land) {
				collider = null;
				hasPhysics = false;
				setParent(other);
				velocity.set(0, 0);
				Game.physicsObjects.remove(this);
				rotSpeed = 0;
			} else {
				land += 0.1f;
				if (delta.x != 0) {
					velocity.x *= -1;
				}
				if (delta.y != 0) {
					velocity.y *= -1;
				}
			}
		}
	}
}
