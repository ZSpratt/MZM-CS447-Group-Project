package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Game;
import com.gdx.game.Objects.Mech;
import com.gdx.game.Objects.MechParts.MechArm;
import com.gdx.game.Objects.MechParts.PartsList;
import com.gdx.game.PlayerProfile;
import com.gdx.game.Stages.LevelStage;

import java.util.ArrayList;

public class MechCustomMenu extends GUIBox{
	public RGBSelector primaryColorSelector;
	public RGBSelector secondaryColorSelector;
	public Mech previewMech;
	public GUIBox subBox;

	public GUITextBox address;
	public GUIButton start;
	public GUIButton connect;

	ArrayList<GUIButton> buttons = new ArrayList<GUIButton>();
	ArrayList<Rectangle> hovers = new ArrayList<Rectangle>();
	ArrayList<ShopPopup> popups = new ArrayList<ShopPopup>();
	public GUICheckbox headInvert;
	public GUICheckbox bodyInvert;
	public GUICheckbox armLInvert;
	public GUICheckbox armRInvert;
	public GUICheckbox legInvert;

	public StringDraw headString;
	public StringDraw bodyString;
	public StringDraw armLString;
	public StringDraw armRString;
	public StringDraw legString;

	public MechCustomMenu() {
		super(0, 0, 850, 400, 10);

		primaryColorSelector = new RGBSelector(340, 0, 0);
		primaryColorSelector.setColor(PlayerProfile.primaryColor);
		primaryColorSelector.label = "Primary Color";

		secondaryColorSelector = new RGBSelector(170, 0, 0);
		secondaryColorSelector.setColor(PlayerProfile.secondaryColor);
		secondaryColorSelector.label = "Secondary Color";


		subBox = new GUIBox(-310, 90, 200, 200, 5);

		previewMech = new Mech(new Vector2(-310, 90-64), 0, 0, null);
		previewMech.enabled = false;
		previewMech.hasPhysics = false;
		previewMech.arms.get(0).hasPhysics = false;
		Game.physicsObjects.remove(previewMech.arms.get(0));
		previewMech.arms.get(1).hasPhysics = false;
		Game.physicsObjects.remove(previewMech.arms.get(1));
		previewMech.legs.hasPhysics = false;
		Game.physicsObjects.remove(previewMech.legs);
		previewMech.body.hasPhysics = false;
		Game.physicsObjects.remove(previewMech.body);
		previewMech.head.hasPhysics = false;
		Game.physicsObjects.remove(previewMech.head);

		ButtonAction action = new ButtonAction() {
			@Override
			public void onClick() {
				int head = PlayerProfile.equips[4];
				head = (head + 1) % PartsList.Head.length;
				while (!PlayerProfile.hasItem("Head", head)) {
					head = (head + 1) % PartsList.Head.length;
				}
				PlayerProfile.equips[4] = head;
			}
		};
		headInvert = new GUICheckbox(-175, 175, 25, 25, 0);
		headString = new StringDraw("Head", -45, 175, 0, 4);
		buttons.add(new GUIButton(50, 175, 25, 25, ">", action, 0));

		action = new ButtonAction() {
			@Override
			public void onClick() {
				int head = PlayerProfile.equips[4];
				head = (head - 1) % PartsList.Head.length;
				if (head < 0) {
					head += PartsList.Head.length;
				}
				while (!PlayerProfile.hasItem("Head", head)) {
					if (head < 0) {
						head += PartsList.Head.length;
					}
					head = (head - 1) % PartsList.Head.length;
				}
				PlayerProfile.equips[4] = head;
			}
		};
		buttons.add(new GUIButton(-140, 175, 25, 25, "<", action, 0));

		action = new ButtonAction() {
			@Override
			public void onClick() {
				int body = PlayerProfile.equips[1];
				body = (body + 1) % PartsList.Torso.length;
				while (!PlayerProfile.hasItem("Torso", body)) {
					body = (body + 1) % PartsList.Torso.length;
				}
				PlayerProfile.equips[1] = body;
			}
		};
		bodyInvert = new GUICheckbox(-175, 140, 25, 25, 0);
		bodyString = new StringDraw("Body", -45, 140, 0, 4);
		buttons.add(new GUIButton(50, 140, 25, 25, ">", action, 0));
		action = new ButtonAction() {
			@Override
			public void onClick() {
				int body = PlayerProfile.equips[1];
				body = (body - 1) % PartsList.Torso.length;
				if (body < 0) {
					body += PartsList.Torso.length;
				}
				while (!PlayerProfile.hasItem("Torso", body)) {
					if (body < 0) {
						body += PartsList.Torso.length;
					}
					body = (body - 1) % PartsList.Torso.length;
				}
				PlayerProfile.equips[1] = body;
			}
		};
		buttons.add(new GUIButton(-140, 140, 25, 25, "<", action, 0));

		action = new ButtonAction() {
			@Override
			public void onClick() {
				int armL = PlayerProfile.equips[2];
				armL = (armL + 1) % PartsList.Arm.length;
				while (!PlayerProfile.hasItem("Arm", armL)) {
					armL = (armL + 1) % PartsList.Arm.length;
				}
				PlayerProfile.equips[2] = armL;
			}
		};
		armLInvert = new GUICheckbox(-175, 105, 25, 25, 0);
		armLString = new StringDraw("ArmL", -45, 105, 0, 4);
		buttons.add(new GUIButton(50, 105, 25, 25, ">", action, 0));
		action = new ButtonAction() {
			@Override
			public void onClick() {
				int armL = PlayerProfile.equips[2];
				armL = (armL - 1) % PartsList.Arm.length;
				if (armL < 0) {
					armL += PartsList.Arm.length;
				}
				while (!PlayerProfile.hasItem("Arm", armL)) {
					if (armL < 0) {
						armL += PartsList.Arm.length;
					}
					armL = (armL - 1) % PartsList.Arm.length;
				}
				PlayerProfile.equips[2] = armL;
			}
		};
		buttons.add(new GUIButton(-140, 105, 25, 25, "<", action, 0));

		action = new ButtonAction() {
			@Override
			public void onClick() {
				int armR = PlayerProfile.equips[3];
				armR = (armR + 1) % PartsList.Arm.length;
				while (!PlayerProfile.hasItem("Arm", armR)) {
					armR = (armR + 1) % PartsList.Arm.length;
				}
				PlayerProfile.equips[3] = armR;
			}
		};
		armRInvert = new GUICheckbox(-175, 70, 25, 25, 0);
		armRString = new StringDraw("ArmR", -45, 70, 0, 4);
		buttons.add(new GUIButton(50, 70, 25, 25, ">", action, 0));
		action = new ButtonAction() {
			@Override
			public void onClick() {
				int armR = PlayerProfile.equips[3];
				armR = (armR - 1) % PartsList.Arm.length;
				if (armR < 0) {
					armR += PartsList.Arm.length;
				}
				while (!PlayerProfile.hasItem("Arm", armR)) {
					if (armR < 0) {
						armR += PartsList.Arm.length;
					}
					armR = (armR - 1) % PartsList.Arm.length;
				}
				PlayerProfile.equips[3] = armR;
			}
		};
		buttons.add(new GUIButton(-140, 70, 25, 25, "<", action, 0));

		action = new ButtonAction() {
			@Override
			public void onClick() {
				int legs = PlayerProfile.equips[0];
				legs = (legs + 1) % PartsList.Leg.length;
				while (!PlayerProfile.hasItem("Leg", legs)) {
					legs = (legs + 1) % PartsList.Leg.length;
				}
				PlayerProfile.equips[0] = legs;
			}
		};
		legInvert = new GUICheckbox(-175, 35, 25, 25, 0);
		legString = new StringDraw("Legs", -45, 35, 0, 4);
		buttons.add(new GUIButton(50, 35, 25, 25, ">", action, 0));
		action = new ButtonAction() {
			@Override
			public void onClick() {
				int legs = PlayerProfile.equips[0];
				legs = (legs - 1) % PartsList.Leg.length;
				if (legs < 0) {
					legs += PartsList.Leg.length;
				}
				while (!PlayerProfile.hasItem("Leg", legs)) {
					if (legs < 0) {
						legs += PartsList.Leg.length;
					}
					legs = (legs - 1) % PartsList.Leg.length;
				}
				PlayerProfile.equips[0] = legs;
			}
		};
		buttons.add(new GUIButton(-140, 35, 25, 25, "<", action, 0));


		headInvert.toggled = PlayerProfile.inverts[4];
		bodyInvert.toggled = PlayerProfile.inverts[1];
		armLInvert.toggled = PlayerProfile.inverts[2];
		armRInvert.toggled = PlayerProfile.inverts[3];
		legInvert.toggled = PlayerProfile.inverts[0];

		for (int i = 0; i < 6; i++) {
			ShopPopup p = new ShopPopup(-165, -100, "", 0, false);
			p.enabled = false;
			popups.add(p);
			if (i != 5) {
				hovers.add(new Rectangle(-45 - 250 / 2f, 175 - 35 * i - 25 / 2f, 250, 25));
			}
		}
	}

	@Override
	public void update() {
		super.update();

		PlayerProfile.primaryColor.set(primaryColorSelector.color);
		PlayerProfile.secondaryColor.set(secondaryColorSelector.color);

		primaryColorSelector.update();
		secondaryColorSelector.update();
		subBox.update();

		for (GUIButton b : buttons) {
			b.update();
		}

		int head = PlayerProfile.equips[4];
		int body = PlayerProfile.equips[1];
		int legs = PlayerProfile.equips[0];
		int armR = PlayerProfile.equips[3];
		int armL = PlayerProfile.equips[2];

		previewMech.head.updatePart(PartsList.Head[head]);
		previewMech.body.updatePart(PartsList.Torso[body]);
		previewMech.arms.get(0).updatePart(PartsList.Arm[armL]);
		previewMech.arms.get(1).updatePart(PartsList.Arm[armR]);
		previewMech.legs.updatePart(PartsList.Leg[legs]);

		previewMech.update();
		previewMech.localPos.set(-310, 90-64);
		previewMech.pos.set(-310, 90-64);
		previewMech.velocity.set(0, 0);
		previewMech.lateUpdate();

		headInvert.update();
		headString.str = PartsList.Head[PlayerProfile.equips[4]].split("/")[PartsList.Head[PlayerProfile.equips[4]].split("/").length - 1];
		headString.update();
		PlayerProfile.inverts[4] = headInvert.toggled;
		PlayerProfile.equips[4] = head;
		bodyInvert.update();
		bodyString.str = PartsList.Torso[PlayerProfile.equips[1]].split("/")[PartsList.Torso[PlayerProfile.equips[1]].split("/").length - 1];
		bodyString.update();
		PlayerProfile.inverts[1] = bodyInvert.toggled;
		PlayerProfile.equips[1] = body;
		armLInvert.update();
		armLString.str = PartsList.Arm[PlayerProfile.equips[2]].split("/")[PartsList.Arm[PlayerProfile.equips[2]].split("/").length - 1];
		armLString.update();
		PlayerProfile.inverts[2] = armLInvert.toggled;
		PlayerProfile.equips[2] = armL;
		armRInvert.update();
		armRString.str = PartsList.Arm[PlayerProfile.equips[3]].split("/")[PartsList.Arm[PlayerProfile.equips[3]].split("/").length - 1];
		armRString.update();
		PlayerProfile.inverts[3] = armRInvert.toggled;
		PlayerProfile.equips[3] = armR;
		legInvert.update();
		legString.str = PartsList.Leg[PlayerProfile.equips[0]].split("/")[PartsList.Leg[PlayerProfile.equips[0]].split("/").length - 1];
		legString.update();
		PlayerProfile.inverts[0] = legInvert.toggled;
		PlayerProfile.equips[0] = legs;

		int hover = 5;
		for (int i = 0; i < hovers.size(); i++) {
			if (hovers.get(i).contains(Game.GUImouseX, Game.GUImouseY)) {
				hover = i;
			}
		}
		popups.get(0).enabled = (hover == 0);
		popups.get(0).setPart(PartsList.Head[PlayerProfile.equips[4]]);
		popups.get(1).enabled = (hover == 1);
		popups.get(1).setPart(PartsList.Torso[PlayerProfile.equips[1]]);
		popups.get(2).enabled = (hover == 2);
		popups.get(2).setPart(PartsList.Arm[PlayerProfile.equips[2]]);
		popups.get(3).enabled = (hover == 3);
		popups.get(3).setPart(PartsList.Arm[PlayerProfile.equips[3]]);
		popups.get(4).enabled = (hover == 4);
		popups.get(4).setPart(PartsList.Leg[PlayerProfile.equips[0]]);
		popups.get(5).enabled = (hover == 5);

		for (ShopPopup pop : popups) {
			pop.update();
		}
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		primaryColorSelector.renderGUI(batch);
		secondaryColorSelector.renderGUI(batch);
		subBox.renderGUI(batch);
		previewMech.primaryColor.set(primaryColorSelector.color);
		previewMech.secondaryColor.set(secondaryColorSelector.color);
		previewMech.render(batch);

		for (GUIButton b : buttons) {
			b.renderGUI(batch);
		}

		headInvert.renderGUI(batch);
		bodyInvert.renderGUI(batch);
		armLInvert.renderGUI(batch);
		armRInvert.renderGUI(batch);
		legInvert.renderGUI(batch);

		headString.renderGUI(batch);
		bodyString.renderGUI(batch);
		armLString.renderGUI(batch);
		armRString.renderGUI(batch);
		legString.renderGUI(batch);

		for (ShopPopup pop : popups) {
			if (pop.enabled) {
				pop.renderGUI(batch);
			}
		}
	}
}
