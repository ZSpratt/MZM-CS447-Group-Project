package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Game;
import com.gdx.game.Objects.Mech;
import com.gdx.game.Objects.MechParts.MechArm;
import com.gdx.game.Objects.MechParts.MechLeg;
import com.gdx.game.Objects.MechParts.MechPart;
import com.gdx.game.Objects.MechParts.PartsList;
import com.gdx.game.PlayerProfile;
import com.gdx.game.ResourceManager;
import com.sun.org.apache.regexp.internal.RE;

import java.awt.*;

public class ShopButton extends GUIButton {
    public String buttonPart;
    public AnimatedEntity buttonSprite;
    public int cost;
    public ShopMenu menuParent;
    public StringDraw costDraw;
    public boolean pCanBuy;

    public ShopButton(float x, float y, float width, float height, String value, ButtonAction action, int z, GameObject parent, int price) {
        super(x, y, width, height, "", action, 0);

        menuParent = (ShopMenu)parent;
        buttonPart = value;
        cost = price;
        buttonSprite = new AnimatedEntity(new Vector2(x, y), 45f, 0, null);
        int animFrames = Integer.parseInt(ResourceManager.getPartData(buttonPart, "Primary-Frames")[0]);

        buttonSprite.setAnim(ResourceManager.getAnim(buttonPart + ".png", animFrames, 0), 0);
        buttonSprite.scale = .35f;
        //buttonSprite.looping = false;
        costDraw = new StringDraw(Integer.toString(cost), x, y + 20, 0, 7);
        //costDraw.scale = .25f;
    }


    @Override
    public void update() {
        super.update();
        costDraw.update();
        if (pCanBuy) {
            costDraw.color = Color.WHITE;
        }
        else {
            costDraw.color = Color.RED;
        }
        //buttonSprite.update();
    }

    @Override
    public void renderGUI(Batch batch) {
        super.renderGUI(batch);
        buttonSprite.render(batch);
        if (cost > 0) {
            costDraw.renderGUI(batch);
        }
    }
}
