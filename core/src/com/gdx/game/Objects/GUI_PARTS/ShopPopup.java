package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.gdx.game.BaseObjects.GUIBox;
import com.gdx.game.BaseObjects.StringDraw;
import com.gdx.game.PlayerProfile;

public class ShopPopup extends GUIBox {
	String resource = "";
	StringDraw mechName;
	StringDraw partName;
	StringDraw cost;
	StringDraw currentlyOwned;
	int c;
	boolean showCost = true;
	float x = 0;
	float y = 0;
	public ShopPopup(float x, float y, String resource, int c, boolean showCost) {
		super(x, y, 400, 100, 5);
		this.x = x;
		this.y = y;
		setPart(resource);
		this.c = c;
		this.showCost = showCost;
	}

	public void setPart(String s) {
		resource = s;
		if (s != "") {
			String mech = resource.split("/")[1];
			mechName = new StringDraw("Mech Class : " + mech, x - 200, y + 25, 0, 3);
			String part = resource.split("/")[2];
			partName = new StringDraw("Part : " + part, x - 200, y - 25, 0, 3);
			cost = new StringDraw("Cost : " + c, x + 200, y + 25, 0, 5);
			currentlyOwned = new StringDraw("Currently Owned : " + c, x + 200, y - 25, 0, 5);
		}
	}

	@Override
	public void update() {
		if (resource != "") {
			mechName.update();
			mechName.lateUpdate();
			partName.update();
			partName.lateUpdate();
			cost.update();
			cost.lateUpdate();

			String s;
			float amnt = PlayerProfile.partCount(resource);
			if (amnt == Float.POSITIVE_INFINITY) {
				s = "Currently Owned : INF";
			} else {
				s = "Currently Owned : " + (int)amnt;
			}
			currentlyOwned.str = s;
			currentlyOwned.update();
			currentlyOwned.lateUpdate();
		}
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		if (resource != "") {
			mechName.renderGUI(batch);
			partName.renderGUI(batch);
			if (showCost) {
				cost.renderGUI(batch);
			}
			currentlyOwned.renderGUI(batch);
		}
	}
}
