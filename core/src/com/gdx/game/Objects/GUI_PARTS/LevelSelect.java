package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.gdx.game.BaseObjects.ButtonAction;
import com.gdx.game.BaseObjects.GUIBox;
import com.gdx.game.BaseObjects.GUIButton;
import com.gdx.game.Game;
import com.gdx.game.ResourceManager;
import com.gdx.game.Stages.LevelStage;

import java.util.ArrayList;

public class LevelSelect extends GUIBox{
	ArrayList<GUIButton> buttons = new ArrayList<GUIButton>();
	String[] stages = {"Maps/MapProjects/HubWorld.tmx", "Maps/MapProjects/TempleRuins.tmx", "Maps/MapProjects/SkullForest.tmx","Maps/MapProjects/AreaSixNine.tmx","Maps/MapProjects/AncientForest.tmx"};
	GUIBox box;

	public LevelSelect(final String currentLevel) {
		super(0, 0, 325, 300, 1000);

		ButtonAction action = new ButtonAction() {
			@Override
			public void onClick() {
				LevelStage stage = new LevelStage(currentLevel);
				stage.restart = true;
				Game.setStage(stage);
			}
		};
		buttons.add(new GUIButton(-100, 100, 100, 32, "Restart", action, 10));
		box = new GUIBox(64, 0, 128, 128, 0);

		for (int i = 0; i < stages.length; i++) {
			final String newStage = stages[i];
			action = new ButtonAction() {
				@Override
				public void onClick() {
					LevelStage stage = new LevelStage(newStage);
					stage.restart = true;
					Game.setStage(stage);
				}
			};
			Popup popup = new Popup(64, 0, newStage);
			GUIButton button = new GUIButton(-100, 50 - 44 * i, 115, 32, newStage.split("/")[2].split("\\.")[0], action, 10);
			button.popup = popup;
			buttons.add(button);
		}
	}

	@Override
	public void update() {
		for (GUIButton button : buttons) {
			button.update();
			button.lateUpdate();
		}
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		box.renderGUI(batch);
		for (GUIButton button : buttons) {
			button.renderGUI(batch);
		}
	}

	private class Popup extends GUIBox {

		Texture preview;
		public Popup(float x, float y, String level) {
			super(x, y, 128, 128, 100);
			preview = ResourceManager.getImage(level.split("\\.")[0] + ".png");
			if (preview == null) {
				preview = ResourceManager.getImage("Maps/NoPreview.png");
			}
		}

		@Override
		public void renderGUI(Batch batch) {
			super.renderGUI(batch);
			batch.draw(preview, pos.x - 64, pos.y - 64);
		}
	}
}
