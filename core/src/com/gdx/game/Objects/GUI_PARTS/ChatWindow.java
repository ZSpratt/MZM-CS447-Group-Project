package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Game;
import com.gdx.game.InProcess;
import com.gdx.game.Settings;

import java.util.ArrayList;

/**
 * Created by zachm on 12/7/2017.
 */
public class ChatWindow extends NetworkEntity{
	private static ChatTextBox chat;
	public static String message;

	ArrayList<StringDraw> messages = new ArrayList<StringDraw>();

	public ChatWindow(Vector2 pos, float angle, int z, GameObject parent) {
		super(pos, angle, z, parent);
		chat = new ChatTextBox(pos.x- 250, pos.y, 0, 16, "", 100, 1);
		netId = "Messaging";
		System.out.println("Make chatbox");
	}

	@Override
	public void update() {
		isLocal = true;
		netId = "Messaging";
		if (Game.networkObjects.containsKey(netId)) {
			Game.networkObjects.put(netId, this);
		}
		chat.update();

		if (!Game.connected) {
			serialize();
		}

		super.update();
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		chat.renderGUI(batch);

		for (int i = messages.size() - 1; i >= 0; i--) {
			messages.get(i).localPos.set(pos.x-250, pos.y + 32 + 16 * i);
			messages.get(i).update();
			messages.get(i).lateUpdate();
			messages.get(i).renderGUI(batch);
			if (messages.get(i).delete) {
				messages.remove(i);
			}
		}
	}

	@Override
	public synchronized String serialize() {
		netId = "Messaging";
		String msg = netId;
		if (chat.outMessage != null && !chat.outMessage.equals("")) {
			msg = "Messaging  " + Game.netID + ":" + chat.outMessage;
			messages.add(0, new StringDraw(Game.netID + ":" + chat.outMessage, 0, 0, 0, 3));
			messages.get(0).lifetime = 20;
			//System.out.println("" + msg + " " + chat.outMessage.length());
			chat.outMessage = null;
			chat.value = "";
		}
		return msg;
	}

	@Override
	public synchronized void deSerialize(String[] s) {
		netId = "Messaging";
		String message = "";
		for (int i = 1; i < s.length; i++) {
			message += s[i];
			if (i < s.length - 1) {
				message += "  ";
			}
		}
		if (s.length > 1) {
			messages.add(0, new StringDraw(message, 0, 0, 0, 3));
			messages.get(0).lifetime = 20;
		}
	}

	private class ChatTextBox extends GUITextBox {
		public String outMessage = null;
		public ChatTextBox(float x, float y, float width, float height, String value, int maxLength, int z) {
			super(x, y, width, height, value, maxLength, z);
			System.out.println("Make textInput");
			stringDraw.allignment = 3;
			value = "";
		}

		public synchronized void update() {
			if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
				if (active) {
					active = false;
					outMessage = value;
				} else {
					active = true;
				}
				Game.paused = active;
			}

			if (Gdx.input.isKeyJustPressed(Settings.pause)) {
				if (active) {
					active = false;
					Game.paused = false;
				}
			}

			if (active) {
				color = activeColor;
				if (Gdx.input.isKeyJustPressed(Input.Keys.BACKSPACE)) {
					if (value.length() > 0) {
						value = value.substring(0, value.length() - 1);
					}
				} else if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
					if (value.length() < maxLength && Character.isDefined(InProcess.lastChar) && !Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
						value += InProcess.lastChar;
					}
				}
				stringDraw.color = color;
			} else {
				color = inactiveColor;
			}

			subBox.update();
			stringDraw.setFont(font);
			stringDraw.color = color;
			stringDraw.str = value;
			stringDraw.update();
		}

		@Override
		public void renderGUI(Batch batch) {
			stringDraw.renderGUI(batch);
		}
	}
}
