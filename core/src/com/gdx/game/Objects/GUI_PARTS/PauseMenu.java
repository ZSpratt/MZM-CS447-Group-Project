package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Game;
import com.gdx.game.InProcess;
import com.gdx.game.Objects.Mech;
import com.gdx.game.Objects.MechParts.MechArm;
import com.gdx.game.Objects.MechParts.PartsList;
import com.gdx.game.PlayerProfile;
import com.gdx.game.Settings;
import com.gdx.game.Stages.LevelStage;

public class PauseMenu extends GUIBox{

	public GUITextBox address;
	public GUIButton start;
	public GUIButton connect;
	public GUIButton disconnect;

	public GUIButton debug;

	public StringDraw gibText;
	public GUISlider gibs;

	public StringDraw volumeText;
	public GUISlider volume;

	boolean keyListen = false;
	int keyChoice = 0;

	public GUIButton[] keyButtons = new GUIButton[13];

	public PauseMenu() {
		super(0, 0, 850, 400, 10);

		final GUITextBox box = new GUITextBox(-250,105, 200, 30, "localhost", 50, 0);
		address = box;

		ButtonAction action = new ButtonAction() {
			@Override
			public void onClick() {
				Game.setServer();
			}
		};
		start = new GUIButton(-250, 65, 125, 30, "Start Server", action, 0);

		action = new ButtonAction() {
			GUITextBox t = box;
			@Override
			public void onClick() {
				Game.connectToServer(t.value);
			}
		};
		connect = new GUIButton(-250,25, 125, 30, "Connect", action, 0);

		action = new ButtonAction() {
			GUITextBox t = box;
			@Override
			public void onClick() {
				Game.disconnect();
			}
		};
		disconnect = new GUIButton(-250, -15, 125, 30, "Disconnect", action, 0);

		gibText = new StringDraw("Gibs", -375, -54, 0, 4);
		gibs = new GUISlider(-250, -54, 175, 20, 0);
		gibs.minValue = 0;
		gibs.maxValue = 2;
		gibs.setValue(Settings.gibScale);

		volumeText = new StringDraw("Volume", -375, -90, 0, 4);
		volume = new GUISlider(-250, -90, 175, 20, 0);
		volume.minValue = 0;
		volume.maxValue = 1;
		volume.setValue(Settings.volume);

		debug = new GUIButton(-250, -120, 125, 30, "Debug Off", action, 0);
		action = new ButtonAction() {
			@Override
			public void onClick() {
				Game.debug = !Game.debug;
				if (Game.debug) {
					debug.label = "Debug On";
				} else {
					debug.label = "Debug Off";
				}
			}
		};
		debug.action = action;

		for (int i = 0; i < keyButtons.length - 1; i++) {
			final int x = i;
			action = new ButtonAction() {
				@Override
				public void onClick() {
					keyListen = true;
					keyChoice = x;
				}
			};
			int xp = i % 2;
			int yp = i / 2;
			keyButtons[i] = new GUIButton((xp * 125) + 158, -yp * 48 + 128, 120, 32, "Button", action, 10);
		}
		action = new ButtonAction() {
			@Override
			public void onClick() {
				Settings.up = Input.Keys.W;
				Settings.down = Input.Keys.S;
				Settings.left = Input.Keys.A;
				Settings.right = Input.Keys.D;

				Settings.sprint = Input.Keys.SHIFT_LEFT;
				Settings.mapSelect = Input.Keys.M;
				Settings.pause = Input.Keys.ESCAPE;

				Settings.repair = Input.Keys.R;
				Settings.respawn = Input.Keys.N;
				Settings.interact = Input.Keys.E;

				Settings.profile = Input.Keys.P;
				Settings.reload = Input.Keys.U;
			}
		};
		keyButtons[12] = new GUIButton(220.5f, -112-48, 240, 32, "Reset", action, 10);
	}

	@Override
	public void update() {
		super.update();

		address.update();
		address.lateUpdate();
		start.update();
		start.lateUpdate();
		connect.update();
		connect.lateUpdate();
		disconnect.disabled = !Game.connected;
		disconnect.update();
		disconnect.lateUpdate();
		debug.update();
		debug.lateUpdate();

		gibText.update();
		gibs.update();
		gibs.lateUpdate();
		volumeText.update();
		volume.update();
		volume.lateUpdate();
		Settings.gibScale = gibs.value;
		Settings.volume = volume.value;

		for (int i = 0; i < keyButtons.length; i++) {
			keyButtons[i].update();
			keyButtons[i].lateUpdate();
		}

		for (int i = 0; i < keyButtons.length; i++) {
			if (keyChoice == i && keyListen) {
				keyButtons[i].label = "Press Key";
			} else {
				String label = "";
				switch (i) {
					case 0: {
						label = "Up : " + Input.Keys.toString(Settings.up);
						break;
					}
					case 1: {
						label = "Down : " + Input.Keys.toString(Settings.down);
						break;
					}
					case 2: {
						label = "Left : " + Input.Keys.toString(Settings.left);
						break;
					}
					case 3: {
						label = "Right : " + Input.Keys.toString(Settings.right);
						break;
					}
					case 4: {
						label = "Sprint : " + Input.Keys.toString(Settings.sprint);
						break;
					}
					case 5: {
						label = "Select : " + Input.Keys.toString(Settings.mapSelect);
						break;
					}
					case 6: {
						label = "Pause : " + Input.Keys.toString(Settings.pause);
						break;
					}
					case 7: {
						label = "Repair : " + Input.Keys.toString(Settings.repair);
						break;
					}
					case 8: {
						label = "Respawn : " + Input.Keys.toString(Settings.respawn);
						break;
					}
					case 9: {
						label = "Interact : " + Input.Keys.toString(Settings.interact);
						break;
					}
					case 10: {
						label = "Profile : " + Input.Keys.toString(Settings.profile);
						break;
					}
					case 11: {
						label = "Reload : " + Input.Keys.toString(Settings.reload);
						break;
					}
					case 12: {
						label = "Reset";
						break;
					}
				}
				keyButtons[i].label = label;
			}
		}

		if (keyListen) {
			if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
				keyListen = false;
				switch (keyChoice) {
					case 0: {
						Settings.up = InProcess.lastKey;
						break;
					}
					case 1: {
						Settings.down = InProcess.lastKey;
						break;
					}
					case 2: {
						Settings.left = InProcess.lastKey;
						break;
					}
					case 3: {
						Settings.right = InProcess.lastKey;
						break;
					}
					case 4: {
						Settings.sprint = InProcess.lastKey;
						break;
					}
					case 5: {
						Settings.mapSelect = InProcess.lastKey;
						break;
					}
					case 6: {
						Settings.pause = InProcess.lastKey;
						break;
					}
					case 7: {
						Settings.repair = InProcess.lastKey;
						break;
					}
					case 8: {
						Settings.respawn = InProcess.lastKey;
						break;
					}
					case 9: {
						Settings.interact = InProcess.lastKey;
						break;
					}
					case 10: {
						Settings.profile = InProcess.lastKey;
						break;
					}
					case 11: {
						Settings.reload = InProcess.lastKey;
						break;
					}
				}
			}
		}
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		address.renderGUI(batch);
		connect.renderGUI(batch);
		start.renderGUI(batch);
		disconnect.renderGUI(batch);
		gibText.renderGUI(batch);
		gibs.renderGUI(batch);
		debug.renderGUI(batch);
		volume.renderGUI(batch);
		volumeText.renderGUI(batch);

		for (int i = 0; i < keyButtons.length; i++) {
			keyButtons[i].renderGUI(batch);
		}
	}
}
