package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Game;

/**
 * Created by Zachary on 2017-11-24.
 */
public class RGBSelector extends GUIBox {

	GUISlider[] rgb = new GUISlider[3];
	GUIFillBar preview;
	public Color color = new Color(Color.WHITE);
	public String label = "RGB SLIDER";
	private StringDraw labelDraw;

	public RGBSelector(float x, float y, int z) {
		super(x, y, 150, 300, z);

		for (int i = 0; i < 3; i++) {
			rgb[i] = new GUISlider(x - 5, y - 32 - (150f/3f) * i, 125, 16, z + 1);
			rgb[i].maxValue = 255;
			rgb[i].intSlider = true;
			rgb[i].value = 1;
			rgb[i].showNumber = false;
		}
		preview = new GUIFillBar(x, y + 150f/2f, 100, 100, z + 1);
		preview.fill = 1.0f;

		labelDraw = new StringDraw(label, x, y + 140, 0, 4);
	}

	public void setColor (Color c) {
		rgb[0].setValue(c.r * 255);
		rgb[1].setValue(c.g * 255);
		rgb[2].setValue(c.b * 255);

		color.set(c);
	}

	@Override
	public void update() {
		for (int i = 0; i < 3; i++) {
			rgb[i].update();
		}
		preview.update();
		preview.lateUpdate();
		color.set(rgb[0].value/255f, rgb[1].value/255f, rgb[2].value/255f, 1.0f);
		preview.fullColor.set(color);
		labelDraw.str = label;
		labelDraw.update();
		labelDraw.lateUpdate();
	}

	@Override
	public void renderGUI(Batch batch) {
		super.renderGUI(batch);
		for (int i = 0; i < 3; i++) {
			rgb[i].renderGUI(batch);
		}
		preview.renderGUI(batch);
		labelDraw.renderGUI(batch);
	}

	@Override
	public int guiz() {
		return 0;
	}
}
