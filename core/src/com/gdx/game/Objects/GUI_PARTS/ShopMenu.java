package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Game;
import com.gdx.game.Objects.Mech;
import com.gdx.game.Objects.MechParts.MechArm;
import com.gdx.game.Objects.MechParts.MechPart;
import com.gdx.game.Objects.MechParts.PartsList;
import com.gdx.game.PlayerProfile;

import java.util.ArrayList;

public class ShopMenu extends GUIBox {
    private GUIButton accept;
    public int numItems;
    public ArrayList<ShopButton> itemButtons;
    //public Mech previewMech = new Mech(new Vector2(0, 100), 0, 0, null);
    public ArrayList<MechPart> cart = new ArrayList<MechPart>();
    public String walletLabel = "Wallet: ";
    public String[] sectionLabels = {"Heads", "Arms", "Legs", "Torso"};
    public StringDraw[] sectionLabelDrawers = new StringDraw[4];
    public StringDraw walletLabelDraw;

    public int partCost;
    public ArrayList<StringDraw> costDrawers = new ArrayList<StringDraw>();

    public int numHeads = PartsList.Head.length;
    public int numArms = PartsList.Arm.length;
    public int numLegs = PartsList.Leg.length;
    public int numTorso = PartsList.Torso.length;
    private int bVSpace = 64;
    private int bHSpace = 48;
    private int shopWidth = 600;
    private int shopHeight = 400;
    private float xPos;
    private float yPos;

    private int _i = 0;
    public ShopMenu() {
        super(0, 0, 600, 400, 0);
        for (int i = 0; i < 4; i++) {
            xPos = -(shopWidth/2) + 50;
            yPos = (shopHeight/2) - 65 - (i*bVSpace);
            sectionLabelDrawers[i] = new StringDraw(sectionLabels[i], xPos, yPos, 0, 5);
        }
        numItems = numArms + numHeads + numLegs + numTorso;
        itemButtons = new ArrayList<ShopButton>();
        ButtonAction action = new ButtonAction() {
            @Override
            public void onClick() {
                System.out.println("clicked");
            }
        };
        float popupX = 100;
        float popupY = -150;
        // load heads
        for (_i = 0; _i < numHeads; _i++) {
            yPos = (shopHeight/2) - 65;
            xPos = -(shopWidth/2) + 80 + (_i*bHSpace);

            final ShopButton ib = new ShopButton(xPos, yPos, 32, 32, PartsList.Head[_i], action, 0, this, _i * 100);
            action = new ButtonAction() {
                ShopButton button = ib;
                @Override
                public void onClick() {
                    buyPart(button);
                }
            };
            if (_i != 0) {
                ib.action = action;
            }
            ib.popup = new ShopPopup(popupX, popupY, ib.buttonPart, _i * 100, true);
            itemButtons.add(ib);
        }

        // load arms
        for (_i = 0; _i < numArms; _i++) {
            yPos = (shopHeight/2) - 129;
            xPos = -(shopWidth/2) + 80 + (_i*bHSpace);

            final ShopButton ib = new ShopButton(xPos, yPos, 32, 32, PartsList.Arm[_i], action, 0, this, (int) (_i/2) * 100);
            action = new ButtonAction() {
                ShopButton button = ib;
                @Override
                public void onClick() {
                    buyPart(button);
                }
            };
            if (_i != 0 && _i != 1) {
                ib.action = action;
            }
            ib.popup = new ShopPopup(popupX, popupY, ib.buttonPart, (_i/2) * 100, true);
            itemButtons.add(ib);
        }

        // load legs
        for (_i = 0; _i < numLegs; _i++) {
            yPos = (shopHeight/2) - 193;
            xPos = -(shopWidth/2) + 80 + (_i*bHSpace);

            final ShopButton ib = new ShopButton(xPos, yPos, 32, 32, PartsList.Leg[_i], action, 0, this, _i * 100);
            action = new ButtonAction() {
                ShopButton button = ib;
                @Override
                public void onClick() {
                    buyPart(button);
                }
            };
            if (_i != 0) {
                ib.action = action;
            }
            ib.popup = new ShopPopup(popupX, popupY, ib.buttonPart, _i * 100, true);
            itemButtons.add(ib);
        }

        // load torsos
        for (_i = 0; _i < numTorso; _i++) {
            yPos = (shopHeight/2) - 257;
            xPos = -(shopWidth/2) + 80 + (_i*bHSpace);

            final ShopButton ib = new ShopButton(xPos, yPos, 32, 32, PartsList.Torso[_i], action, 0, this, _i * 100);
            action = new ButtonAction() {
                ShopButton button = ib;
                @Override
                public void onClick() {
                    buyPart(button);
                }
            };
            if (_i != 0) {
                ib.action = action;
            }
            ib.popup = new ShopPopup(popupX, popupY, ib.buttonPart, _i * 100, true);
            itemButtons.add(ib);
        }

        walletLabelDraw = new StringDraw(walletLabel + PlayerProfile.getWallet(), -(shopWidth/2) + 15, -(shopHeight/2) + 30, 0, 4);
    }

    public void buyPart(ShopButton sb) {
        if (sb.pCanBuy) {
            Integer pPartCount = PlayerProfile.inventory.get(sb.buttonPart);
            if (pPartCount != null) {
                PlayerProfile.inventory.put(sb.buttonPart, pPartCount+1);
            }
            else {
                PlayerProfile.inventory.put(sb.buttonPart, 1);
            }
            PlayerProfile.remCurrency(sb.cost);
            //System.out.println("bought " + sb.buttonPart + " for total of " + PlayerProfile.inventory.get(sb.buttonPart));
        }
        else {
            System.out.println("cannot afford " + sb.buttonPart);
        }

    }

    @Override
    public void update() {
        super.update();
        for (ShopButton sb : itemButtons) {
            if (PlayerProfile.hasItem(sb.buttonPart)) {
                sb.activeColor = new Color(0.5f, 0.5f, 1, 1);
                sb.inactiveColor = new Color(0, 0, 0.5f, 1);
            } else {
                sb.activeColor = new Color(1, 1, 1, 1);
                sb.inactiveColor = new Color(0.5f, 0.5f, 0.5f, 1);
            }
            if (PlayerProfile.getWallet() < sb.cost) {
                sb.pCanBuy = false;
            }
            else if (PlayerProfile.getWallet() >= sb.cost) {
                sb.pCanBuy = true;
            }
            sb.update();
        }
        for (int i = 0; i < 4; i++) {
            sectionLabelDrawers[i].update();
        }
    }

    @Override
    public void renderGUI(Batch batch) {
        super.renderGUI(batch);
        for (ShopButton sb : itemButtons) {
            sb.renderGUI(batch);
        }
        for (int i = 0; i < 4; i++) {
            sectionLabelDrawers[i].renderGUI(batch);
        }
        walletLabelDraw.str = walletLabel +  Integer.toString(PlayerProfile.getWallet());
        walletLabelDraw.renderGUI(batch);
    }
}
