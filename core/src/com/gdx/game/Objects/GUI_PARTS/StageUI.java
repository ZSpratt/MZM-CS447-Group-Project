package com.gdx.game.Objects.GUI_PARTS;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GUIBox;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.BaseObjects.StringDraw;
import com.gdx.game.PlayerProfile;

/**
 * Created by zachm on 12/6/2017.
 */
public class StageUI extends Entity {
	public int enemyCount = 0;
	public int zombieCount = 0;
	public float roundTime = 100;
	public int lives = 3;
	public String message = "";

	private GUIBox Enemies = new GUIBox(-350, -200, 200, 64, 0);
	private StringDraw enemies = new StringDraw("Zombie Mech : ", -350, -200, 0, 4);

	private GUIBox Zombies = new GUIBox(350, -200, 200, 64, 0);
	private StringDraw zombies = new StringDraw("Zombies : ", 350, -200, 0, 4);

	private GUIBox Wallet = new GUIBox(0, -200, 200, 32, 0);
	private StringDraw wallet = new StringDraw("Zombies : ", 0, -200, 0, 4);

	private GUIBox Time = new GUIBox(0, 200, 200, 98, 0);
	private StringDraw Smessage = new StringDraw("Message", 0, 200+8, 0, 4);
	private StringDraw time = new StringDraw("00:00:00", 0, 200-16, 0, 4);
	private StringDraw livesString = new StringDraw("0 : Lives Remaining", 0, 200-32, 0, 4);

	public StageUI() {
		super(new Vector2(), 0, 0, null);
	}

	public String floatTimeString(float time) {
		return String.format("%02d:%02d:%02d", (int) (time/3600), ((int) (time%3600)/60),  ((int) time % 60));
	}

	@Override
	public void update() {
		Enemies.update();
		Enemies.lateUpdate();
		enemies.str = "Zombie Mech : " + enemyCount;
		enemies.update();
		enemies.lateUpdate();

		Zombies.update();
		Zombies.lateUpdate();
		zombies.str = "Zombies : " + zombieCount;
		zombies.update();
		zombies.lateUpdate();

		Wallet.update();
		Wallet.lateUpdate();
		wallet.str = "$ " + PlayerProfile.getWallet() + " $";
		wallet.update();
		wallet.lateUpdate();

		Time.update();
		Time.lateUpdate();
		Smessage.str = message;
		Smessage.update();
		Smessage.lateUpdate();
		time.str = floatTimeString(roundTime);
		time.update();
		time.lateUpdate();
		livesString.str = String.format("%02d : Lives Remaining", lives);
		livesString.update();
		livesString.lateUpdate();
	}

	@Override
	public void renderGUI(Batch batch) {
		Enemies.renderGUI(batch);
		enemies.renderGUI(batch);

		Zombies.renderGUI(batch);
		zombies.renderGUI(batch);

		Wallet.renderGUI(batch);
		wallet.renderGUI(batch);

		Time.renderGUI(batch);
		Smessage.renderGUI(batch);
		time.renderGUI(batch);
		livesString.renderGUI(batch);
	}
}
