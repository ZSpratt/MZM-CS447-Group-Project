package com.gdx.game.Objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.BaseObjects.NetworkEntity;
import com.gdx.game.CreatePolygon;
import com.gdx.game.LevelLoader;

public class Objective extends NetworkEntity{

	int objImage = 0;
	public Objective(int i, Vector2 pos, float angle, int z, GameObject parent) {
		super(pos, angle, z, parent);
		objImage = i;
		sprite = new Sprite(LevelLoader.objectivesImages.get(i));
		localPos.add(sprite.getWidth()/2, sprite.getHeight()/2);
		hasPhysics = true;
		kinematic = true;
		canMoveObjects = true;
		collider = CreatePolygon.rect(sprite.getWidth(), sprite.getHeight());
		team = -2;
	}

	public Objective(String s) {
		super(s);
		if (LevelLoader.objectivesImages != null && LevelLoader.objectivesImages.size() > 0) {
			sprite = new Sprite(LevelLoader.objectivesImages.get(0));
		}
		hasPhysics = true;
		kinematic = true;
		canMoveObjects = true;
		if (sprite != null) {
			collider = CreatePolygon.rect(sprite.getWidth(), sprite.getHeight());
		}
		team = -2;
	}



	@Override
	public void render(Batch batch) {
		if (sprite != null) {
			super.render(batch);
		}
	}

	@Override
	public void onCollision(Entity other) {
		if (other instanceof Mech && ((Mech) other).isPlayer) {
			Delete();
		}
	}

	@Override
	public synchronized String serialize() {
		return super.serialize() + "  " + objImage;
	}

	@Override
	public synchronized void deSerialize(String[] s) {
		super.deSerialize(s);
		try {
			if (s.length > 6) {
				objImage = Integer.parseInt(s[6]);
				if (LevelLoader.objectivesImages != null && LevelLoader.objectivesImages.size() > objImage)
				sprite = new Sprite(LevelLoader.objectivesImages.get(objImage));
				collider = CreatePolygon.rect(sprite.getWidth(), sprite.getHeight());
			}
		} catch (Exception e) {

		}
	}
}
