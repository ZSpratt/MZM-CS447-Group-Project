package com.gdx.game.Objects.MechParts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.Game;
import com.gdx.game.Objects.Projectiles.Projectile;
import com.gdx.game.Settings;
import com.gdx.game.Time;

public class  MechArm extends MechPart {

    private Vector2 firePoint;
	private float fireRate = 1;
	private float fireTime = 0;
	private String round = "Bullet";

	public boolean left = false;

    public MechArm(String resource, GameObject parent) {
        super(resource, parent);
		setPart(resource);
    }

    @Override
    public void parsePartData(String[] line) {
        super.parsePartData(line);
        if (line[0].equals("FirePoint")) {
            //System.out.println("Fire");
            String[] args = line[1].split(",");
            firePoint = new Vector2(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
        }
		if (line[0].equals("FireRate")) {
			fireRate = Float.parseFloat(line[1]);
		}
		if (line[0].equals("Projectile")) {
			round = line[1];
		}
    }

    @Override
    public void setPart(String resource) {
        firePoint = new Vector2();
		fireRate = -1;
        super.setPart(resource);
    }

    @Override
    public void setRotation(float x, float y) {
        Vector2 socket = new Vector2(pos).sub(new Vector2(offset).rotate(rot));
        Vector2 fire = new Vector2(pos).add(new Vector2(firePoint).rotate(rot));
        Vector2 delta = new Vector2(socket).sub(x, y);
		float tRot = localRot;

        if (mirrorX) {
            socket = new Vector2(pos).sub(new Vector2(-offset.x , offset.y).rotate(rot));
            fire = new Vector2(pos).add(new Vector2(-firePoint.x, firePoint.y).rotate(rot));
            delta = new Vector2(socket).sub(x, y);
        }

        //check to see if cursor lies within buffer zone so arms dont spin wildly
        /*
		if(delta.len() < fire.len() - socket.len() + 150){
            if(delta.len() >= socket.len() + 60 ){
                delta = delta.scl(100);
                delta.set(delta.x, delta.y + 100 * (fire.y - socket.y));
            }

            //if extremely close to socket
            else{
                if(delta.len() > 150)
                    delta = delta.scl(100);
                else
                    delta.scl(200);
            }
        }

        Vector2 targetAngle = new Vector2(socket).sub(delta).sub(fire);
        */

        localRot = delta.angle() + 180 - tRot;
        if (mirrorX) {
            localRot += 180;
            Vector2 tmpOff = new Vector2(offset);
            tmpOff.x *= -1;
            localPos.add(new Vector2(tmpOff).rotate(localRot));
			localRot = localRot + tRot;
        } else {
            localPos.add(new Vector2(offset).rotate(localRot));
			localRot = localRot + tRot;
        }
        lateUpdate();
    }

    public boolean autoFire = false;
    @Override
    public void update() {
        if (myMech != null) {
            super.update();
            if (myMech.enabled) {
            	//System.out.println(myMech.direction + " : " + mirrorX);
				setRotation(myMech.aimPoint.x, myMech.aimPoint.y);

				if (fireRate > 0) {
					if (fireTime > fireRate) {
						fireTime = fireRate;
					}
				}
				fireTime += Time.deltaTime;
				if ((myMech.isLocal && !Game.paused && myMech.isPlayer && myMech.canMove && !(Gdx.input.isKeyPressed(Settings.sprint))) || autoFire) {
					if (((left && Gdx.input.isButtonPressed(Input.Buttons.LEFT)) || (!left && Gdx.input.isButtonPressed(Input.Buttons.RIGHT))) || autoFire) {
						while (fireTime > fireRate && fireRate > 0) {
							float forward = Time.deltaTime * (int) (fireTime / fireRate - 1);
							fireTime -= fireRate;
							shoot(forward);
						}
						if (fireRate <= 0) {
							shoot(0);
						}
					}
				}
			} else {
            	if (!mirrorX) {
					setRotation(100000, 0);
				} else {
					setRotation(-100000, 0);
				}
			}
        }
    }


	private void shoot(float forward) {
		if (mirrorX) {
			Projectile p = new Projectile(round, new Vector2(pos).add(new Vector2(firePoint.x, -firePoint.y).rotate(rot + 180)), rot + 180, forward, 1, null);
			p.team = myMech.team;
			Game.addGameObject(p);
		} else {
			Projectile p = new Projectile(round, new Vector2(pos).add(new Vector2(firePoint).rotate(rot)), rot, forward, 1, null);
			p.team = myMech.team;
			Game.addGameObject(p);
		}
	}
}