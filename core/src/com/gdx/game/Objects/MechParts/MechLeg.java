package com.gdx.game.Objects.MechParts;

import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.GameObject;

public class MechLeg extends MechPart {
    public MechLeg(String resource, GameObject parent) {
        super(resource, parent);
        setPart(resource);
    }

    private float jumpPower;
    private float moveSpeed;

    public float getJumpPower() {
        return jumpPower;
    }

    public float getMoveSpeed() {
        return moveSpeed;
    }

    @Override
    public void parsePartData(String[] line) {
        if (line[0].contains("Primary-Frames")) {
            primaryFrames = Integer.parseInt(line[1]);
        }
        if (line[0].contains("Secondary-Frames")) {
            secondaryFrames = Integer.parseInt(line[1]);
        }
        if (line[0].contains("Origin")) {
            String[] args = line[1].split(",");
            offset = new Vector2(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
        }
        if (line[0].contains("Connect")) {
            String[] args = line[1].split(",");
            Vector2 point = new Vector2(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
            connectPoints.add(point);
        }
        if (line[0].contains("Jump-Power")) {
            String[] args = line[1].split(",");
            jumpPower = Float.parseFloat(args[0]);
            //System.out.println(this + "set jump " + jumpPower);
        }
        if (line[0].contains("Move-Speed")) {
            String[] args = line[1].split(",");
            moveSpeed = Float.parseFloat(args[0]);
            //System.out.println(this + "set speed " + moveSpeed);
        }
    }

    @Override
    public void update() {
        team = myMech.team;
        localPos.set(offset);
        localPos.scl(-1);
    }
}