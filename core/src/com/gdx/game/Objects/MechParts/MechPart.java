package com.gdx.game.Objects.MechParts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.AnimatedEntity;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.CreatePolygon;
import com.gdx.game.Game;
import com.gdx.game.Objects.Mech;
import com.gdx.game.ResourceManager;
import com.gdx.game.Time;

import java.security.Key;
import java.text.ParseException;
import java.util.ArrayList;

public class MechPart extends AnimatedEntity{
	public ArrayList<Vector2> connectPoints = new ArrayList<Vector2>();
	public ArrayList<Connection> connections = new ArrayList<Connection>();
	public boolean mirrorX = false;

	public String currentResource = "";
	public Color primaryColor = Color.WHITE;
	public Color secondaryColor = Color.WHITE;
	public boolean invertPart = false;
	public float health = 20;

	public Animation<TextureRegion> secondaryAnim;
	protected int primaryFrames = 1;
	protected int secondaryFrames = 1;

	public Mech myMech;
	public MechPart(String resource, GameObject parent) {
		super(parent.pos, 0, 0, parent);

		updatePart(resource);
		hasPhysics = true;
		canMoveObjects = false;
		kinematic = true;
		Game.physicsObjects.add(this);
		myMech = (Mech) parent;
	}

	public void parsePartData(String[] line) {
		if (line[0].contains("Primary-Frames")) {
			//System.out.println(line[1]);
			primaryFrames = Integer.parseInt(line[1]);
		}
		if (line[0].contains("Secondary-Frames")) {
			secondaryFrames = Integer.parseInt(line[1]);
		}
		if (line[0].contains("Origin")) {
			String[] args = line[1].split(",");
			offset = new Vector2(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
		}
		if (line[0].contains("Connect")) {
			String[] args = line[1].split(",");
			Vector2 point = new Vector2(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
			connectPoints.add(point);
			//System.out.println(this + "added point " + point);
		}
		if (line[0].contains("Health")) {
			//System.out.println(line[1]);
			health = Float.parseFloat(line[1]);
		}
	}

	public void updatePart(String resource) {
		health = 20;
		if (!currentResource.equals(resource)) {
			setPart(resource);
		}

		TextureRegion tr = (TextureRegion) anim.getKeyFrame(stateTime, looping);

		if (! (this instanceof MechArm)) {
			if (this instanceof MechLeg) {
				TextureRegion tr2 = (TextureRegion) ((Mech) parent).body.anim.getKeyFrame(stateTime, looping);
				collider = CreatePolygon.trap(tr.getRegionWidth(), tr2.getRegionWidth(), tr.getRegionHeight());
			} else {
				collider = CreatePolygon.rect(tr.getRegionWidth(), tr.getRegionHeight());
			}
		} else {
			collider = null;
			hasPhysics = false;
		}
	}

	public void setPart(String resource) {
		currentResource = resource;
		connectPoints.clear();
		localPos.set(0, 0);
		anim = null;
		secondaryAnim = null;
		FileHandle resouceData = Gdx.files.internal(resource);
		if (resouceData.exists()) {
			String[] lines = resouceData.readString().split("\n");
			for (int i = 0; i < lines.length; i++) {
				String line = lines[i].replaceAll("\\s+", "");
				String[] data = line.split(":");
				if (data.length != 2) {
					System.err.println("Line Error in file " + resource + "\nOn Line " + i + "\n" + lines[i]);
				} else {
					parsePartData(data);
				}
				setResource(resource, primaryFrames, secondaryFrames);
			}
		} else {
			setResource(resource, 1, 1);
		}
	}

	public void setResource(String resource, int pFrames, int sFrames) {
		Animation<TextureRegion> prim = ResourceManager.getAnim(resource + ".png", pFrames, 1f / pFrames);
		if (Gdx.files.internal(resource + "Sec.png").exists()) {
			secondaryAnim = ResourceManager.getAnim(resource + "Sec.png", sFrames, 1f / sFrames);
		}
		setAnim(prim, secondaryAnim, 0);
	}

	public void setAnim(Animation<TextureRegion> primary, Animation<TextureRegion> secondary, float time) {
		super.setAnim(primary, time);
		this.secondaryAnim = secondary;

	}

	public void connect(MechPart part, int point) {
		connections.add(new Connection(part, point));
		setParent(part);
	}

	@Override
	public void update() {
		super.update();
		team = myMech.team;

		if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
			setPart(currentResource);
		}

		if (connections.size() > 0) {
			Vector2 finalConnection = connections.get(0).getPoint();
			for (int i = 1; i < connections.size(); i++) {
				Vector2 newPoint = connections.get(i).getPoint();
				if (newPoint.y > finalConnection.y) {
					finalConnection.y = newPoint.y;
				}
				finalConnection.x += newPoint.x;
			}
			finalConnection.x /= connections.size();
			if (! (this instanceof MechArm)) {
				finalConnection.add(offset);
			}

			if (mirrorX) {
				finalConnection.x *= -1;
			}
			localPos.set(finalConnection);
		}
	}

	public void setRotation(float x, float y) {
		//System.out.println(localPos);
		localRot = (float) Math.toDegrees(Math.atan2(y - pos.y, x - pos.x));
		if (mirrorX) {
			localRot += 180;
			Vector2 tmpOff = new Vector2(offset);
			tmpOff.x *= -1;
			localPos.add(new Vector2(tmpOff).rotate(localRot));
		} else {
			localPos.add(new Vector2(offset).rotate(localRot));
		}
	}

	private class Connection{
		MechPart part;
		int index;
		public Connection(MechPart part, int index) {
			this.part = part;
			this.index = index;
		}

		public Vector2 getPoint() {
			Vector2 retVec = new Vector2();
			if (part.connectPoints.size() > index) {
				retVec.set(part.connectPoints.get(index));
			}
			return retVec;
		}
	}

	@Override
	public void render(Batch batch) {
		if (anim != null) {
		//Wastes some time, but in order for current frame to update, the frame must be drawn... bad code.
		stateTime += Time.deltaTime * playbackSpeed;
		Color a = primaryColor;
		Color b = secondaryColor;
		if (invertPart) {
			b = primaryColor;
			a = secondaryColor;
		}
		drawFrame(anim, a, batch);
		if (secondaryAnim != null) {
			drawFrame(secondaryAnim, b, batch);
		}

		} else if (sprite != null) {
			super.render(batch);
		}
	}

	private void drawFrame(Animation<TextureRegion> anim, Color c, Batch batch) {
		if (!anim.isAnimationFinished(stateTime) | looping) {
			currentFrame = new Sprite((TextureRegion) anim.getKeyFrame(stateTime, looping));

			currentFrame.setScale(scale);
			if (mirrorX) {
				currentFrame.setScale(-currentFrame.getScaleX(), currentFrame.getScaleY());
			}
			currentFrame.setRotation(rot);
			currentFrame.setCenter(pos.x, pos.y);
			currentFrame.setColor(c);
			currentFrame.draw(batch);
		}
	}

	@Override
	public void onDelete() {
		super.onDelete();
		Game.physicsObjects.remove(this);
	}
}
