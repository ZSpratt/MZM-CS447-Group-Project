package com.gdx.game.Objects.MechParts;

import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.GameObject;

public class MechTorso extends MechPart {
    public MechTorso(String resource, GameObject parent) {
        super(resource, parent);
    }
}