package com.gdx.game.Objects.MechParts;

public class PartsList {
	public static String[] Head = {"Mechs/X-1TE Destroyer/X-1TE Head", "Mechs/AngularTitan/ATitan-C75", "Mechs/Void Star/VS Head", "Mechs/SE-19 Berzerker/Berzerker Head", "Mechs/Metal Knight N12/MK Head"};
	public static String[] Torso = {"Mechs/X-1TE Destroyer/X-1TE Torso", "Mechs/AngularTitan/ATitan-T74", "Mechs/Void Star/VS Torso", "Mechs/SE-19 Berzerker/Berzerker Torso", "Mechs/Metal Knight N12/MK Torso"};
	public static String[] Arm = {"Mechs/X-1TE Destroyer/Gattling Gun", "Mechs/X-1TE Destroyer/Boxer Hand", "Mechs/AngularTitan/Dominator", "Mechs/AngularTitan/Rock-it",  "Mechs/Void Star/Star Claw", "Mechs/SE-19 Berzerker/Berzerk Gloves", "Mechs/SE-19 Berzerker/Flamethrower", "Mechs/SE-19 Berzerker/Shotgun", "Mechs/Metal Knight N12/U-90 Long Cannon", "Mechs/Metal Knight N12/E-20 Zapper"};
	public static String[] Leg = {"Mechs/X-1TE Destroyer/X-1TE Legs", "Mechs/AngularTitan/ATitan-L45", "Mechs/Void Star/VS Legs", "Mechs/SE-19 Berzerker/Berzerker Legs", "Mechs/Metal Knight N12/MK Legs"};
}
