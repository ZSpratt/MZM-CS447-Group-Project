package com.gdx.game.Objects.MechParts;


import com.badlogic.gdx.math.Vector2;
import com.gdx.game.CreatePolygon;
import com.gdx.game.Game;
import com.gdx.game.Objects.ZombieBlood;

public class MechGib extends ZombieBlood {


	public MechGib(Vector2 pos, float angle) {
		super(pos, angle);

		setSprite("Mechs/MechGibs/Gib " + ((int) (Game.random.nextFloat() * 4) + 1) + ".png");
		collider = CreatePolygon.rect(sprite.getWidth()/2, sprite.getHeight()/2);

		velocity.set(0, 200 + Game.random.nextFloat() * 100);
		velocity.setAngle(angle);
	}


}
