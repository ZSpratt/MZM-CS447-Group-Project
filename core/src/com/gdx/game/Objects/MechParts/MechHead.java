package com.gdx.game.Objects.MechParts;

import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.GameObject;

public class MechHead extends MechPart {
    public MechHead(String resource, GameObject parent) {
        super(resource, parent);
    }
}
