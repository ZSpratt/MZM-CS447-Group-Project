package com.gdx.game.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.*;
import com.gdx.game.BaseObjects.*;
import com.gdx.game.Objects.MechParts.*;
import com.gdx.game.Objects.Projectiles.Explosion;

import java.util.ArrayList;
import java.util.Set;

public class Mech extends NetworkEntity {
    // potentially more than 2 arms and legs
    public ArrayList<MechArm> arms = new ArrayList<MechArm>();
    public MechLeg legs;
    // just 1 head and body
    public MechPart body;
    public MechPart head;
    public int direction = 1;
    private boolean isAirborn;
	public boolean showHealth = true;

    public Color primaryColor = new Color(Color.WHITE);
    public Color secondaryColor = new Color(Color.WHITE);

    public Vector2 aimPoint = new Vector2();

    public boolean isPlayer = true;
    public boolean right = true;
    private WalkCollider[] walkTargets;
    private ShootCollider shootTarget;
    private Mech target;

    float maxHealth = 100;
    public float health = 100;
    private GUIFillBar healthBar;

    public boolean canMove = true;

    public Mech(Vector2 pos, float angle, int z, GameObject parent) {
        super(pos, angle, z, parent);
		initMech();
		hasPhysics = true;
		kinematic = false;
		collider = CreatePolygon.rect(50, 1, 0, 0);
    }

	public Mech(String s) {
		super(s);
		initMech();
		hasPhysics = true;
		kinematic = false;
		collider = CreatePolygon.rect(50, 1, 0, 0);
	}

	public void initMech() {
		String newLegs = PartsList.Leg[0];
		String newTorso = PartsList.Torso[0];
		String newArmL = PartsList.Arm[0];
		String newArmR = PartsList.Arm[0];
		String newHead = PartsList.Head[0];

		body = new MechPart(newTorso, this);
		body.invertPart = (Game.random.nextBoolean());
		head = new MechPart(newHead, this);
		head.invertPart = (Game.random.nextBoolean());
		arms.add(new MechArm(newArmL, this));
		arms.get(0).invertPart = (Game.random.nextBoolean());
		arms.add(new MechArm(newArmR, this));
		arms.get(1).invertPart = (Game.random.nextBoolean());
		legs = new MechLeg(newLegs, this);
		legs.invertPart = (Game.random.nextBoolean());

		/*
		legs.get(0).connectPoints.add(new Vector2(0, 30));
		legs.get(0).offset.set(0, -32);
		legs.get(1).connectPoints.add(new Vector2(0, 30));
		legs.get(1).offset.set(0, -32);
		legs.get(1).stateTime = 0.5f;
		body.offset.set(0, 18);
		body.connectPoints.add(new Vector2(-10, 13));
		body.connectPoints.add(new Vector2(0, 30));
		*/
		legs.stateTime = 0;
		body.connect(legs, 0);
		arms.get(0).connect(body, 0);
		arms.get(0).left = true;
		arms.get(1).connect(body, 0);
		arms.get(1).left = false;
		head.connect(body, 1);

		healthBar = new GUIFillBar(0, 0, 100, 10, 10);
	}

	public void randomParts() {
		String newLegs = PartsList.Leg[(int) (Game.random.nextFloat() * PartsList.Leg.length)];
		String newTorso = PartsList.Torso[(int) (Game.random.nextFloat() * PartsList.Torso.length)];
		String newArmL = PartsList.Arm[(int) (Game.random.nextFloat() * PartsList.Arm.length)];
		String newArmR = PartsList.Arm[(int) (Game.random.nextFloat() * PartsList.Arm.length)];
		String newHead = PartsList.Head[(int) (Game.random.nextFloat() * PartsList.Head.length)];

		body.updatePart(newTorso);
		head.updatePart(newHead);
		arms.get(0).updatePart(newArmL);
		arms.get(1).updatePart(newArmR);
		legs.updatePart(newLegs);

		body.invertPart = (Game.random.nextBoolean());
		head.invertPart = (Game.random.nextBoolean());
		arms.get(0).invertPart = (Game.random.nextBoolean());
		arms.get(1).invertPart = (Game.random.nextBoolean());
		legs.invertPart = (Game.random.nextBoolean());
	}

	@Override
    public void update() {
    	if (isPlayer) {
    		team = 0;
		} else {
    		team = 1;
		}

		float delta = health/maxHealth;
		maxHealth = head.health + body.health + arms.get(0).health + arms.get(1).health + legs.health;
		if (!isPlayer) {
			maxHealth /= 4f;
		}
		health = delta * maxHealth;


        super.update();
    	if (isPlayer) {
			if (isLocal && !Game.paused) {
				canMove = !Gdx.input.isKeyPressed(Settings.repair);
				if (canMove) {
					aimPoint.set(Game.mouseX, Game.mouseY);
					velocity.x = 0;

					MechLeg tmp0 = (MechLeg) legs;
					//MechLeg tmp1 = (MechLeg)legs.get(1);

					float speedPowet = 1;
					((MechLeg) legs).playbackSpeed = 1;
					if (Gdx.input.isKeyPressed(Settings.sprint)) {
						speedPowet = 4;
						((MechLeg) legs).playbackSpeed = 4;
					}
					if (Gdx.input.isKeyPressed(Settings.up)) {
						if (!isAirborn) velocity.y += tmp0.getJumpPower();
						isAirborn = true;
					}
					if (Gdx.input.isKeyPressed(Settings.down)) {
						//velocity.y -= tmp0.getJumpPower();
					}
					if (Gdx.input.isKeyPressed(Settings.right)) {
						velocity.x += tmp0.getMoveSpeed() * speedPowet;
					}
					if (Gdx.input.isKeyPressed(Settings.left)) {
						velocity.x -= tmp0.getMoveSpeed() * speedPowet;
					}
				} else {
					velocity.x = 0;
					if (health/maxHealth < 0.75) {
						float dHealth = 10 * maxHealth / 100 * Time.deltaTime;
						if (PlayerProfile.getWallet() > dHealth * 10) {
							PlayerProfile.remCurrency(dHealth * 10);
							health += dHealth;
							if (health > maxHealth) {
								health = maxHealth;
							}
						}
					}
				}
			} else if (Game.paused) {
				velocity.x = 0;
			}
		} else {
    		if (isLocal) {
				if (walkTargets == null) {
					walkTargets = new WalkCollider[2];
					walkTargets[0] = new WalkCollider(new Vector2(pos.x + 50, pos.y-64), this);
					walkTargets[1] = new WalkCollider(new Vector2(pos.x + 50, pos.y + 64), this);
					Game.physicsObjects.add(walkTargets[0]);
					Game.physicsObjects.add(walkTargets[1]);
					shootTarget = new ShootCollider(new Vector2(pos.x, pos.y + 100), this);
					Game.physicsObjects.add(shootTarget);
				} else {
					velocity.x = 0;
					if (target == null) {
						if (right) {
							walkTargets[0].localPos.x = 50;
							walkTargets[1].localPos.x = 50;
							if (walkTargets[0].hasGround && !walkTargets[1].hasGround) {
								velocity.x = legs.getMoveSpeed() * (0.25f + 0.25f * Game.random.nextFloat());
							} else {
								velocity.x = 0;
								right = false;
								walkTargets[0].localPos.x = -50;
								walkTargets[1].localPos.x = -50;
							}
						} else {
							walkTargets[0].localPos.x = -50;
							walkTargets[1].localPos.x = -50;
							if (walkTargets[0].hasGround && !walkTargets[1].hasGround) {
								velocity.x = -legs.getMoveSpeed() * (0.25f + 0.25f * Game.random.nextFloat());
							} else {
								velocity.x = 0;
								right = true;
								walkTargets[0].localPos.x = +50;
								walkTargets[1].localPos.x = +50;
							}
						}
					}

					if (target == null) {
						arms.get(0).autoFire = false;
						arms.get(1).autoFire = false;
						if (shootTarget.newTarget != null) {
							target = shootTarget.newTarget;
						}
						if (isLocal) {
							if (direction == -1) {
								aimPoint.set(pos.x - 100000, pos.y);
							} else {
								aimPoint.set(pos.x + 100000, pos.y);
							}
						}
					} else {
						if (isLocal) {
							aimPoint.set(target.pos.x - 25 + Game.random.nextFloat() * 50, target.pos.y + Game.random.nextFloat() * 128);
						}
						arms.get(0).autoFire = true;
						arms.get(1).autoFire = true;

						right = (target.pos.x > pos.x);

						if (!Operations.CompareVector(pos, target.pos, 2048) || target.delete) {
							target = null;
						}
					}



					walkTargets[0].update();
					walkTargets[0].lateUpdate();
					walkTargets[1].update();
					walkTargets[1].lateUpdate();
					shootTarget.update();
					shootTarget.lateUpdate();
				}
			}
		}


		if (velocity.x < 0) {
			direction = -1;
		}

		if (velocity.x > 0) {
			direction = 1;
		}

		if (velocity.x == 0) {
			legs.stateTime = 0;
			legs.playbackSpeed = 1;
		}

		legs.mirrorX = (direction == -1);
		legs.localRot = localRot;
		legs.update();
		body.update();
		for (MechPart arm : arms) {
			arm.mirrorX = (direction == -1);
			arm.localRot = localRot;
			arm.update();
		}
		head.mirrorX = (direction == -1);
		head.localRot = localRot;
		head.update();
		body.mirrorX = (direction == -1);
		body.localRot = localRot;
		body.update();

		float height = head.pos.y + head.getCollider().getVertices()[5] - pos.y;

		if (isPlayer) {
			collider = CreatePolygon.rect(50, height, 0, height / 2);
		} else {
			collider = CreatePolygon.rect(50, height, 0, height / 2);
		}

		if (isPlayer) {
			healthBar.fullColor = Color.GREEN;
			healthBar.midColor = Color.YELLOW;
			healthBar.emptyColor = Color.RED;
		} else {
			if (!Game.connected || Game.isServer) {
				colorSlide += Time.deltaTime;
				primaryColor = Operations.interpolateColor(Color.WHITE, Color.RED, (float) Math.abs(Math.sin((colorSlide * 3))), 5);
				secondaryColor = Operations.interpolateColor(Color.RED, Color.WHITE, (float) Math.abs(Math.sin((colorSlide * 3))), 5);
			}
			healthBar.fullColor = Color.BLUE;
			healthBar.midColor = Color.GRAY;
			healthBar.emptyColor = Color.BLACK;
		}

		healthBar.fill = health / maxHealth;
		if (healthBar.fill < 0) {
			healthBar.fill = 0;
		}
		healthBar.update();
		if (isLocal) {
			if (health <= 0) {
				Delete();
			}
		}
    }

    float colorSlide = 0;

    @Override
    public void lateUpdate() {
        super.lateUpdate();
        if (isPlayer && isLocal) {
        	if (Gdx.input.isKeyPressed(Settings.down)) {
        		velocity.y += 4 * Time.deltaTime * Game.gravity.y;
			}
		}
        if (legs != null && arms != null && head != null && body != null) {
			legs.lateUpdate();
			body.lateUpdate();
			for (MechPart arm : arms) {
				arm.lateUpdate();
			}
			head.lateUpdate();
			body.lateUpdate();
			if (isLocal && isPlayer && enabled && !delete) {
				Game.mainCamera.position.set(pos.x, pos.y + 64, 0);
				Game.mainCamera.zoom += InProcess.scroll * 0.125f;
				if (Game.mainCamera.zoom < 1) {
					Game.mainCamera.zoom = 1;
				}

				if (Game.mainCamera.zoom > 10) {
					Game.mainCamera.zoom = 10;
				}
			}

			showHealth = health < maxHealth;
			if (showHealth) {
				healthBar.localPos.set(pos).add(0, -32);
				healthBar.lateUpdate();
				healthBar.pos.set(Operations.worldToCamera(healthBar.localPos));
				healthBar.width = 256 / Game.mainCamera.zoom;
				healthBar.height = 32 / Game.mainCamera.zoom;
			}
		}
    }

    @Override
    public void render(Batch batch) {
    	int i = 0;
    	if (direction != 1) {
    		i += 1;
		}

		if (isLocal) {
			head.invertPart = PlayerProfile.inverts[4];
			body.invertPart = PlayerProfile.inverts[1];
			arms.get(0).invertPart = PlayerProfile.inverts[2];
			arms.get(1).invertPart = PlayerProfile.inverts[3];
			legs.invertPart = PlayerProfile.inverts[0];
		}

		arms.get(i).primaryColor = Operations.multiplyColor(primaryColor, Game.backgroundColor, 0.8f);
    	arms.get(i).secondaryColor = Operations.multiplyColor(secondaryColor, Game.backgroundColor, 0.8f);
		arms.get(i).render(batch);
		legs.primaryColor = Operations.multiplyColor(primaryColor, Game.backgroundColor, 0.8f);
		legs.secondaryColor = Operations.multiplyColor(secondaryColor, Game.backgroundColor, 0.8f);
		float tmp = legs.stateTime;
		legs.render(batch);
		head.primaryColor = primaryColor;
		head.secondaryColor = secondaryColor;
		head.render(batch);
		body.primaryColor = primaryColor;
		body.secondaryColor = secondaryColor;
		body.render(batch);
		i = (i + 1) % 2;
		legs.primaryColor = primaryColor;
		legs.secondaryColor = secondaryColor;
		legs.stateTime = tmp + 0.5f;
		legs.render(batch);
		legs.stateTime -= 0.5f;

		arms.get(i).primaryColor = primaryColor;
		arms.get(i).secondaryColor = secondaryColor;
		arms.get(i).render(batch);
    }

	@Override
	public void renderGUI(Batch batch) {
		if (showHealth) {
			healthBar.renderGUI(batch);
		}
	}

	@Override
	public void onCollision(Entity other) {
		if (other.team == -1 || other.team == -2) {
			if (delta.y != 0) {
				if (other.team == -1 || (other.team == -2 && !(Gdx.input.isKeyPressed(Settings.down)))) {
					if (other.team != -2 || (other.team == -2 && delta.y < 5)) {
						velocity.y = 0;
						if (delta.y > 0) {
							isAirborn = false;
						}
					}
				}
			}
		}
	}

	public void pushBack(Vector2 a, float d, Entity other) {
		delta.set(a);
		delta.scl(d);
		//System.out.println(delta.y);
		if (other.team != -2 || !Gdx.input.isKeyPressed(Settings.down)) {
			if (other.team != -2 || (other.team == -2 && delta.y < 5)) {
				localPos.add(delta);
				pos.add(delta);
			}
		}
	}

	@Override
	public void onDelete() {
		super.onDelete();
		if (!delete) {
			if (walkTargets != null) {
				Game.physicsObjects.remove(walkTargets[0]);
				Game.physicsObjects.remove(walkTargets[1]);
				Game.physicsObjects.remove(shootTarget);
			}

			if (isLocal) {
				Game.addGameObject(new Explosion(new Vector2(Game.random.nextFloat() * 10, Game.random.nextFloat() * 10).add(head.pos), 0));
				Game.addGameObject(new Explosion(new Vector2(Game.random.nextFloat() * 10, Game.random.nextFloat() * 10).add(body.pos), 0));
				Game.addGameObject(new Explosion(new Vector2(Game.random.nextFloat() * 10, Game.random.nextFloat() * 10).add(legs.pos), 0));
				Game.addGameObject(new Explosion(new Vector2(Game.random.nextFloat() * 10, Game.random.nextFloat() * 10).add(arms.get(0).pos), 0));
				Game.addGameObject(new Explosion(new Vector2(Game.random.nextFloat() * 10, Game.random.nextFloat() * 10).add(arms.get(1).pos), 0));
			}

			int gibs = (int) ((20 + Game.random.nextFloat() * 10) * Settings.gibScale);
			for (int i = 0; i < gibs; i++) {
				Vector2 spawn = new Vector2(pos);
				spawn.x -= 25;
				spawn.add(new Vector2(Game.random.nextFloat() * 50, Game.random.nextFloat() * 128));
				MechGib g = new MechGib(spawn, Game.random.nextFloat() * 360);
				if (Game.random.nextFloat() > 0.5f) {
					g.color = new Color(primaryColor);
				} else {
					g.color = new Color(secondaryColor);
				}
				Game.addGameObject(g);
			}
		}
		if (!isPlayer) {
			PlayerProfile.addCurrency((int) (2.5f * maxHealth));
		}
	}

	@Override
	public void deSerialize(String[] s) {
		super.deSerialize(s);
		//System.out.println(list.length);
		if (arms != null) {
			//System.out.println(velocity);
			velocity.set(Float.parseFloat(s[4]), Float.parseFloat(s[5]));
			//velocity.set(0, 0);
			direction = Integer.parseInt(s[8]);
			aimPoint.set(Float.parseFloat(s[6]), Float.parseFloat(s[7]));
			aimPoint.set(Float.parseFloat(s[6]), Float.parseFloat(s[7]));

			primaryColor.r = Float.parseFloat(s[9]);
			primaryColor.g = Float.parseFloat(s[10]);
			primaryColor.b = Float.parseFloat(s[11]);

			secondaryColor.r = Float.parseFloat(s[12]);
			secondaryColor.g = Float.parseFloat(s[13]);
			secondaryColor.b = Float.parseFloat(s[14]);

			head.updatePart(s[15]);
			body.updatePart(s[20]);
			arms.get(0).updatePart(s[16]);
			arms.get(1).updatePart(s[17]);
			legs.updatePart(s[18]);
			legs.updatePart(s[19]);

			health = Float.parseFloat(s[21]);
			isPlayer = s[22].equals("true");

			head.invertPart = Boolean.parseBoolean(s[23]);
			body.invertPart = Boolean.parseBoolean(s[24]);
			arms.get(0).invertPart = Boolean.parseBoolean(s[25]);
			arms.get(1).invertPart = Boolean.parseBoolean(s[26]);
			legs.invertPart = Boolean.parseBoolean(s[27]);
			legs.invertPart = Boolean.parseBoolean(s[28]);

			update();
			lateUpdate();
		}
	}

	@Override
    public String serialize() {
		if (isLocal) {
			netId = this.toString() + Game.netID;
		}
    	String resources = "  ";
    	resources += head.currentResource + "  ";
    	resources += arms.get(0).currentResource + "  ";
    	resources += arms.get(1).currentResource + "  ";
    	resources += legs.currentResource + "  ";
    	resources += legs.currentResource + "  ";
    	resources += body.currentResource;

		float X = aimPoint.x;
		float Y = aimPoint.y;

		String inverts = "";

		inverts += head.invertPart + "  ";
		inverts += body.invertPart + "  ";
		inverts += arms.get(0).invertPart + "  ";
		inverts += arms.get(1).invertPart + "  ";
		inverts += legs.invertPart + "  ";
		inverts += legs.invertPart;
        return super.serialize() + "  " + X + "  " + Y  + "  " + direction + "  " + primaryColor.r + "  " + primaryColor.g + "  " + primaryColor.b + "  " + secondaryColor.r + "  " + secondaryColor.g + "  " + secondaryColor.b + resources + "  " + health + "  " + isPlayer + "  " + inverts;
    }

	private class WalkCollider extends Entity {

		public boolean hasGround;
		public WalkCollider(Vector2 pos, GameObject parent) {
			super(pos, 0, 0, parent);
			hasPhysics = true;
			kinematic = true;
			canMoveObjects = false;
			team = -100;
			collider = CreatePolygon.rect(5, 128);
		}

		@Override
		public void preparePhysics() {
			super.preparePhysics();
			hasGround = false;
		}

		@Override
		public void onCollision(Entity other) {
			if (other.team == -1 || other.team == -2) {
				hasGround = true;
			}
		}
	}

	private class ShootCollider extends Entity {

		public Mech newTarget;
		public ShootCollider(Vector2 pos, GameObject parent) {
			super(pos, 0, 0, parent);
			hasPhysics = true;
			kinematic = true;
			canMoveObjects = false;
			team = -100;
			collider = CreatePolygon.circle(256, 8);
		}

		@Override
		public void preparePhysics() {
			super.preparePhysics();
			newTarget = null;
		}

		@Override
		public void onCollision(Entity other) {
			if (other instanceof Mech) {
				if (((Mech) other).isPlayer) {
					newTarget = (Mech) other;
				}
			}
		}
	}
}
