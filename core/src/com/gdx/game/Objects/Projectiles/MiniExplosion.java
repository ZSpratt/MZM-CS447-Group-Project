package com.gdx.game.Objects.Projectiles;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.AnimatedEntity;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.BaseObjects.NetworkEntity;
import com.gdx.game.CreatePolygon;
import com.gdx.game.ResourceManager;
import com.gdx.game.Time;

public class MiniExplosion extends NetworkEntity{

	float lifeTime = 1;
	AnimatedEntity explosion;
	public MiniExplosion(Vector2 pos, float size) {
		super(pos, 0, 0, null);

		explosion = new AnimatedEntity(pos, 0, 0, null);
		explosion.setAnim(ResourceManager.getAnim("Projectiles/spritesheet2.png", 81, lifeTime/81f), 0);
		explosion.looping = false;
	}

	public MiniExplosion(String s) {
		super(s);

		explosion = new AnimatedEntity(pos, 0, 0, null);
		explosion.setAnim(ResourceManager.getAnim("Projectiles/spritesheet2.png", 81, lifeTime/81f), 0);
		explosion.looping = false;
	}

	@Override
	public void update() {
		team = -1;
		super.update();
		explosion.localPos.set(localPos);
		explosion.update();
		explosion.lateUpdate();
		lifeTime -= Time.deltaTime;

		if (lifeTime <= 0) {
			Delete();
		}
	}

	@Override
	public void render(Batch batch) {
		if (lifeTime > 0) {
			explosion.render(batch);
		}
	}
}
