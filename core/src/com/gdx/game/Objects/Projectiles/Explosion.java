package com.gdx.game.Objects.Projectiles;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.BaseObjects.AnimatedEntity;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.BaseObjects.NetworkEntity;
import com.gdx.game.CreatePolygon;
import com.gdx.game.Objects.Mech;
import com.gdx.game.ResourceManager;
import com.gdx.game.SoundManager;
import com.gdx.game.Time;

import java.util.ArrayList;

public class Explosion extends NetworkEntity{

	float lifeTime = 1;
	AnimatedEntity explosion;
	ArrayList<Mech> hit = new ArrayList<Mech>();
	public float damageScale = 1;
	public Explosion(Vector2 pos, float damageScale) {
		super(pos, 0, 0, null);
		SoundManager.playSound(this.toString() ,ResourceManager.getSound("Sounds/Explosion+3.wav"), 1.0f, pos);
		localPos.set(pos);
		pos.set(localPos);

		explosion = new AnimatedEntity(pos, 0, 0, null);
		explosion.setAnim(ResourceManager.getAnim("Projectiles/spritesheet1.png", 81, lifeTime/81f), 0);
		explosion.looping = false;

		if (damageScale > 0) {
			hasPhysics = true;
			canMoveObjects = false;
			kinematic = true;
			collider = CreatePolygon.circle(50, 8);
		}
		team = -99;

		lateUpdate();
		preparePhysics();
		this.damageScale = damageScale;
	}

	public Explosion(String s) {
		super(s);
		deSerialize(s.split("  "));
		SoundManager.playSound(this.toString() ,ResourceManager.getSound("Sounds/Explosion+3.wav"), 1.0f, pos);
		hasPhysics = true;
		canMoveObjects = false;
		kinematic = true;

		explosion = new AnimatedEntity(pos, 0, 0, null);
		explosion.setAnim(ResourceManager.getAnim("Projectiles/spritesheet1.png", 81, lifeTime/81f), 0);
		explosion.looping = false;

		if (damageScale > 0) {
			hasPhysics = true;
			canMoveObjects = false;
			kinematic = true;
			collider = CreatePolygon.circle(50, 8);
		}
		team = -99;

		lateUpdate();
		preparePhysics();
	}

	@Override
	public void update() {
		super.update();
		explosion.localPos.set(localPos);
		explosion.update();
		explosion.lateUpdate();
		lifeTime -= Time.deltaTime;

		if (lifeTime <= 0) {
			Delete();
		}

		if (lifeTime <= 0.75f) {
			collider = null;
		}

	}

	@Override
	public void render(Batch batch) {
		if (lifeTime > 0) {
			explosion.render(batch);
		}
	}

	@Override
	public void onCollision(Entity other) {
		if (!other.kinematic && !hit.contains(other)) {
			Vector2 delta = new Vector2(other.pos).sub(pos);
			delta.setLength(-150);
			other.velocity.add(delta);
		}

		if (other instanceof Mech) {
			if (!hit.contains(other)) {
				((Mech) other).health -= 5.25 * damageScale;
				hit.add((Mech) other);
			}
		}
	}

	@Override
	public synchronized String serialize() {
		return super.serialize() + "  " + damageScale;
	}

	@Override
	public synchronized void deSerialize(String[] s) {
		super.deSerialize(s);
		damageScale = Float.parseFloat(s[s.length - 1]);
	}
}
