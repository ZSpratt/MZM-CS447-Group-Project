package com.gdx.game.Objects.Projectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.gdx.game.*;
import com.gdx.game.BaseObjects.AnimatedEntity;
import com.gdx.game.BaseObjects.Entity;
import com.gdx.game.BaseObjects.GameObject;
import com.gdx.game.BaseObjects.NetworkEntity;
import com.gdx.game.Objects.Mech;
import com.gdx.game.Objects.MechParts.MechPart;

public class Projectile extends NetworkEntity {
	public float lifetime = 10.0f;
	float angle;
	String resource;
	float damageScale = 1;
	boolean explode = false;
	boolean breakOnHit = false;
	int frames = 1;

	Vector2 acceleration = new Vector2();
	Vector2 globalAcceleration = new Vector2();
	AnimatedEntity display;
	public Projectile(String resource, Vector2 pos, float angle, float fastForward, float damageScale, GameObject parent) {
		super(pos, angle, 10, parent);
		if (resource.equals("Buckshot")) {
			resource = "Bullet";
			damageScale = damageScale * 2f/7f;
			for (int i = 0; i < 7; i++) {
				Game.addGameObject(new Projectile(resource, pos, angle + 30 * ((Game.random.nextFloat() - 0.5f)), fastForward, damageScale, parent));
			}
		}
		this.resource = resource;
		setPart(resource);
		display = new AnimatedEntity(new Vector2(0, 0), 0, 0, null);
		display.setAnim(ResourceManager.getAnim("Projectiles/" + resource + ".png", frames, 1f/frames), 0);
		display.currentFrame = new Sprite((TextureRegion) display.anim.getKeyFrame(0));
		display.looping = false;
		setPart(resource);
		velocity.rotate(angle);
		acceleration.rotate(angle);
		localPos.add(new Vector2(velocity).scl(fastForward));
		pos.set(localPos);
		this.angle = angle;
		setSprite("Projectiles/" + resource + ".png");
		collider = CreatePolygon.rect(display.currentFrame.getWidth(), display.currentFrame.getHeight());
		canMoveObjects = false;
		this.damageScale = damageScale;

		if (resource.equals("Fire")) {
				if (Game.random.nextFloat() > 0.75f) {
					SoundManager.playSound(this.toString(), ResourceManager.getSound("Sounds/Fire.wav"), 1, pos);
				}
		} else {
			SoundManager.playSound(this.toString(), ResourceManager.getSound("Sounds/Gunshot" + Game.random.nextInt(4) + ".wav"), 1, pos);
		}
	}

	public Projectile(String s) {
		super(s);
		setPart(resource);
		display = new AnimatedEntity(new Vector2(0, 0), 0, 0, null);
		display.setAnim(ResourceManager.getAnim("Projectiles/" + resource + ".png", frames, 1f/frames), 0);
		display.currentFrame = new Sprite((TextureRegion) display.anim.getKeyFrame(0));
		display.looping = false;
		setPart(resource);
		deSerialize(s.split("  "));
		setSprite("Projectiles/" + resource + ".png");
		this.localRot = angle;
		update();
		collider = CreatePolygon.rect(display.currentFrame.getWidth(), display.currentFrame.getHeight());
		canMoveObjects = false;
		acceleration.rotate(angle);

		if (resource.equals("Fire")) {
			if (Game.random.nextFloat() > 0.75f) {
				SoundManager.playSound(this.toString(), ResourceManager.getSound("Sounds/Fire.wav"), 1, pos);
			}
		} else {
			SoundManager.playSound(this.toString(), ResourceManager.getSound("Sounds/Gunshot" + Game.random.nextInt(4) + ".wav"), 1, pos);
		}
	}

	public void parsePartData(String[] line) {
		if (line[0].contains("initialVelocity")) {
			String[] args = line[1].split(",");
			velocity = new Vector2(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
		}
		if (line[0].contains("acceleration")) {
			String[] args = line[1].split(",");
			acceleration = new Vector2(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
		}
		if (line[0].contains("globalAcceleration")) {
			String[] args = line[1].split(",");
			globalAcceleration = new Vector2(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
		}
		if (line[0].contains("explode")) {
			explode = (line[1].equals("true"));
		}
		if (line[0].contains("breakOnHit")) {
			breakOnHit = (line[1].equals("true"));
		}
		if (line[0].contains("frames")) {
			frames = Integer.parseInt(line[1]);
		}
	}

	public void setPart(String resource) {
		this.resource = resource;

		FileHandle resouceData = Gdx.files.internal("Projectiles/" + resource);
		if (resouceData.exists()) {
			String[] lines = resouceData.readString().split("\n");
			for (int i = 0; i < lines.length; i++) {
				String line = lines[i].replaceAll("\\s+", "");
				String[] data = line.split(":");
				if (data.length != 2) {
					System.err.println("Line Error in file " + resource + "\nOn Line " + i + "\n" + lines[i]);
				} else {
					parsePartData(data);
				}
			}
		}
		if (display != null) {
			if (resource.equals("Fire")) {
				lifetime = 0.8f;
				localRot = Game.random.nextFloat() * 360f;
				display.looping = false;
			} else {
				display.looping = true;
			}
		}
	}

	@Override
	public void render(Batch batch) {
		display.pos.set(pos);
		display.rot = rot;
		display.render(batch);
	}

	@Override
	public void update() {
		updateFreq = 1000;
		velocity.add(acceleration.x* Time.deltaTime, acceleration.y* Time.deltaTime);
		velocity.add(globalAcceleration.x* Time.deltaTime, globalAcceleration.y* Time.deltaTime);
		lifetime -= Time.deltaTime;
		if (lifetime <= 0) {
			Delete();
		}
	}

	@Override
	public String serialize() {
		return super.serialize() + "  " + angle + "  " + resource + "  " + damageScale;
	}

	@Override
	public void onDelete() {
		super.onDelete();
		if(isLocal) {
			if (explode) {
				Game.addGameObject(new Explosion(pos, 1));
				explode = false;
			} else {
				if (breakOnHit) {
					SoundManager.playSound(this.toString() ,ResourceManager.getSound("Sounds/ExplosionSmall.wav"), 1.0f, pos);
					Game.addGameObject(new MiniExplosion(pos, 10));
				}
			}
		}
	}

	@Override
	public void deSerialize(String[] s) {
		super.deSerialize(s);
		pos.x = Float.parseFloat(s[2]);
		pos.y = Float.parseFloat(s[3]);
		velocity.set(Float.parseFloat(s[4]), Float.parseFloat(s[5]));
		angle = Float.parseFloat(s[6]);
		resource = s[7];
		damageScale = Float.parseFloat(s[8]);
	}

	@Override
	public void onCollision(Entity other) {
		if (other.team != team && other.team > -100 && other.team != -2 && !(other instanceof  Projectile)) {
			if (!explode) {
				if (resource.equals("Fire")) {
					if (other instanceof Mech && !delete) {
						((Mech) other).health -= 0.009 * damageScale;
					}
				} else {
					if (other instanceof Mech && !delete) {
						((Mech) other).health -= 1 * damageScale;
					}
					if (other instanceof MechPart && !delete) {
						((MechPart) other).myMech.health -= 1 * damageScale;
					}
				}
			}

			if (breakOnHit) {
				Delete();
			} else {
				if (other.team == -1) {
					collider = null;
				}
			}
		}
	}
}
