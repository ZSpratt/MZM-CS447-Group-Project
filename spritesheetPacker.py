from sys import *
import os
from PIL import Image
from math import *

if __name__ == "__main__":
    path = argv[1]
    print("Files from {}".format(path))
    fileList = os.listdir(path)
    print("Files in folder : ({})".format(len(fileList)))

    images = []
    for f in fileList:
        if f[-4:] == ".png":
            print(f)
            images.append(Image.open("{}{}".format(path, f)))
    print("Images opened : ({})".format(len(images)))
    tileWidth = int(sqrt(len(images)))
    tileHeight = 0
    
    tileHeight = tileWidth
    while (tileHeight * tileWidth) < len(images):
        tileHeight = tileHeight + 1
    print("New Image Tile Dimensions : {}, {}".format(tileWidth, tileHeight))

    imageGrid = []
    imageRow = []
    count = 0
    for image in images:
        imageRow.append(image)
        count += 1
        if count >= tileWidth:
            count = 0
            imageGrid.append(imageRow)
            imageRow = []
    imageGrid.append(imageRow)
    print ("Confirm size of output grid : {}, {}".format(len(imageGrid), len(imageGrid[0])))

    width, height = imageGrid[0][0].size
    outWidth = width * tileWidth
    outHeight = height * tileHeight

    print ("Generating Image of size : {}, {}".format(outWidth, outHeight))
    outImage = Image.new('RGBA', (outWidth, outHeight))

    print("Generate final image")
    for y in range(len(imageGrid)):
        for x in range(len(imageGrid[y])):
            outImage.paste(imageGrid[y][x], (x * width, y * height))
            
    imageName = ""
    if len(argv) >= 3:
        imageName = argv[2]
    if imageName == "":
        imageName = "out.png"
    if imageName[-4:] != ".png":
        imageName += ".png"
    outImage.save("{}../{}".format(path, imageName))
