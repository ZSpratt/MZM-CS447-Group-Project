from sys import *
import os
from math import *

supportedFiles = [".png", ".wav", ".mp3"]

root = ""
manifest = []


def recurseFiles(inPath):
    print(inPath)
    for directory in os.listdir(root + inPath):
        path = root + inPath + directory
        if (os.path.isdir(path)):
            recurseFiles(inPath + directory + "/")
        else:
            for tp in supportedFiles:
                if path.endswith(tp):
                    manifest.append(inPath + directory)

if __name__ == "__main__":
    root = "core/assets/"
    print("Reading Files")
    recurseFiles("")

    print("Writing Manifest")
    outFile = open(root + "manifest", "w+")

    for line in manifest:
        outFile.write(line + "\n")
    outFile.close()
