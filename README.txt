Team members: 
Josiah Peterson
Michael Kim
Michael Wong
Brandon Warner
Zachary Spratt

Controls:
Contorls list in pause menu [ESC] by default

Running Instructions:
1.    Run jar in root folder
2.    gradlew desktop:dist  --  jar in desktop/build/libs

No cheatcodes

Low-bar goals:
(DONE) - Modular mech (two arms, two legs, torso, head)
Mech needs to be somewhat stable with all sets of gear
Mech needs to be able to move and attack
Special attacks for torsos
Different attack styles
(DONE) - Multi-player co-op
Players can connect to each other within the hub level
Peer-to-peer hosting player acts as server
Movements and attacks need to be synchronized over the server
(DONE) - Hub level 
Should be used to customize the player mech as well as move to a level
May be used to implement other high-bar customization features
(DONE, "Bosses" are mech zombies) - 3 different levels
Each �level� will has a different design and look

Other goals & features:
- Saving progression 
- More stuff to do in hub (killing zombies)
- Shop system

Licensing:

The tilesets are CC0 1.0 Universal.
The mech art is public domain
The sound effects CC
The music is made in FL Studio and cannot be redistributed for commercial purposes as the sound effects used in the music come from the FL Studio demo version. The music is also made in FL Studio demo version, which prohibits commercial use.

